""" Plots transfer functions representing time-delay proportional feedback."""
import numpy as np
import matplotlib.pyplot as plt

def main():
	frequency = np.logspace(-2,3,1000)
	omegas = np.pi*2*frequency
	laplace_s_list = complex(0, 1) * omegas
	m = 100
	b = 10
	k = 100
	time_delay = .001
	transfer_function = lambda s: 1/(m*s*s + b*s + np.exp(-s*time_delay)*k)

	fig, (mag_ax, phase_ax) = plt.subplots(2,1, sharex=True)
	mag_ax.loglog(frequency, abs(transfer_function(laplace_s_list)))
	phase_ax.semilogx(frequency, 180/np.pi*np.angle(transfer_function(laplace_s_list)))

	mag_ax.set_ylabel("magnitude")
	phase_ax.set_ylabel("phase (deg)")
	phase_ax.set_xlabel("frequncy (Hz)")

	fig.suptitle("Bode Plot")


	plt.show()


if __name__ == '__main__':
	main()