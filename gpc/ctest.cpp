#include "gpc.hpp"
#include <iostream>

int main(void)
{
	std::cout<<"Hello World!\n"<<"First Order Step Response:"<<std::endl;
	// FirstOrder():a(0.5f),kp(1.0f),kd(0.0f),x0(0.0f) {}
 //    FirstOrder(float a):a(a),kp(1.0f),kd(0.0f),x0(0.0f) {}
 //    FirstOrder(float a, float kp):a(a),kp(kp),kd(0.0f),x0(0.0f) {}
 //    FirstOrder(float a, float kp, float kd):a(a),kp(kp),kd(kd),x0(0.0) {}
 //    FirstOrder(float a, float kp, float kd, float x0)
	gpc::FirstOrder lp0arg; // lowpass
	gpc::FirstOrder lp1arg(0.5); // lowpass
	gpc::FirstOrder lp2arg(0.5, 1.0f); // lowpass
	gpc::FirstOrder lp3arg(0.5, 0.0f, 1.0f); // diff
	gpc::FirstOrder lp4arg(0.5, 1.0f, 0.0f, 0.0f); // lowpass
	gpc::SumOfSystems dummy;
	gpc::FirstOrder lp2arg1(0.75, 0.2f); 
	gpc::FirstOrder lp2arg2(0.3, 0.8f); 
	dummy.systems.push_back(&lp2arg1);
	dummy.systems.push_back(&lp2arg2);

	std::cout<<"t  x1  x2  x3  x4"<<std::endl;
	for (int i=0;i<4;i++)
		std::cout<<i
		<<"  "<<lp0arg.next(1.0)
		<<"  "<<lp1arg.next(1.0)
		<<"  "<<lp2arg.next(1.0)
		<<"  "<<lp3arg.next(1.0)
		<<"  "<<lp4arg.next(1.0)
		<<"  "<< dummy.next(1.0)
		<<std::endl;
	std::cout<<"t  dxdt"<<std::endl;
	for (int i=2;i<8;i++)
		std::cout<<i
		<<"  "<<lp3arg.next(i)
		<<std::endl;
}