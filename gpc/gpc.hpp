/* Gray Thomas, UT Austin---NSTRF NNX15AQ33H---Spring 2017 */

#ifndef GPC_HPP
#define GPC_HPP
#include <vector>
//#include <Eigen/Dense>
namespace gpc{

float sat(float x);
float clamp(float x, float max_x);

class SISOSystem{
public:
    virtual ~SISOSystem(){}
    virtual float next(float y){return 0.5f;};
};

class ZeroOrder: public SISOSystem{
public:
    float d;
    virtual ~ZeroOrder(){}
    ZeroOrder():d(0.0f){}
    ZeroOrder(float d):d(d) {}
    virtual float next(float y);
};

class FirstOrder: public SISOSystem{
    // Z-domain: kp (1-a) z/(z-a) + kd (1-a) (z-1)/(z-a)
public:
    float a;
    float kp;
    float kd;
    float x0;
    virtual ~FirstOrder(){}
    FirstOrder():a(0.5f),kp(1.0f),kd(0.0f),x0(0.0f) {}
    FirstOrder(float a):a(a),kp(1.0f),kd(0.0f),x0(0.0f) {}
    FirstOrder(float a, float kp):a(a),kp(kp),kd(0.0f),x0(0.0f) {}
    FirstOrder(float a, float kp, float kd):a(a),kp(kp),kd(kd),x0(0.0) {}
    FirstOrder(float a, float kp, float kd, float x0):a(a),kp(kp),kd(kd),x0(x0) {}
    virtual float next(float y);
};

class SSFirstOrder: public SISOSystem{
    // Z-domain: k b z/(z-a)
public:
    float a;
    float b;
    float k;
    float x;
    virtual ~SSFirstOrder(){}
    SSFirstOrder(float a, float b, float k, float x):a(a),b(b),k(k),x(x) {}
    virtual float next(float y);
};

class SecondOrder: public SISOSystem{
    // Z-domain: K [zI-A]\Bz
    // A = [c s; -s c]
public:
    float c;
    float s;
    float b0;
    float b1;
    float k0;
    float k1;
    float x0;
    float x1;
        virtual ~SecondOrder(){}
    SecondOrder()
    : c(1.0), s(0.0f), b0(1.0f), b1(0.0f), k0(1.0f), k1(0.0f), x0(0.0f), x1(0.0f)
    {}
    SecondOrder(float c, float s)
    : c(c), s(s), b0(1.0f), b1(0.0f), k0(1.0f), k1(0.0f), x0(0.0f), x1(0.0f)
    {}
    SecondOrder(float c, float s, float b0, float b1, float k0, float k1)
    : c(c), s(s), b0(b0), b1(b1), k0(k0), k1(k1), x0(0.0f), x1(0.0f)
    {}
    SecondOrder(float c, float s, float b0, float b1, float k0, float k1, float x0, float x1)
    : c(c), s(s), b0(b0), b1(b1), k0(k0), k1(k1), x0(x0), x1(x1)
    {}
    virtual float next(float y);
};

class SumOfSystems: public SISOSystem{
public:
    virtual ~SumOfSystems(){}
    std::vector<SISOSystem*> systems;
    SumOfSystems()
        : systems()
        {}

    virtual float next(float y);
};

/*class MIMOSystem{
public:
    virtual std::vector<float> next(std::vector<float> y);
    virtual Eigen::VectorXf next(Eigen::VectorXf y);
};

struct MatrixOfSystems: Public MIMOSystem{ 
    std::vector<std::vector<SISOSystem>> systems;
    std::vector<float> next(std::vector<float> y);
    Eigen::VectorXf next(Eigen::VectorXf y);
};*/

// Templated fixed size version?


}
#endif
