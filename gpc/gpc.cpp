/* Gray Thomas, UT Austin---NSTRF NNX15AQ33H---Spring 2017 */

#include "gpc.hpp"
#include <iostream>
#include <math.h>

namespace gpc{
float sat(float x)
{
    if (isnan(x))
        return 0.0;
    if (x > 1.0)
        return 1.0;
    if (x < -1.0)
        return -1.0;
    return x;
}

float clamp(float x, float max_x)
{
    if (isnan(x))
        return 0.0;
    if (x > max_x)
        return max_x;
    if (x < -max_x)
        return -max_x;
    return x;
}


float ZeroOrder::next(float y)
{
		return d*y;
}
float FirstOrder::next(float y)
{
    // Z-domain: kp (1-a) z/(z-a) + kd (1-a) (z-1)/(z-a)
    float temp = (1.0f - a);
    float diff = temp * (y-x0);
    x0 = x0 * a + temp * y; // precision oriented logic
    // return kp * x0 + kd * (x0 - y); OLD LOGIC
    return kp * x0 + kd * diff; // Now kd is actually derivative gain
}

float SSFirstOrder::next(float y)
{
    // Z-domain: k b z/(z-a)
    float zx = a * x + b * y;
    float ret = k * x;
    x=zx;
    return ret;
}

float SecondOrder::next(float y)
{
    // Z-domain: K [zI-A]\Bz
    // A = [c s; -s c]
    float zx0 = c * x0 + s * x1 + b0 * y;
    float zx1 = -s * x0 + c * x1 + b1 * y;
    float ret = k0 * zx0 + k1 * zx1;
    x0 = zx0;
    x1 = zx1;
    return ret;
}

float SumOfSystems::next(float y)
{
    float res = 0.0f;
    for(std::vector<SISOSystem*>::iterator sys = systems.begin(); sys != systems.end(); ++sys) {
        res += (*sys)->next(y);
    }
    return res;
}

}

