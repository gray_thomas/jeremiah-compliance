from admittance_plotting_utils import *
def main():
    #     data_off = DataSet("log_20181112T142843", csv=True, description=
    # """\tThis experiment is the first one of its type. Presumably it is
    # driven by a commanded current chirp signal, while the spring is held fixed
    # by the frame mount device.""")
        # data_off = DataSet(0, csv=False)
    #     data_off = DataSet("log_20181127T112047", csv=True, description=
    # """\tIn this experiment, I turned off the control current, and shook the
    # actuator from the compliant cuff. I do believe I hit the bottom a few
    # times, hopefully this doesn't corrupt the data too much."""
    #     )

    #     data_off = DataSet("log_20181127T120004", csv=True, description=
    # """\tIn this experiment, I turned off the control current, and shook the
    # actuator from the rigid arm. This should give decent system 1 and 4 estimates."""
    #     )

    #     data_off = DataSet("log_20181127T143127", csv=True, description=
    # """\tIn this experiment, I turned off the control current, and shook the
    # actuator from the rigid arm. There was a feedback controller: {motor_kp: 100
    # motor_kd: 0}. This should give decent system 2 and 4 estimates."""
    #     )    

#     ds = DataSet("log_20181127T163101", csv=True, description=
# """\tIn this experiment, there was a feedback controller: {motor_kp: 100
# motor_kd: 0}. The behavior is forced from the cuff side, featuring steady
# state holds around the arbitrary starting configuration (zero point for the
# motor spring). Zero... Down... Zero... Up... Down (again)... Zero?....""",
#     ss_time_cuts=np.array([[3, 13], [16, 33], [36, 47], [51, 70], [73, 79], [87, 97], [100, 104]])
#     )


#     ds = DataSet("log_20181127T164831", csv=True, description=
# """\tIn this experiment, there was a feedback controller: {motor_kp: 100
# motor_kd: 0}. The behavior is forced from the cuff side, featuring steady
# state holds around the arbitrary starting configuration. This time, I left it
# alone for a while, attached a spring-loaded rope, waiteded, tapped, waited,
# released, waited, tapped, waited... and that sort of thing for a while. I
# tried to leave it long enough to reach an equilibrium. Hopefully the tapps
# will return it to the original equilibrium, and the spring-loaded rope will
# make a more repeatable equilibrium than my previous one.""",
#     ss_time_cuts=np.array([[15,35],[43, 58], [65, 95], [102, 117], [122, 138], [142, 158], [164, 188], [193, 216]])
#     )

    # ds = DataSet("log_20181128T145355", csv=True, description=
# """\tIn this experiment, there was no feedback controller: {motor_kp: 0
# motor_kd: 0}. The behavior is forced from the arm. """,
    # )



#     ds = DataSet("log_20181205T144432", csv=True, description=
# """\tIn this experiment, there was a chirp of amplitude 1: {motor_kp: 1
# motor_kd: 0}. Behavior was forced from the arm. """,
#     )

#     ds = DataSet("log_20181221T105656", csv=True, description=
# """\t In this experiment we attempted to fix the arm while the motor was commanded to chirp for 2.5 minutes from .1 to 10 Hz
# {motor_kp: 1, motor_kd: 0, chirp_amplitude: 4}""",
#     )

#     ds = DataSet("log_20181221T173913", csv=True, description=
# """\t In this experiment I attempted to fix the arm (in the upright position) while the motor was commanded to chirp for 2.5 minutes from .01 to 100 Hz. A
# 0motor_comp fault was triggered, I think it had something to do with the frequency being too high because the amplitude was much lower at high frequencies.
# {motor_kp: 1, motor_kd: 0, chirp_amplitude: 1}""",
#     )


#     ds = DataSet("log_20181221T174849", csv=True, description=
# """\t In this experiment I attempted to fix the arm (in the upright position) while the motor was commanded to chirp for 2.5 minutes from .1 to 10 Hz. 
# {motor_kp: 0, motor_kd: 0, chirp_amplitude: 1}""",
#     )

#     ds = DataSet("log_20181221T175620", csv=True, description=
# """\t In this experiment I attempted to fix the arm (in the upright position) while the motor was commanded to chirp for 2.5 minutes from .1 to 100 Hz. 
# {motor_kp: 0, motor_kd: 0, chirp_amplitude: 3}""",
#     )

#     ds = DataSet("log_20190121T151243", csv=True, description=
# """\t In this experiment we attached a hanging weight to the force torque sensor (which was rigidly mounted, no spring).
# The controller was a motor position controller with a Kp of 100 and no Kd term. The chirp had an amplitude of 6 (amps).
# The chirp function was modified to handle the case where the upper and lower frequencies were the same.
# Additional sensor data was recorded: all six components of the force torque sensor.
# The sensor is about seven inches away from the rotary axis of the joint, 
# and more precicely the edge of the sensor holder is 61 mm from the edge of the rod holder clamp.
# 1196 gram weight.""",
#     )    
#     ds = DataSet("log_20190121T183936", csv=True, description=
# """\t In this experiment we attached a hanging weight to the force torque sensor (which was rigidly mounted, no spring).
# The controller was a motor position controller with a Kp of 100 and no Kd term. The chirp had an amplitude of 6 (amps).
# For the first time, a DOB for only transmission losses was applied (and was stable). DOB filder bandwidth: 1 Hz. Max DOB current: 2 amps.
# The chirp function was modified to handle the case where the upper and lower frequencies were the same.
# Additional sensor data was recorded: all six components of the force torque sensor.
# The sensor is about seven inches away from the rotary axis of the joint, 
# and more precicely the edge of the sensor holder is 61 mm from the edge of the rod holder clamp.
# 1196 gram weight.""",
#     )

#     ds = DataSet("log_20190123T014114", csv=True, description=
# """\t In this experiment we attached a hanging weight to the force torque sensor (which was rigidly mounted, no spring).
# The controller was a motor position controller with a Kp of 100 and no Kd term. The chirp had an amplitude of 6 (amps).
# A DOB for only transmission losses was applied (and was stable). The DOB was set with a new kind of Q filter.
# The Q filter has two parameters, alpha = 5Hz, and lambda = 1Hz. Max DOB current: 2 amps.
# The chirp function was modified to handle the case where the upper and lower frequencies were the same.
# Additional sensor data was recorded: all six components of the force torque sensor.
# Additional controller data was recorded: internal control variables for a second version of the DOB.
# The sensor is about seven inches away from the rotary axis of the joint, 
# and more precicely the edge of the sensor holder is 61 mm from the edge of the rod holder clamp.
# 1196 gram weight.""",
#     )

#     ds = DataSet("log_20190123T022636", csv=True, description=
# """\t In this experiment we attached a hanging weight to the force torque sensor (which was rigidly mounted, no spring).
# The controller was a motor position controller with a Kp of 100 and no Kd term. The chirp had an amplitude of 6 (amps).
# A DOB for only transmission losses was applied (and was stable). The DOB was set with the new style of Q filter.
# The Q filter has two parameters, alpha = 5Hz, and lambda = 1Hz. Max DOB current: 2 amps.
# The chirp function was modified to handle the case where the upper and lower frequencies were the same.
# Additional sensor data was recorded: all six components of the force torque sensor.
# Additional controller data was recorded: internal control variables for a second version of the DOB. (and the reflected motor position in meters)
# The sensor is about seven inches away from the rotary axis of the joint, 
# and more precicely the edge of the sensor holder is 61 mm from the edge of the rod holder clamp.
# 1196 gram weight.""",
#     )

#     ds = DataSet("log_20190123T032007", csv=True, description=
# """\t In this experiment we attached a hanging weight to the force torque sensor (which was rigidly mounted, no spring).
# The controller was a motor position controller with a Kp of 100 and no Kd term. The chirp had an amplitude of 6 (amps).
# A DOB for only transmission losses was applied (and was stable). The DOB was set with the new style of Q filter.
# The Q filter has two parameters, alpha = 10Hz, and lambda = 4Hz. Max DOB current: 2 amps.
# The chirp function was modified to handle the case where the upper and lower frequencies were the same.
# Additional sensor data was recorded: all six components of the force torque sensor.
# Additional controller data was recorded: internal control variables for a second version of the DOB. (and the reflected motor position in meters)
# The sensor is about seven inches away from the rotary axis of the joint, 
# and more precicely the edge of the sensor holder is 61 mm from the edge of the rod holder clamp.
# 1196 gram weight.""",
#     )    

#     ds = DataSet("log_20190123T032436", csv=True, description=
# """\t In this experiment we attached a hanging weight to the force torque sensor (which was rigidly mounted, no spring).
# The controller was a motor position controller with a Kp of 100 and no Kd term. The chirp had an amplitude of 6 (amps).
# A DOB for only transmission losses was applied (and was stable). 
# But the DOB was disabled! The DOB internal data was still recorded, however.
# The DOB was set with the new style of Q filter.
# The Q filter has two parameters, alpha = 10Hz, and lambda = 4Hz. Max DOB current: 2 amps.
# The chirp function was modified to handle the case where the upper and lower frequencies were the same.
# Additional sensor data was recorded: all six components of the force torque sensor.
# Additional controller data was recorded: internal control variables for a second version of the DOB. (and the reflected motor position in meters)
# The sensor is about seven inches away from the rotary axis of the joint, 
# and more precicely the edge of the sensor holder is 61 mm from the edge of the rod holder clamp.
# 1196 gram weight.""",
#     )  

#     ds = DataSet("log_20190126T113341", csv=True, description=
# """\t In this experiment we fixed the exo arm with the new sheet metal bracket connected to the backwards force sensor 
# without the manufactured spring. Chirp amplitude = 6 from .5 to 20 Hz, but faulted near resonsance frequency, pure feedfoward
# no feedback test, no disturbance observer. Distand from force sensor to rod end clamp is now 7.5cm. """)

#     ds = DataSet("log_20190126T113722", csv=True, description=
# """\t In this experiment we fixed the exo arm with the new sheet metal bracket connected to the backwards force sensor 
# without the manufactured spring. Chirp amplitude = 4 from .5 to 20 Hz, but faulted near resonsance frequency, pure feedfoward
# no feedback test, no disturbance observer. Distand from force sensor to rod end clamp is now 7.5cm. """)

#     ds = DataSet("log_20190126T114145", csv=True, description=
# """\t In this experiment we fixed the exo arm with the new sheet metal bracket connected to the backwards force sensor 
# without the manufactured spring. Chirp amplitude = 4 from 20 to .5 Hz (BACKWARDS), but faulted near resonsance frequency, pure feedfoward
# no feedback test, no disturbance observer. Distand from force sensor to rod end clamp is now 7.5cm. """)

#     ds = DataSet("log_20190126T130022", csv=True, description=
# """\t In this experiment we fixed the exo arm with the new sheet metal bracket connected to the backwards force sensor 
# without the manufactured spring. Chirp amplitude = 1 from .5 to 20 Hz, pure feedfoward
# no feedback test, no disturbance observer. Distand from force sensor to rod end clamp is 7.5cm. """)

#     ds = DataSet("log_20190126T130226", csv=True, description=
# """\t In this experiment we fixed the exo arm with the new sheet metal bracket connected to the backwards force sensor 
# without the manufactured spring. Chirp amplitude = 1 from .5 to 20 Hz, WITH disturbance observer with
# corrected sign error on actuator_force. We noticed the DOB saturated at -2A the majority of the time. Also, there was 
# a large transient upon startup. Distance from force sensor to rod end clamp is 7.5cm. """)

#     ds = DataSet("log_20190126T130435", csv=True, description=
# """\t In this experiment we fixed the exo arm with the new sheet metal bracket connected to the backwards force sensor 
# without the manufactured spring. Chirp amplitude = 1, Chirp offset = -2 (to simulate our DOB in previous test)
#  from .5 to 20 Hz, no DOB. Distance from force sensor to rod end clamp is 7.5cm. """)


#     ds = DataSet("log_20190201T103815", csv=True, description=
# """\t This experiment was run with Binghan's setup, unfixed arm with a weight of 2420g. System vibrated for duration
#  of test.{chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1, chirp_offset: 0, motor_kp: 100,
#  motor_kd: 10, reflected_motor_mass: 231 (recently switched signs), max_dob_current: 2}
#  DOB params: print_discrete_AB(alpha_hz=10.000000, lambda_hz=4.000000, time_step=0.001000)""")

#     ds = DataSet("log_20190201T110758", csv=True, description=
# """\t This experiment was run with Binghan's setup, unfixed arm with a weight of 2420g. System was stable.
# {chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1, chirp_offset: 0, motor_kp: 10,
#  motor_kd: 4, reflected_motor_mass: 231, max_dob_current: 2}
#  DOB params: print_discrete_AB(alpha_hz=10.000000, lambda_hz=4.000000, time_step=0.001000)""")

#     ds = DataSet("log_20190201T111829", csv=True, description=
# """\t This experiment was run with Binghan's setup, unfixed arm with a weight of 2420g. System was stable.
# {chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1, chirp_offset: 0, motor_kp: 10,
#  motor_kd: 4, reflected_motor_mass: 231, max_dob_current: 2} Now there is a 1 second ramp up on the current adjustment.
#  DOB params: print_discrete_AB(alpha_hz=10.000000, lambda_hz=4.000000, time_step=0.001000)""")   

#     ds = DataSet("log_20190201T112422", csv=True, description=
# """\t This experiment was run with Binghan's setup, unfixed arm with a weight of 2420g. System was stable.
# {chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1, chirp_offset: 0, motor_kp: 10,
#  motor_kd: 4, reflected_motor_mass: 231, max_dob_current: 2} Now there is a 1 second drop out on the current adjustment to avoid the DOB startup jiggle.
#  DOB params: print_discrete_AB(alpha_hz=10.000000, lambda_hz=4.000000, time_step=0.001000)""") 

#     ds = DataSet("log_20190201T113835", csv=True, description=
# """\t This experiment was run with Binghan's setup, unfixed arm with a weight of 2420g. System was stable.
# {chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1, chirp_offset: 0, motor_kp: 10,
#  motor_kd: 4, reflected_motor_mass: 231, max_dob_current: 2} DOB with 1 second drop out on the current adjustment to avoid the DOB startup jiggle.
#  DOB params: print_discrete_AB(alpha_hz=50.000000, lambda_hz=4.000000, time_step=0.001000)""")  

#     ds = DataSet("log_20190201T114752", csv=True, description=
# """\t This experiment was run with Binghan's setup, unfixed arm with a weight of 2420g. System was stable.
# {chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1, chirp_offset: 0, motor_kp: 10,
#  motor_kd: 4, reflected_motor_mass: 231, max_dob_current: 2} DOB with 1 second drop out on the current adjustment to avoid the DOB startup jiggle.
#  DOB params: print_discrete_AB(alpha_hz=25.000000, lambda_hz=10.000000, time_step=0.001000)""")  

#     ds = DataSet("log_20190208T093327", csv=True, description=
# """\t Spring cuff test with locked cuff (at hinge strap pivots). 
# 18.1 cm spring length (back of spring to back of cuff slider).
# 25.9 cm from base of actuator arm to front of main cuff arm (now attached to spring.)
# System was stable. This test was designed to measure the radial system's mass and spring constant.
# YAML parameter file: {chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1, chirp_offset: 0, motor_kp: 0,
# motor_kd: 0, reflected_motor_mass: 231, reflected_motor_damping: 3465, 
# reflected_motor_constant: 186.176, max_dob_current: 2}
# DOB with 1 second drop out on the current adjustment to avoid the DOB startup jiggle.
# DOB params: print_discrete_AB(alpha_hz=25.000000, lambda_hz=10.000000, time_step=0.001000)""") 

#     ds = DataSet("log_20190208T103905", csv=True, description=
# """\t Spring cuff test with locked cuff (at hinge strap pivots). 
# 18.1 cm spring length (back of spring to back of cuff slider).
# 25.9 cm from base of actuator arm to front of main cuff arm (now attached to spring.)
# System was stable. This test was designed to measure the radial system's mass and spring constant. 
# Added 660g weight to end of arm (est .1415 kg-m^2 larger).
# Noticed that the weight was jiggling during test due to loose nuts. 
# YAML parameter file: {chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1,
# chirp_offset: 0, motor_kp: 0, motor_kd: 0, reflected_motor_mass: 231, reflected_motor_damping: 3465, 
# reflected_motor_constant: 186.176, max_dob_current: 2}
# DOB with 1 second drop out on the current adjustment to avoid the DOB startup jiggle.
# DOB params: print_discrete_AB(alpha_hz=25.000000, lambda_hz=10.000000, time_step=0.001000)""") 

#     ds = DataSet("log_20190208T104533", csv=True, description=
# """\t Spring cuff test with locked cuff (at hinge strap pivots). 
# 18.1 cm spring length (back of spring to back of cuff slider).
# 25.9 cm from base of actuator arm to front of main cuff arm (now attached to spring.)
# System was stable. This test was designed to measure the radial system's mass and spring constant. 
# Added 660g weight to end of arm (est .1415 kg-m^2 larger).
# Tightened loose nuts and prevented jiggling of weight.
# YAML parameter file: {chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1,
# chirp_offset: 0, motor_kp: 0, motor_kd: 0, reflected_motor_mass: 231, reflected_motor_damping: 3465, 
# reflected_motor_constant: 186.176, max_dob_current: 2}
# DOB with 1 second drop out on the current adjustment to avoid the DOB startup jiggle.
# DOB params: print_discrete_AB(alpha_hz=25.000000, lambda_hz=10.000000, time_step=0.001000)""") 

#     ds = DataSet("log_20190208T105413", csv=True, description=
# """\t Spring cuff test with locked cuff (at hinge strap pivots). 
# 18.1 cm spring length (back of spring to back of cuff slider).
# 25.9 cm from base of actuator arm to front of main cuff arm (now attached to spring.)
# System was stable. This test was designed to measure the radial system's mass and spring constant. 
# Added 660g weight to end of arm (est .1415 kg-m^2 larger).
# Loosened cuff-to-sheet metal screws in order to unconstrain rotation.
# YAML parameter file: {chirp_low_freq_hz: .5, chirp_high_freq_hz: 20, chirp_amplitude: 1,
# chirp_offset: 0, motor_kp: 0, motor_kd: 0, reflected_motor_mass: 231, reflected_motor_damping: 3465, 
# reflected_motor_constant: 186.176, max_dob_current: 2}
# DOB with 1 second drop out on the current adjustment to avoid the DOB startup jiggle.
# DOB params: print_discrete_AB(alpha_hz=25.000000, lambda_hz=10.000000, time_step=0.001000)""")  

#     ds = DataSet("log_20190214T113320", csv=True, description=
# """\t Backdrove the arm through its entire range, just to plot jacobian vs arm angle. 
# .yaml params:
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0 #1 CHANGED
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0 # = K1, in units of N/m, < 6.5e6, <<9e6 
# motor_kd: 0 #30000 # = B1, in units of Ns/m < 6.534, <<9e4 CHANGED
# spring_kp: 0 #4 # = K2, in units of N/N CHANGED
# spring_kd: 0 # = B2, Ns/N
# cuff_kp: 0 #5 #this is K3 in compliance shaping controller, units of Nm/Nm CHANGED
# cuff_kd: 0 # 0.03 #this is B3 is compliance shaping controller, Nms/Nm CHANGED
# reflected_motor_mass: 231 #in linear space (kg)
# reflected_motor_damping: 3465 #in linear space (Ns/m)
# reflected_motor_constant: 186.176 #in units of N/A
# max_dob_current: 2
# spring_k: 562100 # SEA spring stiffness in linear frame N/m""")  

#     ds = DataSet("log_20190214T141515", csv=True, description=
# """\t Tried to run the same test but it vibrated and I E-stopped.
# .yaml params:
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0 #1 CHANGED
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0 # = K1, in units of N/m, < 6.5e6, <<9e6 
# motor_kd: 0 #30000 # = B1, in units of Ns/m < 6.534, <<9e4 CHANGED
# spring_kp: 0 #4 # = K2, in units of N/N CHANGED
# spring_kd: 0 # = B2, Ns/N
# cuff_kp: 0 #5 #this is K3 in compliance shaping controller, units of Nm/Nm CHANGED
# cuff_kd: 0 # 0.03 #this is B3 is compliance shaping controller, Nms/Nm CHANGED
# reflected_motor_mass: 231 #in linear space (kg)
# reflected_motor_damping: 3465 #in linear space (Ns/m)
# reflected_motor_constant: 186.176 #in units of N/A
# max_dob_current: 2
# spring_k: 562100 # SEA spring stiffness in linear frame N/m""")  

#     ds = DataSet("log_20190214T141944", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set DOB current to zero and it doesn't vibrate now.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0 #1 CHANGED
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0 # = K1, in units of N/m, < 6.5e6, <<9e6 
# motor_kd: 0 #30000 # = B1, in units of Ns/m < 6.534, <<9e4 CHANGED
# spring_kp: 0 #4 # = K2, in units of N/N CHANGED
# spring_kd: 0 # = B2, Ns/N
# cuff_kp: 0 #5 #this is K3 in compliance shaping controller, units of Nm/Nm CHANGED
# cuff_kd: 0 # 0.03 #this is B3 is compliance shaping controller, Nms/Nm CHANGED
# reflected_motor_mass: 231 #in linear space (kg)
# reflected_motor_damping: 3465 #in linear space (Ns/m)
# reflected_motor_constant: 186.176 #in units of N/A
# max_dob_current: 0 #2 CHANGED
# spring_k: 562100. # SEA spring stiffness in linear frame N/m""")  
#     ds = DataSet("log_20190214T142643", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set limit in .cpp to 10N. Noticed that motor vibrates with very small amplitude;
# need to debug. 
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0 #1 CHANGED
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0 # = K1, in units of N/m, < 6.5e6, <<9e6 
# motor_kd: 0 #30000 # = B1, in units of Ns/m < 6.534, <<9e4 CHANGED
# spring_kp: 0 #4 # = K2, in units of N/N CHANGED
# spring_kd: 0 # = B2, Ns/N
# cuff_kp: 0 #5 #this is K3 in compliance shaping controller, units of Nm/Nm CHANGED
# cuff_kd: 0 # 0.03 #this is B3 is compliance shaping controller, Nms/Nm CHANGED
# reflected_motor_mass: 231 #in linear space (kg)
# reflected_motor_damping: 3465 #in linear space (Ns/m)
# reflected_motor_constant: 186.176 #in units of N/A
# max_dob_current: 0 #2 CHANGED
# spring_k: 562100. # SEA spring stiffness in linear frame N/m""")  

#     ds = DataSet("log_20190214T145324", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0 #1 CHANGED
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0 # = K1, in units of N/m, < 6.5e6, <<9e6 
# motor_kd: 0 #30000 # = B1, in units of Ns/m < 6.534, <<9e4 CHANGED
# spring_kp: 0 #4 # = K2, in units of N/N CHANGED
# spring_kd: 0 # = B2, Ns/N
# cuff_kp: 0 #5 #this is K3 in compliance shaping controller, units of Nm/Nm CHANGED
# cuff_kd: 0 # 0.03 #this is B3 is compliance shaping controller, Nms/Nm CHANGED
# reflected_motor_mass: 231 #in linear space (kg)
# reflected_motor_damping: 3465 #in linear space (Ns/m)
# reflected_motor_constant: 186.176 #in units of N/A
# max_dob_current: 0 #2 CHANGED
# spring_k: 562100. # SEA spring stiffness in linear frame N/m""")  
#     ds = DataSet("log_20190217T163204", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """)  

#     ds = DataSet("log_20190217T164611", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """) 
#     ds = DataSet("log_20190217T172135", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# Something was wrong with the way I was calculating the Jacobian, so logged more vars to debug.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """) 

#     ds = DataSet("log_20190217T173446", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# More debugging.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """) 

#     ds = DataSet("log_20190217T174620", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# Added another logged param.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """)  

#     ds = DataSet("log_20190217T175246", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# Added another logged param.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """) 

#     ds = DataSet("log_20190217T180446", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# Added another logged param.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """) 

#     ds = DataSet("log_20190217T180927", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """)   
#     ds = DataSet("log_20190217T181218", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """) 
#     ds = DataSet("log_20190217T181336", csv=True, description=
# """\t Another test backdriving the arm to check Jacobian. Set all motor current outputs to 0.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 0               
# spring_k: 562100.                """) 

#     ds = DataSet("log_20190217T181931", csv=True, description=
# """\t Jacobian is confirmed working. Turned on DOB again, it is stable.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 20
# chirp_amplitude: 0               
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 2               
# spring_k: 562100.                       """) 

#     ds = DataSet("log_20190219T143630", csv=True, description=
# """\t Low frequency chirp test (w/ offset for gravity comp) to determine new lower spring stiffness.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 1
# chirp_amplitude: 0.5             
# chirp_offset: -1
# chirp_duration_m: 1
# motor_kp: 0                      
# motor_kd: 0                      
# spring_kp: 0                     
# spring_kd: 0                     
# cuff_kp: 0                       
# cuff_kd: 0                       
# reflected_motor_mass: 231        
# reflected_motor_damping: 3465    
# reflected_motor_constant: 186.176
# max_dob_current: 2               
# spring_k: 562100.                        """) 


#     ds = DataSet("log_20190219T144827", csv=True, description=
# """\t Low frequency chirp test (w/ offset for gravity comp) to determine new lower spring stiffness. 
# Tightened the screws between cuff and sheet metal piece to reduce low-frequency phase noise. 
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 3
# chirp_amplitude: 0.5              
# chirp_offset: -1
# chirp_duration_m: 1
# motor_kp: 0                       
# motor_kd: 0                       
# spring_kp: 0                      
# spring_kd: 0                      
# cuff_kp: 0                        
# cuff_kd: 0                        
# reflected_motor_mass: 231         
# reflected_motor_damping: 3465     
# reflected_motor_constant: 186.176 
# max_dob_current: 2               
# spring_k: 562100.                     """)     
#     ds = DataSet("log_20190221T143156", csv=True, description=
# """\t Grab and move compliant cuff test. 
# Force and cuff force gains still have opposite sign. 
# It feels like moving makes oscillations happen. 
# Mostly a low frequency test. 
# DOB at print_discrete_butterworth(alpha_hz=10.000000, time_step=0.001000)
# recently fixed a bug in the butterworth implementation, btw.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 3
# chirp_amplitude: 0                
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                       # = K1, in units of N/m, < 6.5e6, <<9e6 
# motor_kd: 0                   # = B1, in units of Ns/m < 6.534, <<9e4 
# spring_kp: 0                      # = K2, in units of N/N 
# spring_kd: 0 #-0.1                    # = B2, Ns/N
# cuff_kp: -5 #1                       #this is K3 in compliance shaping controller, units of Nm/Nm CHANGED
# cuff_kd: 0 #0.0795774                #this is B3 is compliance shaping controller, Nms/Nm CHANGED
# reflected_motor_mass: 231         #in linear space (kg)
# reflected_motor_damping: 3465     #in linear space (Ns/m)
# reflected_motor_constant: 186.176 #in units of N/A
# max_dob_current: 2               
# spring_k: 562100.                 # SEA spring stiffness in linear frame N/m
# max_feedback_current: 2""") 
#     ds = DataSet("log_20190221T151458", csv=True, description=
# """\t Grab and move compliant cuff test. 
# Force and cuff force gains still have opposite sign. 
# It feels like moving makes oscillations happen. 
# Mostly a low frequency test. 
# Removed the DOB to test if the oscillations go away.
# DOB at print_discrete_butterworth(alpha_hz=10.000000, time_step=0.001000)
# recently fixed a bug in the butterworth implementation, btw.
# #Cuff Control Yaml
# do_chirp: true
# chirp_low_freq_hz: .5
# chirp_high_freq_hz: 3
# chirp_amplitude: 0                
# chirp_offset: 0
# chirp_duration_m: 1
# motor_kp: 0                       # = K1, in units of N/m, < 6.5e6, <<9e6 
# motor_kd: 0                   # = B1, in units of Ns/m < 6.534, <<9e4 
# spring_kp: 0                      # = K2, in units of N/N 
# spring_kd: 0 #-0.1                    # = B2, Ns/N
# cuff_kp: -5 #1                       #this is K3 in compliance shaping controller, units of Nm/Nm CHANGED
# cuff_kd: 0 #0.0795774                #this is B3 is compliance shaping controller, Nms/Nm CHANGED
# reflected_motor_mass: 231         #in linear space (kg)
# reflected_motor_damping: 3465     #in linear space (Ns/m)
# reflected_motor_constant: 186.176 #in units of N/A
# max_dob_current: 0               
# spring_k: 562100.                 # SEA spring stiffness in linear frame N/m
# max_feedback_current: 2""") 
#     ds = DataSet("log_20190225T164334", csv=True, description=
# """\t Real test. Hand-excited via the arm/cuff as appropriate.
# This time I beefed up the inertia reduction aspect.
# IGNORE_CUFF
# Should be easier to move
# #Cuff Control Yaml
# ## Limit parameters ##
# max_dob_current: 3      
# dob_deadzone: 0.0        
# max_feedback_current: 7.
# ignore_cuff: True
# ## Chirp Parameters ##
# do_chirp: true
# chirp_low_freq_hz: 1
# chirp_high_freq_hz: 100.
# chirp_amplitude: 0.0             
# chirp_offset: 0.0
# chirp_duration_m: 1.0
# ## Model parameters ##
# reflected_motor_mass: 231.0 # in linear space (kg)
# reflected_motor_damping: 3465.0 # in linear space (Ns/m)
# reflected_motor_constant: 186.176 # in units of N/A
# spring_k: 562100. # SEA spring stiffness in linear frame N/m
# exo_inertia: 0.2814 # (Kg m^2)
# ## Controller Behavior ##
# desired_attenuated_inertia: 1.39692045e-01 # alpha^-1 \hat M_m (Kg m^2)
# desired_amplification_q_1: 1.15498337e-02 # amplification derivative component q_1 (1/s)
# desired_amplification_gain: 3.00000000e+00 # amplification gain  (alpha-1) (dimensionless)
# desired_SEA_zeta: 7.00000000e-01 # damping ratio of SEA's second order zero pair (dimensionless)
# desired_tilde_K_1: 0.00000000e+00 # virtual motor spring over mass (typically zero)
# desired_tilde_B_1: 4.71238898e+00 # virtual motor damping pole (rad s)
# ## Filter Parameters ##
# # lp_1: print_discrete_2(omega_hz=2.30000e+01, damping=2.00000e-01, time_step=0.001)
# torque_filter_lp_1_a_00: 9.89774032e-01
# torque_filter_lp_1_a_01: 9.68268028e-04
# torque_filter_lp_1_a_10: -2.02213898e+01
# torque_filter_lp_1_a_11: 9.33803004e-01
# torque_filter_lp_1_b_0: 1.02259678e-02
# torque_filter_lp_1_b_1: 2.02213898e+01
# torque_filter_lp_1_c_0: -2.02566536e+04
# torque_filter_lp_1_c_1: -6.63124363e+01
# torque_filter_lp_1_d: 2.02566536e+04
# # lp_2: print_discrete_2(omega_hz=1.40000e+01, damping=2.00000e-01, time_step=0.001)
# torque_filter_lp_2_a_00: 9.96178555e-01
# torque_filter_lp_2_a_01: 9.81344932e-04
# torque_filter_lp_2_a_10: -7.59342123e+00
# torque_filter_lp_2_a_11: 9.61649111e-01
# torque_filter_lp_2_b_0: 3.82144536e-03
# torque_filter_lp_2_b_1: 7.59342123e+00
# torque_filter_lp_2_c_0: -7.59832126e+03
# torque_filter_lp_2_c_1: -3.83756367e+01
# torque_filter_lp_2_d: 7.59832126e+03
# # bk_1: print_discrete_BK(B=4.71239e+00, K=0.00000e+00, time_step=0.001)
# torque_filter_bk_1_a_00: 1.00000000e+00
# torque_filter_bk_1_a_01: 9.97647502e-04
# torque_filter_bk_1_a_10: 0.00000000e+00
# torque_filter_bk_1_a_11: 9.95298697e-01
# torque_filter_bk_1_b_0: 4.99714350e-07
# torque_filter_bk_1_b_1: 9.97645152e-04
# torque_filter_bk_1_c_0: 0.00000000e+00
# torque_filter_bk_1_c_1: -4.70131417e+00
# torque_filter_bk_1_d: 9.97647502e-01
# # dob Q: print_discrete_2(omega_hz=2.50000e+01, damping=4.00000e-01, time_step=0.001)
# dob_Q_a_00: 9.88188042e-01
# dob_Q_a_01: 9.35860732e-04
# dob_Q_a_10: -2.30914380e+01
# dob_Q_a_11: 8.70584314e-01
# dob_Q_b_0: 1.18119582e-02
# dob_Q_b_1: 2.30914380e+01
# dob_Q_c_0: -2.31390227e+04
# dob_Q_c_1: -1.29682374e+02
# dob_Q_d: 2.31390227e+04
# """) 

#     ds = DataSet("log_20190304T133424", csv=True, description=
# """\t Modified cpp file to turn feedback controller off. Turned DOB off. Set chirp offset to basically compensate for gravity (found good value
# with guess and check). 

# #Cuff Control Yaml
# ## Limit parameters ##
# max_dob_current: 0 # 3                                                                    CHANGED      
# dob_deadzone: 0.0        
# max_feedback_current: 7.                                                       
# ignore_cuff: False
# ## Chirp Parameters ##
# do_chirp: true
# chirp_low_freq_hz: 1
# chirp_high_freq_hz: 1.
# chirp_amplitude: 2.             
# chirp_offset: -4.5
# chirp_duration_m: 1.0
# ## Model parameters ##
# reflected_motor_mass: 231.0 # in linear space (kg)
# reflected_motor_damping: 3465.0 # in linear space (Ns/m)
# reflected_motor_constant: 186.176 # in units of N/A
# spring_k: 562100. # SEA spring stiffness in linear frame N/m
# exo_inertia: 0.2814 # (Kg m^2)
# ## Controller Behavior ##
# desired_attenuated_inertia: 2.79384091e-01 # alpha^-1 \hat M_m (Kg m^2)
# desired_amplification_q_1: 1.47914779e-02 # amplification derivative component q_1 (1/s)
# desired_amplification_gain: 7.00000000e+00 # amplification gain  (alpha-1) (dimensionless)
# desired_SEA_zeta: 7.00000000e-01 # damping ratio of SEA's second order zero pair (dimensionless)
# desired_tilde_K_1: 0.00000000e+00 # virtual motor spring over mass (typically zero)
# desired_tilde_B_1: 4.71238898e+00 # virtual motor damping pole (rad s)
# ## Filter Parameters ##
# # lp_1: print_discrete_2(omega_hz=2.30000e+01, damping=2.00000e-01, time_step=0.001)
# torque_filter_lp_1_a_00: 9.89774032e-01
# torque_filter_lp_1_a_01: 9.68268028e-04
# torque_filter_lp_1_a_10: -2.02213898e+01
# torque_filter_lp_1_a_11: 9.33803004e-01
# torque_filter_lp_1_b_0: 1.02259678e-02
# torque_filter_lp_1_b_1: 2.02213898e+01
# torque_filter_lp_1_c_0: -2.02566536e+04
# torque_filter_lp_1_c_1: -6.63124363e+01
# torque_filter_lp_1_d: 2.02566536e+04
# # lp_2: print_discrete_2(omega_hz=1.40000e+01, damping=2.00000e-01, time_step=0.001)
# torque_filter_lp_2_a_00: 9.96178555e-01
# torque_filter_lp_2_a_01: 9.81344932e-04
# torque_filter_lp_2_a_10: -7.59342123e+00
# torque_filter_lp_2_a_11: 9.61649111e-01
# torque_filter_lp_2_b_0: 3.82144536e-03
# torque_filter_lp_2_b_1: 7.59342123e+00
# torque_filter_lp_2_c_0: -7.59832126e+03
# torque_filter_lp_2_c_1: -3.83756367e+01
# torque_filter_lp_2_d: 7.59832126e+03
# # bk_1: print_discrete_BK(B=4.71239e+00, K=0.00000e+00, time_step=0.001)
# torque_filter_bk_1_a_00: 1.00000000e+00
# torque_filter_bk_1_a_01: 9.97647502e-04
# torque_filter_bk_1_a_10: 0.00000000e+00
# torque_filter_bk_1_a_11: 9.95298697e-01
# torque_filter_bk_1_b_0: 4.99714350e-07
# torque_filter_bk_1_b_1: 9.97645152e-04
# torque_filter_bk_1_c_0: 0.00000000e+00
# torque_filter_bk_1_c_1: -4.70131417e+00
# torque_filter_bk_1_d: 9.97647502e-01
# # dob Q: print_discrete_2(omega_hz=2.50000e+01, damping=4.00000e-01, time_step=0.001)
# dob_Q_a_00: 9.88188042e-01
# dob_Q_a_01: 9.35860732e-04
# dob_Q_a_10: -2.30914380e+01
# dob_Q_a_11: 8.70584314e-01
# dob_Q_b_0: 1.18119582e-02
# dob_Q_b_1: 2.30914380e+01
# dob_Q_c_0: -2.31390227e+04
# dob_Q_c_1: -1.29682374e+02
# dob_Q_d: 2.31390227e+04
# """) 

#     ds = DataSet("log_20190304T134718", csv=True, description=
# """\t Modified cpp file to turn feedback controller off. Turned DOB back on. Had to increase chirp offset now that DOB is on, since stiction 
# no longer keeps the exo in place.
# #Cuff Control Yaml
# ## Limit parameters ##
# max_dob_current: 0 # 3                                                                    CHANGED      
# dob_deadzone: 0.0        
# max_feedback_current: 7.                                                       
# ignore_cuff: False
# ## Chirp Parameters ##
# do_chirp: true
# chirp_low_freq_hz: 1
# chirp_high_freq_hz: 1.
# chirp_amplitude: 2.             
# chirp_offset: -4.5
# chirp_duration_m: 1.0
# ## Model parameters ##
# reflected_motor_mass: 231.0 # in linear space (kg)
# reflected_motor_damping: 3465.0 # in linear space (Ns/m)
# reflected_motor_constant: 186.176 # in units of N/A
# spring_k: 562100. # SEA spring stiffness in linear frame N/m
# exo_inertia: 0.2814 # (Kg m^2)
# ## Controller Behavior ##
# desired_attenuated_inertia: 2.79384091e-01 # alpha^-1 \hat M_m (Kg m^2)
# desired_amplification_q_1: 1.47914779e-02 # amplification derivative component q_1 (1/s)
# desired_amplification_gain: 7.00000000e+00 # amplification gain  (alpha-1) (dimensionless)
# desired_SEA_zeta: 7.00000000e-01 # damping ratio of SEA's second order zero pair (dimensionless)
# desired_tilde_K_1: 0.00000000e+00 # virtual motor spring over mass (typically zero)
# desired_tilde_B_1: 4.71238898e+00 # virtual motor damping pole (rad s)
# ## Filter Parameters ##
# # lp_1: print_discrete_2(omega_hz=2.30000e+01, damping=2.00000e-01, time_step=0.001)
# torque_filter_lp_1_a_00: 9.89774032e-01
# torque_filter_lp_1_a_01: 9.68268028e-04
# torque_filter_lp_1_a_10: -2.02213898e+01
# torque_filter_lp_1_a_11: 9.33803004e-01
# torque_filter_lp_1_b_0: 1.02259678e-02
# torque_filter_lp_1_b_1: 2.02213898e+01
# torque_filter_lp_1_c_0: -2.02566536e+04
# torque_filter_lp_1_c_1: -6.63124363e+01
# torque_filter_lp_1_d: 2.02566536e+04
# # lp_2: print_discrete_2(omega_hz=1.40000e+01, damping=2.00000e-01, time_step=0.001)
# torque_filter_lp_2_a_00: 9.96178555e-01
# torque_filter_lp_2_a_01: 9.81344932e-04
# torque_filter_lp_2_a_10: -7.59342123e+00
# torque_filter_lp_2_a_11: 9.61649111e-01
# torque_filter_lp_2_b_0: 3.82144536e-03
# torque_filter_lp_2_b_1: 7.59342123e+00
# torque_filter_lp_2_c_0: -7.59832126e+03
# torque_filter_lp_2_c_1: -3.83756367e+01
# torque_filter_lp_2_d: 7.59832126e+03
# # bk_1: print_discrete_BK(B=4.71239e+00, K=0.00000e+00, time_step=0.001)
# torque_filter_bk_1_a_00: 1.00000000e+00
# torque_filter_bk_1_a_01: 9.97647502e-04
# torque_filter_bk_1_a_10: 0.00000000e+00
# torque_filter_bk_1_a_11: 9.95298697e-01
# torque_filter_bk_1_b_0: 4.99714350e-07
# torque_filter_bk_1_b_1: 9.97645152e-04
# torque_filter_bk_1_c_0: 0.00000000e+00
# torque_filter_bk_1_c_1: -4.70131417e+00
# torque_filter_bk_1_d: 9.97647502e-01
# # dob Q: print_discrete_2(omega_hz=2.50000e+01, damping=4.00000e-01, time_step=0.001)
# dob_Q_a_00: 9.88188042e-01
# dob_Q_a_01: 9.35860732e-04
# dob_Q_a_10: -2.30914380e+01
# dob_Q_a_11: 8.70584314e-01
# dob_Q_b_0: 1.18119582e-02
# dob_Q_b_1: 2.30914380e+01
# dob_Q_c_0: -2.31390227e+04
# dob_Q_c_1: -1.29682374e+02
# dob_Q_d: 2.31390227e+04
# """) 
    ds = DataSet("log_20190304T144110", csv=True, description=
"""\t Added a position controller to produce more consistent tests with no "drift" like with the chirp offset. DOB off.
#Cuff Control Yaml
## Limit parameters ##
max_dob_current: 0 # 3.                                                                          
dob_deadzone: 0.0        
max_feedback_current: 7.                                                       
ignore_cuff: False
## Chirp Parameters ##
do_chirp: true
chirp_low_freq_hz: 1
chirp_high_freq_hz: 1.
chirp_amplitude: 2.5             
chirp_offset: 0.
chirp_duration_m: 1.0
## Model parameters ##
reflected_motor_mass: 231.0 # in linear space (kg)
reflected_motor_damping: 3465.0 # in linear space (Ns/m)
reflected_motor_constant: 186.176 # in units of N/A
spring_k: 562100. # SEA spring stiffness in linear frame N/m
exo_inertia: 0.2814 # (Kg m^2)
## Controller Behavior ##
desired_attenuated_inertia: 2.79384091e-01 # alpha^-1 \hat M_m (Kg m^2)
desired_amplification_q_1: 1.47914779e-02 # amplification derivative component q_1 (1/s)
desired_amplification_gain: 7.00000000e+00 # amplification gain  (alpha-1) (dimensionless)
desired_SEA_zeta: 7.00000000e-01 # damping ratio of SEA's second order zero pair (dimensionless)
desired_tilde_K_1: 0.00000000e+00 # virtual motor spring over mass (typically zero)
desired_tilde_B_1: 4.71238898e+00 # virtual motor damping pole (rad s)
## Filter Parameters ##
# lp_1: print_discrete_2(omega_hz=2.30000e+01, damping=2.00000e-01, time_step=0.001)
torque_filter_lp_1_a_00: 9.89774032e-01
torque_filter_lp_1_a_01: 9.68268028e-04
torque_filter_lp_1_a_10: -2.02213898e+01
torque_filter_lp_1_a_11: 9.33803004e-01
torque_filter_lp_1_b_0: 1.02259678e-02
torque_filter_lp_1_b_1: 2.02213898e+01
torque_filter_lp_1_c_0: -2.02566536e+04
torque_filter_lp_1_c_1: -6.63124363e+01
torque_filter_lp_1_d: 2.02566536e+04
# lp_2: print_discrete_2(omega_hz=1.40000e+01, damping=2.00000e-01, time_step=0.001)
torque_filter_lp_2_a_00: 9.96178555e-01
torque_filter_lp_2_a_01: 9.81344932e-04
torque_filter_lp_2_a_10: -7.59342123e+00
torque_filter_lp_2_a_11: 9.61649111e-01
torque_filter_lp_2_b_0: 3.82144536e-03
torque_filter_lp_2_b_1: 7.59342123e+00
torque_filter_lp_2_c_0: -7.59832126e+03
torque_filter_lp_2_c_1: -3.83756367e+01
torque_filter_lp_2_d: 7.59832126e+03
# bk_1: print_discrete_BK(B=4.71239e+00, K=0.00000e+00, time_step=0.001)
torque_filter_bk_1_a_00: 1.00000000e+00
torque_filter_bk_1_a_01: 9.97647502e-04
torque_filter_bk_1_a_10: 0.00000000e+00
torque_filter_bk_1_a_11: 9.95298697e-01
torque_filter_bk_1_b_0: 4.99714350e-07
torque_filter_bk_1_b_1: 9.97645152e-04
torque_filter_bk_1_c_0: 0.00000000e+00
torque_filter_bk_1_c_1: -4.70131417e+00
torque_filter_bk_1_d: 9.97647502e-01
# dob Q: print_discrete_2(omega_hz=2.50000e+01, damping=4.00000e-01, time_step=0.001)
dob_Q_a_00: 9.88188042e-01
dob_Q_a_01: 9.35860732e-04
dob_Q_a_10: -2.30914380e+01
dob_Q_a_11: 8.70584314e-01
dob_Q_b_0: 1.18119582e-02
dob_Q_b_1: 2.30914380e+01
dob_Q_c_0: -2.31390227e+04
dob_Q_c_1: -1.29682374e+02
dob_Q_d: 2.31390227e+04


position_k: 50
""") 
    ds = DataSet("log_20190304T144237", csv=True, description=
"""\t Added a position controller to produce more consistent tests with no "drift" like with the chirp offset. DOB on.
#Cuff Control Yaml
## Limit parameters ##
max_dob_current:  3.                                                                          
dob_deadzone: 0.0        
max_feedback_current: 7.                                                       
ignore_cuff: False
## Chirp Parameters ##
do_chirp: true
chirp_low_freq_hz: 1
chirp_high_freq_hz: 1.
chirp_amplitude: 2.5             
chirp_offset: 0.
chirp_duration_m: 1.0
## Model parameters ##
reflected_motor_mass: 231.0 # in linear space (kg)
reflected_motor_damping: 3465.0 # in linear space (Ns/m)
reflected_motor_constant: 186.176 # in units of N/A
spring_k: 562100. # SEA spring stiffness in linear frame N/m
exo_inertia: 0.2814 # (Kg m^2)
## Controller Behavior ##
desired_attenuated_inertia: 2.79384091e-01 # alpha^-1 \hat M_m (Kg m^2)
desired_amplification_q_1: 1.47914779e-02 # amplification derivative component q_1 (1/s)
desired_amplification_gain: 7.00000000e+00 # amplification gain  (alpha-1) (dimensionless)
desired_SEA_zeta: 7.00000000e-01 # damping ratio of SEA's second order zero pair (dimensionless)
desired_tilde_K_1: 0.00000000e+00 # virtual motor spring over mass (typically zero)
desired_tilde_B_1: 4.71238898e+00 # virtual motor damping pole (rad s)
## Filter Parameters ##
# lp_1: print_discrete_2(omega_hz=2.30000e+01, damping=2.00000e-01, time_step=0.001)
torque_filter_lp_1_a_00: 9.89774032e-01
torque_filter_lp_1_a_01: 9.68268028e-04
torque_filter_lp_1_a_10: -2.02213898e+01
torque_filter_lp_1_a_11: 9.33803004e-01
torque_filter_lp_1_b_0: 1.02259678e-02
torque_filter_lp_1_b_1: 2.02213898e+01
torque_filter_lp_1_c_0: -2.02566536e+04
torque_filter_lp_1_c_1: -6.63124363e+01
torque_filter_lp_1_d: 2.02566536e+04
# lp_2: print_discrete_2(omega_hz=1.40000e+01, damping=2.00000e-01, time_step=0.001)
torque_filter_lp_2_a_00: 9.96178555e-01
torque_filter_lp_2_a_01: 9.81344932e-04
torque_filter_lp_2_a_10: -7.59342123e+00
torque_filter_lp_2_a_11: 9.61649111e-01
torque_filter_lp_2_b_0: 3.82144536e-03
torque_filter_lp_2_b_1: 7.59342123e+00
torque_filter_lp_2_c_0: -7.59832126e+03
torque_filter_lp_2_c_1: -3.83756367e+01
torque_filter_lp_2_d: 7.59832126e+03
# bk_1: print_discrete_BK(B=4.71239e+00, K=0.00000e+00, time_step=0.001)
torque_filter_bk_1_a_00: 1.00000000e+00
torque_filter_bk_1_a_01: 9.97647502e-04
torque_filter_bk_1_a_10: 0.00000000e+00
torque_filter_bk_1_a_11: 9.95298697e-01
torque_filter_bk_1_b_0: 4.99714350e-07
torque_filter_bk_1_b_1: 9.97645152e-04
torque_filter_bk_1_c_0: 0.00000000e+00
torque_filter_bk_1_c_1: -4.70131417e+00
torque_filter_bk_1_d: 9.97647502e-01
# dob Q: print_discrete_2(omega_hz=2.50000e+01, damping=4.00000e-01, time_step=0.001)
dob_Q_a_00: 9.88188042e-01
dob_Q_a_01: 9.35860732e-04
dob_Q_a_10: -2.30914380e+01
dob_Q_a_11: 8.70584314e-01
dob_Q_b_0: 1.18119582e-02
dob_Q_b_1: 2.30914380e+01
dob_Q_c_0: -2.31390227e+04
dob_Q_c_1: -1.29682374e+02
dob_Q_d: 2.31390227e+04


position_k: 50
""") 

    # mag_ax, phase_ax = empty_bode_plot(suptitle="Actuator Compliance")
    # tf_A_over_B_phase(data_off.joint, data_off.inf_motor_torque, 
    #     ax=phase_ax, label="joint / motor torque")
    # tf_A_over_B(data_off.joint, data_off.inf_motor_torque, 
    #     ax=mag_ax, label="joint / motor torque")
    # tf_A_over_B_phase(data_off.joint, data_off.torque, 
    #     ax=phase_ax, label="joint / spring torque")
    # tf_A_over_B(data_off.joint, data_off.torque, 
    #     ax=mag_ax, label="joint / spring torque")
    # tf_A_over_B_phase(data_off.joint, data_off.loadtorque, 
    #     ax=phase_ax, label="joint / load torque")
    # tf_A_over_B(data_off.joint, data_off.loadtorque, 
    #     ax=mag_ax, label="joint / load torque")
    # mag_ax.legend()
    if(False):
        plt.figure()
        plt.plot(*plot_discrete(ds.torque), label="torque")
        plt.plot(*plot_discrete(ds.loadtorque), label="loadtorque")
        plt.plot(*plot_discrete(ds.current), label="current")
        plt.plot(*plot_discrete(ds.cmd), label="cmd")
        plt.plot(*plot_discrete(ds.actuator), label="actuator")
        plt.plot(*plot_discrete(ds.motor), label="motor")
        plt.plot(*plot_discrete(ds.motor_vel), label="motor_vel")
        plt.plot(*plot_discrete(ds.joint), label="joint")
        plt.legend()

        # plot_admittance(data.force, data.infloadforce, suptitle="Spring")
        # plot_admittance(data.motor, data.infloadforce, suptitle="Motor")
        # plot_admittance(data.actuator, data.infloadforce, suptitle="Actuator")
        plt.show()


if __name__ == '__main__':
    main()