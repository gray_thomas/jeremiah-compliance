""" Plots transfer functions representing time-delay proportional feedback."""
import numpy as np
import matplotlib.pyplot as plt
from admittance_plotting_utils import get_cross_and_power, fft_plot
from python_for_filter_constants import *

def combine_series(sys_in,series):
    compliance,input_compliance = sys_in
    compliance_out = lambda s: compliance(s) + series(s)
    
    
    return (compliance_out,input_compliance)
    
def combine_parallel(sys_in,parallel):
    compliance,input_compliance = sys_in
    compliance_out = lambda s: 1/(1/compliance(s) + 1/parallel(s))
    
    input_compliance_out = lambda s: (compliance_out(s)/compliance(s)
                                     * input_compliance(s))
    
    
    return (compliance_out, input_compliance_out)
    
def virtual_parallel(sys_in, position_controller):
    compliance, input_compliance = sys_in
    parallel_out = lambda s: -1/position_controller(s) * compliance(s)/input_compliance(s)
    return parallel_out
    
def virtual_series(sys_in, force_controller):
    compliance, input_compliance = sys_in
    series_out = lambda s: input_compliance(s) * force_controller(s)
    return series_out

class DoubleBode(object):
    def __init__(self):
        self.compliance_bode = BodePlot()
        self.input_bode = BodePlot()
    
    def plot_system(self,system, name = 'name', **kwargs):
        
        self.compliance_bode.plot_system(system[0], suptitle = 'Compliance', name = name, **kwargs)
        self.input_bode.plot_system(system[1], suptitle = 'Input Compliance', name = name, **kwargs)
   
def fix_overphase(freqs, phases):
    nf, np = [], []
    p0=phases[0]
    for f, p in zip(freqs, phases):
        if (p0>175 and p<-175) or (p0<-175 and p>175):
            nf.append(f), np.append(float("nan"))
        nf.append(f), np.append(p)
        p0=p
    return nf, np
        
class BodePlot(object):
    def __init__(self):
        self.mag_ax = None
        self.phase_ax = None
        self.fig = None
        self.frequency = None
        self.omegas = None
        self.laplace_s_list = None
    
    def plot_system(self,system,suptitle = 'title', name = 'name', **kwargs):
        
        self.lazy_init_plot()
        self.mag_ax.loglog(self.frequency, abs(system(self.laplace_s_list)), label = name, **kwargs)
        self.phase_ax.semilogx(*fix_overphase(self.frequency, 180/np.pi*np.angle(system(self.laplace_s_list))), label = name, **kwargs)

        self.mag_ax.set_ylabel("magnitude")
        self.phase_ax.set_ylabel("phase (deg)")
        self.phase_ax.set_xlabel("frequncy (Hz)")
        self.phase_ax.set_yticks([-180, -90, 0, 90, 180])
        self.phase_ax.grid(True, 'both')
        self.mag_ax.grid(True, 'both')

        self.fig.suptitle(suptitle)
        self.phase_ax.legend()
        
    def scatter(self, signalA, signalB, **kwargs):
        self.lazy_init_plot()
        cross, power, f = get_cross_and_power(signalA, signalB)
        transfer = cross / power
        self.mag_ax.loglog(f, abs(transfer), **kwargs)
        self.phase_ax.semilogx(f, 180./np.pi*np.angle(transfer), **kwargs)  
        self.phase_ax.legend()
    
    def lazy_init_plot(self):
        if np.any(np.array([var is None for var in [
                self.mag_ax, self.phase_ax, self.fig, self.frequency, 
                self.omegas, self.laplace_s_list
                ]])):
            self.init_plot()
    def init_plot(self, freqs = [-1,4,4000]):
        self.fig, (self.mag_ax, self.phase_ax) = plt.subplots(2,1,
                                            sharex=True)
        self.frequency = np.logspace(*freqs)
        self.omegas = np.pi*2*self.frequency
        self.laplace_s_list = complex(0, 1) * self.omegas
   

def butterworth_poly_2(butterworth_lambda_hz):
    l = butterworth_lambda_hz*2*np.pi
    return lambda s: (s/l)**2+1.414*(s/l)+1

def butterworth_poly_3(butterworth_lambda_hz):
    l = butterworth_lambda_hz*2*np.pi
    return lambda s: (s/l)**3+2*(s/l)**2+2*(s/l)+1

def main(arg_jac=0.02):
    ## Parameters ##
    lin_m_m = 231 # reflected motor inertia (Kg = Ns^2/m)
    lin_b_m = 3465 # reflected motor damping (Ns/m)
    lin_g_m = 186.176 # reflected motor constant (N/amp)
    lin_k_s = 562100 # SEA spring constant (N/m)
    j_e = .1399 # exoskeleton inertia (Nm s^2/rad = Kg m^2)
    # extra_j_e = .1415 # adding the 660 g weight to the arm (Nm s^2/rad = Kg m^2)
    # extra_j_e = 5*.1415 # even more
    extra_j_e = 4560/660*.1415 # even more
    j_e += extra_j_e
    # b_e = 1.1 # exoskeleton damping (Nm s/rad)
    k_c = 250 # Cuff spring (Nm/rad)
    jacobian = arg_jac # transmission ratio (m)
    j_h = .01377 # Kg m^2
    time_delay = .004 # time delay (s) (tuned by finding out when controllers go unstable (Kp1000 and Kd10 both unstable) (Kp700 + Kd7 stable))

    ## Adjusted parmeters ##
    j_m = lin_m_m * jacobian**2
    b_m = lin_b_m * jacobian**2
    k_s = lin_k_s * jacobian**2

    ## Filtering Parameters ##
    lp_1_w = 2*np.pi*23 # rad s
    lp_1_z = .2 # damping ratio
    lp_1 = lambda s: lp_1_w**2/(s**2+2*s*lp_1_z*lp_1_w+lp_1_w**2)
    lp_2_w = 2*np.pi*14 # rad s
    lp_2_z = .2 # damping ratio
    lp_2 = lambda s: lp_2_w**2/(s**2+2*s*lp_2_z*lp_2_w+lp_2_w**2)
    spring_force_derivative_pole_hz = 50
    lp_spring = lambda s: 1./((s/(2*np.pi*spring_force_derivative_pole_hz))+1)
    cuff_builtin_pole_hz = 500
    lp_cuff_builtin = lambda s: 1./((s/(2*np.pi*cuff_builtin_pole_hz))+1)

    ## Controller Meta Parameters ##
    # Tests 80, 81: alpha = 8, alpha_hat_J_m = j_e/alpha*2, alpha_hat_B_m = alpha_hat_J_m * 2* np.pi*.75 , hat_zeta_nominal = .3, sea_zeta = .7
    # Tests 82, 83: alpha = 4, alpha_hat_J_m = j_e/alpha, alpha_hat_B_m = alpha_hat_J_m * 2* np.pi*.75 , hat_zeta_nominal = .3, sea_zeta = .7
    alpha = 8 # amplification ratio
    alpha_hat_J_m = j_e / alpha *2  # amplified inertia (Kg m^2)
    alpha_hat_B_m = alpha_hat_J_m * 2*np.pi*(2) # amplified damping (Nm s/rad)
    hat_zeta_nominal = .3 # damping ratio for amplified compliance zeros
    sea_zeta = .7/2 # damping ratio for SEA zeros

    ## visualization tools
    virtual_motor_compliance = lambda s: 1/(s**2*alpha_hat_J_m*alpha + s*alpha_hat_B_m*alpha )




    ## Algebra to compute gains ##
    hat_J_m = alpha_hat_J_m*alpha
    hat_B_m = alpha_hat_B_m*alpha
    q_1 = (2*hat_zeta_nominal*np.sqrt(k_c*alpha_hat_J_m)-alpha_hat_B_m)/k_c # zero in cuff feedback
    nominal_human_compliance = lambda s: (1+q_1*s)/(s**2*alpha_hat_J_m + s*alpha_hat_B_m)+1/k_c
    human_compliance_w_lags = lambda s: ((1+q_1*s )/(s**2*alpha_hat_J_m + s*alpha_hat_B_m)*np.exp(-s*time_delay)
        *lp_1(s)*lp_2(s)+1/k_c)
    # q_1 = 1/(2*np.pi*15)
    C_h_nominal = lambda s: 1/k_c + (1+q_1*s)/(alpha_hat_J_m*s*s+alpha_hat_B_m*s)
    C_h_test_1 = lambda s: 1/k_c + (
        (1+q_1*s)/(alpha_hat_J_m*s*s+alpha_hat_B_m*s)
        *np.exp(-s*time_delay)
        *lp_1(s)*lp_2(s)
        )
    S6_test = lambda s: (
        (1+q_1*s)/(alpha_hat_J_m*s*s+alpha_hat_B_m*s)
        *np.exp(-s*time_delay)
        *lp_1(s)*lp_2(s)
        )
    tilde_B_1 = alpha_hat_B_m/alpha_hat_J_m
    tilde_K_1 = 0 # based on exoskeleton no-spring objective
    tilde_B_2 = 2*sea_zeta*np.sqrt(k_s/hat_J_m)
    tilde_K_2 = k_s/hat_J_m
    C_sea = lambda s: 1/(k_s)*(s**2+tilde_B_2*s+tilde_K_2)/(s**2+tilde_B_1*s+tilde_K_1)
    S4_test = lambda s: (
        1/(k_s) 
        + 1/(j_m*(s**2+tilde_B_1*s+tilde_K_1+.01)) 
        + np.exp(-s*time_delay)*((tilde_B_2-tilde_B_1)*s*lp_spring(s) + (tilde_K_2-tilde_K_1-k_s/j_m))/(s**2+tilde_B_1*s+tilde_K_1)/(k_s)
        )

    K_1 = 0 # gain from joint-space motor displacement to joint-space motor torque
    B_1 = tilde_B_1*j_m-b_m # Nm s/rad
    K_2 = (tilde_K_2*j_m - K_1 - k_s)/(k_s) # Nm/Nm
    B_2 = (tilde_B_2*j_m - B_1 - b_m)/(k_s) # don't forget the b_m
    C_sea2 = lambda s: 1/(k_s)+(1+np.exp(-s*time_delay)*(K_2+B_2*s*lp_spring(s)))/(j_m*s**2+b_m*s + (s*B_1+K_1))

    print("tilde_B_1", tilde_B_1)

    ## Controller joint space transfer functions ##
    tau_m_per_js_motor_angle = lambda s: -K_1-B_1*s
    tau_m_per_js_spring_torque = lambda s: K_2+B_2*s*lp_spring(s)
    tau_m_per_js_cuff_torque = lambda s: (j_m/hat_J_m)*(1+
        j_e/(k_s)
        *(s**2*lp_2(s))
        *(s**2+tilde_B_2*s+tilde_K_2)/(s**2+tilde_B_1*s+tilde_K_1))*(
        (alpha-1)*(1+q_1*s)*lp_1(s))
    tau_m_per_js_cuff_torque = lambda s: (j_m/hat_J_m)*(1+
        j_e/(k_s)
        *(s*lp_2(s))
        *(s**2+tilde_B_2*s+tilde_K_2)*lp_2(s)/(s+tilde_B_1))*(
        (alpha-1)*(1+q_1*s)*lp_1(s))

    low_pass = lambda f, zeta: lambda s: 1/((s/(2*np.pi*f))**2+2*zeta*(s/(2*np.pi*f))+1)
    # tau_m_per_js_cuff_torque = lambda s: .5*low_pass(9.9,.15)(s)/low_pass(5.2,.15)(s)  #*(s/(2*np.pi*5)+1)/(s/(2*np.pi*20)+1)
    # tau_m_per_js_cuff_torque = lambda s: (.5
    # *low_pass(9.9,.15)(s)/low_pass(5.2,.15)(s)
    # *low_pass(1,.05)(s)*low_pass(10,.05)(s)  ) #*(s/(2*np.pi*5)+1)/(s/(2*np.pi*20)+1)
    ## Convert to amps
    motor_current_per_js_motor_angle = lambda s: 1/lin_g_m/jacobian*tau_m_per_js_motor_angle(s)
    motor_current_per_js_spring_torque = lambda s: 1/lin_g_m/jacobian*tau_m_per_js_spring_torque(s)
    motor_current_per_js_cuff_torque = lambda s: 1/lin_g_m/jacobian*tau_m_per_js_cuff_torque(s)


    # rdouble_bode = DoubleBode()
    bode=BodePlot()
    bode.init_plot(freqs = [-1,np.log(500)/np.log(10),4000])

    
    rsystem1 = (lambda s: (1/(b_m*s + j_m*s*s)),
               lambda s: (np.exp(-time_delay * s)*lin_g_m*jacobian/(b_m*s + j_m*s*s))) 
    rparallel1 = virtual_parallel(rsystem1, motor_current_per_js_motor_angle)
    rsystem2 = combine_parallel(rsystem1, rparallel1)
    rseries2 = virtual_series(rsystem2, motor_current_per_js_spring_torque)
    rsystem3 = combine_series(rsystem2, rseries2)
    rseries3 = lambda s: 1/(k_s+0*s)
    rsystem4 = combine_series(rsystem3, rseries3)
    rparallel4 = lambda s: 1/(j_e*s*s)
    rsystem5 = combine_parallel(rsystem4, rparallel4)
    rseries5 = virtual_series(rsystem5, motor_current_per_js_cuff_torque)
    rsystem6 = combine_series(rsystem5, rseries5)
    rseries6 = lambda s: 1/(0*s+k_c)
    rsystem7 = combine_series(rsystem6, rseries6)
    amplification = lambda s: (rsystem6[0](s)-rsystem5[0](s))/rsystem5[0](s)
    

    # rdouble_bode.plot_system(rsystem1, name = 'Rotary System 1')
    # rdouble_bode.plot_system(rsystem2, name = 'Rotary System 2')
    # rdouble_bode.plot_system(rsystem3, name = 'Rotary System 3')
    bode.plot_system(rsystem5[0], name = 'C 5--vs. env. (inc. lags)', lw=3, dashes=(4,1,.01,1), dash_capstyle="round")
    # bode.plot_system(lambda s: rsystem5[1](s)*1/lin_g_m/jacobian, name = 'Input compliance 5')
    # bode.plot_system(lambda s: rsystem5[1](s)*1/lin_g_m/jacobian*tau_m_per_js_cuff_torque(s), name = 'Adjusted input compliance 5')
    # bode.plot_system(rsystem6[0], name = 'Rotary System 6')
    bode.plot_system(rsystem7[0], name = 'C 7--vs. hum. (inc. lags)', lw=3, dashes=(3,1,.01,1,.01,1), dash_capstyle="round")
    # bode.plot_system(rsystem4[0], name = 'Rotary System 4')
    bode.plot_system(virtual_motor_compliance, name="virtual_motor_compliance", linestyle=":", dash_capstyle="round", suptitle="Compliance")

    bode.plot_system(nominal_human_compliance, name="nominal_human_compliance", linestyle=":", dash_capstyle="round", suptitle="Compliance")
    bode.plot_system(C_sea, name="nominal_SEA_compliance", linestyle="-", dash_capstyle="round", suptitle="Compliance")
    # bode.plot_system(human_compliance_w_lags, name="human_compliance_w_lags", linestyle=":", dash_capstyle="round", suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(rseries3, name="SEA Spring", linestyle=":", suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(rparallel4, name="Exo Mass", linestyle=":", suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(rseries6, name="Cuff Spring", linestyle=":", suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(amplification, name="Amplification", linestyle=":", suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(force_adjustment, name="Force Adjustement", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(force_adjustment2, name="Force Adjustement?", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(C_h_nominal, name="C_h_nominal", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(C_h_test_1, name="C_h_test", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(S6_test, name="S6_test", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(C_sea, name="C_sea", dashes=(3,1,.1,1,.1,1), lw=2, dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(rsystem4[0], name="rsystem4", dashes=(2.75,1,.1,1,.1,1), lw=2, dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(S4_test, name="S4_test", dashes=(2.5,1,.1,1,.1,1), lw=2, dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(C_sea2, name="C_sea2", dashes=(2.5,.5,.1,.5,.1,1), lw=2, dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(lambda s: 1/(j_h*s*s), name="Human inertia", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(lambda s: 1/(j_h*s*s + 1/C_h_test_1(s)), name="Human inertia test", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(lambda s: 1/(j_h*s*s*2 + 1/C_h_test_1(s)), name="Human inertia test", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # bode.plot_system(lambda s: 1/(j_h*s*s/2 + 1/C_h_test_1(s)), name="Human inertia test", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(lambda s: -np.exp(-.004*s), name="-time delay", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    
    print("## Controller Behavior ##")
    print("desired_attenuated_inertia: %.8e # alpha^-1 \hat M_m (Kg m^2)"%alpha_hat_J_m)
    print("desired_amplification_q_1: %.8e # amplification derivative component q_1 (1/s)"%q_1)
    print("desired_amplification_gain: %.8e # amplification gain  (alpha-1) (dimensionless)"%(alpha-1))
    print("desired_SEA_zeta: %.8e # damping ratio of SEA's second order zero pair (dimensionless)"%sea_zeta)
    print("desired_tilde_K_1: %.8e # virtual motor spring over mass (typically zero)"%tilde_K_1)
    print("desired_tilde_B_1: %.8e # virtual motor damping pole (rad s)"%tilde_B_1)
    print("## Filter Parameters ##")
    print ("# lp_1: print_discrete_2(omega_hz=%.5e, damping=%.5e, time_step=0.001)"%(lp_1_w/2/np.pi, lp_1_z))
    print_discrete_2(omega_hz=lp_1_w/2/np.pi, damping=lp_1_z, time_step=0.001, plotting=False, prestring="torque_filter_lp_1")
    print ("# lp_2: print_discrete_2(omega_hz=%.5e, damping=%.5e, time_step=0.001)"%(lp_2_w/2/np.pi, lp_2_z))
    print_discrete_2(omega_hz=lp_2_w/2/np.pi, damping=lp_2_z, time_step=0.001, plotting=False, prestring="torque_filter_lp_2")
    print ("# bk_1: print_discrete_BK(B=%.5e, K=%.5e, time_step=0.001)"%(tilde_B_1, tilde_K_1))
    print_discrete_BK(B=tilde_B_1, K=tilde_K_1, time_step=0.001, plotting=False, prestring="torque_filter_bk_1")

    if False: # debug cuff senstivity
        bode=BodePlot()
        bode.init_plot(freqs = [-1,np.log(500)/np.log(10),4000])
        bode.plot_system(tau_m_per_js_cuff_torque, name='tau_m_per_js_cuff_torque')
        bode.plot_system(tau_m_per_js_spring_torque, name='tau_m_per_js_spring_torque')
        bode.plot_system(tau_m_per_js_motor_angle, name='tau_m_per_js_motor_angle')
    dob_f_hz = 25
    dob_zeta = 0.4
    if False: # debug DOB stability
        Q = lambda s: 1/((s/(2*np.pi*dob_f_hz))**2 + s/(2*np.pi*dob_f_hz)*2*dob_zeta + 1)
        fb = lambda s: Q(s)*np.exp(-time_delay*s)/(1-Q(s))

        bode=BodePlot()
        bode.init_plot(freqs = [-1,np.log(500)/np.log(10),4000])
        bode.plot_system(fb, name='feedback')
        bode.plot_system(lambda s: Q(s)/(1-Q(s))*np.exp(-s*time_delay), name='Q/(1-Q)e^{-st}')
        bode.plot_system(lambda s: 1/(1+1/fb(s)), name='closed loop')
        bode.plot_system(lambda s: Q(s)*np.exp(-s*time_delay)/(1+Q(s)*(np.exp(-s*time_delay)-1)), name='closed loop')
        # bode.plot_system(Q, name='Q')

    print ("# dob Q: print_discrete_2(omega_hz=%.5e, damping=%.5e, time_step=0.001)"%(dob_f_hz, dob_zeta))
    print_discrete_2(omega_hz=dob_f_hz, damping=dob_zeta, time_step=0.001, plotting=False, prestring="dob_Q")

    plt.show()


if __name__ == '__main__':
    main(arg_jac=-0.02)
    # main(arg_jac=0.027)
    # main(arg_jac=0.005)
