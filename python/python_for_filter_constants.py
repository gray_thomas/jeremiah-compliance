import numpy as np
import scipy.linalg as linalg

import matplotlib.pyplot as plt

from six_systems import BodePlot

default_bode = BodePlot()

def plot_TF(A, B):

	tf = np.vectorize(lambda s: np.linalg.solve(s*np.eye(A.shape[0])-A, B)[0,0])
	default_bode.plot_system(tf)
	pass

def plot_DTF(A, B):

	tf_z = np.vectorize(lambda z: np.linalg.solve(z*np.eye(A.shape[0])-A, B)[0,0])
	tf_s = lambda s: tf_z(np.exp(s*0.001))
	default_bode.plot_system(tf_s)
	pass

def plot_TF_CD(A, B, C, D):

	tf = np.vectorize(lambda s: D + C.dot(np.linalg.solve(s*np.eye(A.shape[0])-A, B))[0,0])
	default_bode.plot_system(tf,label="acc(s)")

def plot_DTF_CD(A, B, C, D):

	tf_z = np.vectorize(lambda z: D + C.dot(np.linalg.solve(z*np.eye(A.shape[0])-A, B))[0,0])
	tf_s = lambda s: tf_z(np.exp(s*0.001))
	default_bode.plot_system(tf_s, label="acc(z)")

	C0 = np.array([[1, -B[0]/B[1]]])
	C = C0.dot(A.dot(A)-2*A+np.eye(A.shape[0]))/0.001**2
	D = C0.dot(A.dot(B))/0.001**2
	tf_z = np.vectorize(lambda z: D + C.dot(np.linalg.solve(z*np.eye(A.shape[0])-A, B))[0,0])
	tf_s = lambda s: tf_z(np.exp(s*0.001))
	default_bode.plot_system(tf_s, label="cd adjust")

	# tf_z = np.vectorize(lambda z: 1-(np.linalg.solve(z*np.eye(A.shape[0])-A, B))[0,0])
	# tf_s = lambda s: tf_z(np.exp(s*0.001))
	# default_bode.plot_system(tf_s)

	# p1, p2 = np.linalg.eig(A)[0]
	# print(p1, p2)
	# A = np.array([[0,1],[-(p1*p2).real, (p1+p2).real]])
	# B = np.array([[0],[1]])
	# C = np.array([[1-(p1*p2).real, (p1+p2).real-2]])
	# D = 1
	# tf_z = np.vectorize(lambda z: D + C.dot(np.linalg.solve(z*np.eye(A.shape[0])-A, B))[0,0])
	# tf_s = lambda s: tf_z(np.exp(s*0.001))
	# default_bode.plot_system(tf_s, label='new')


	pass

def print_discrete_AB(alpha_hz=20, lambda_hz=5, time_step=.001, plotting=False):
	# third order butterworth Q filter
	alpha_rads = alpha_hz*2*np.pi
	lambda_rads = lambda_hz*2*np.pi
	# gray's fancy new poly: ((s/a)^2+(s/a)+1)(s/lambda)+1 
	a_mat = np.array([[0,1,0],[0,0,1],[-alpha_rads**2*lambda_rads, -alpha_rads**2, -alpha_rads]])
	b_mat = np.array([[0],[0],[alpha_rads**2*lambda_rads]])
	
	a_disc, b_disc = discretize(a_mat, b_mat, time_step)

	if plotting:
		plot_TF(a_mat, b_mat)
		plot_DTF(a_disc, b_disc)

	print("\n\t"+"//print_discrete_AB(alpha_hz=%f, lambda_hz=%f, time_step=%f)"%(alpha_hz, lambda_hz, time_step))
	render_AB(a_disc, b_disc)

def print_discrete_butterworth(alpha_hz=20, time_step=.001, plotting=False):
	# third order butterworth Q filter
	alpha_rads = alpha_hz*2*np.pi
	# old poly: (s/a)^3 + 2 (s/a)^2 + 2 (s/a)^1 + 1
	a_mat = np.array([[0,1,0],[0,0,1],[-alpha_rads**3, -2*alpha_rads**2, -2*alpha_rads]])
	b_mat = np.array([[0],[0],[alpha_rads**3]])

	a_disc, b_disc = discretize(a_mat, b_mat, time_step)

	if plotting:
		plot_TF(a_mat, b_mat)
		plot_DTF(a_disc, b_disc)

	print("\n\t"+"//print_discrete_butterworth(alpha_hz=%f, time_step=%f)"%(alpha_hz, time_step))
	render_AB(a_disc, b_disc)
	

def print_discrete_bessel(alpha_hz=20, time_step=.001, plotting=False):
	alpha_rads = alpha_hz*2*np.pi
	a_mat = np.array([[0,1,0],[0,0,1],[-15*alpha_rads**3, -15*alpha_rads**2, -6*alpha_rads]])
	b_mat = np.array([[0],[0],[15*alpha_rads**3]])

	a_disc, b_disc = discretize(a_mat, b_mat, time_step)

	if plotting:
		plot_TF(a_mat, b_mat)
		plot_DTF(a_disc, b_disc)

	print("\n\t"+"//print_discrete_bessel(alpha_hz=%f, time_step=%f)"%(alpha_hz, time_step))
	render_AB(a_disc, b_disc)


def print_discrete_2(omega_hz=20, damping=1.0, time_step=.001, plotting=False, prestring=None):
	# second order butterworth Q filter
	omega_rads = omega_hz*2*np.pi
	# equation: (s^2 + 2*zeta*omega * s + omega^2) x = omega^2 * u
	a_mat = np.array([[0,1],[-omega_rads**2 , -2*omega_rads*damping]])
	b_mat = np.array([[0],[omega_rads**2]])
	c_mat = np.array([[-omega_rads**2 , -2*omega_rads*damping]])
	d_mat = np.array([[omega_rads**2]])

	a_disc, b_disc = discretize(a_mat, b_mat, time_step)
	c_disc, d_disc = cd_disc(a_disc, b_disc)

	if plotting:
		plot_TF(a_mat, b_mat)
		plot_DTF(a_disc, b_disc)
		plot_TF_CD(a_mat, b_mat, c_mat, d_mat)
		plot_DTF_CD(a_disc, b_disc, c_disc, d_disc)
		# plot_DTF(a_disc.T, b_disc)
		plot_DTF_CD(a_disc.T, b_disc, c_disc, d_disc)

	if prestring is None:
		print("\n\t"+"//print_discrete_2(omega_hz=%f, damping=%f, time_step=%f)"%(omega_hz, damping, time_step))
		render_ABCD(a_disc, b_disc, c_disc, d_disc)
	else: # YAML file output
		render_ABCD_YAML(prestring, a_disc, b_disc, c_disc, d_disc)

def discretize(a_mat, b_mat, time_step):
	a_disc = linalg.expm(a_mat*time_step)
	try:
		b_disc_analytical = (a_disc.dot(np.linalg.inv(a_mat).dot(
			np.eye(a_disc.shape[0])-np.linalg.inv(a_disc))
		).dot(b_mat))
		return a_disc, b_disc_analytical
	except np.linalg.linalg.LinAlgError:
		b_disc = 0
		num_steps = 1000
		for subt in np.linspace(0,time_step, num_steps+1)[:-1]:
			b_disc += linalg.expm(a_mat*(time_step-subt)).dot(b_mat)*time_step/(num_steps)
		return a_disc, b_disc



	# print (a_disc)
	# print(np.linalg.eig(a_mat)[0])
	# print(np.linalg.eig(a_disc)[0])

	# b_mat = np.array([[0],[0],[alpha_rads**2*lambda_rads]])
	# print(b_disc)
	# print(b_disc_analytical)
	# print(b_disc_analytical-b_disc)

def render_AB(a_disc, b_disc):
	for row in range(0,a_disc.shape[0]):
		print("\t"+", ".join(["%.8e"%a for a in a_disc[row,:]])+',')
	print("\t"+", ".join(["%.8e"%b for b in b_disc[:,0]]))

def render_ABCD(a_disc, b_disc, c_mat, d_mat):
	for row in range(0,a_disc.shape[0]):
		print("\t"+", ".join(["%.8e"%a for a in a_disc[row,:]])+',')
	print("\t"+", ".join(["%.8e"%b for b in b_disc[:,0]])+',')
	print("\t"+", ".join(["%.8e"%c for c in c_mat[0,:]])+',')
	print("\t"+", ".join(["%.8e"%d for d in d_mat[:,0]]))

def render_ABCD_YAML(prestring, a_disc, b_disc, c_mat, d_mat):
	for i,r in enumerate(a_disc):
		for j, v in enumerate(r):
			print("%s_a_%d%d: %.8e"%(prestring, i, j, v))
	for i,r in enumerate(b_disc):
		print("%s_b_%d: %.8e"%(prestring, i, r))
	for i,r in enumerate(c_mat):
		for j, v in enumerate(r):
			print("%s_c_%d: %.8e"%(prestring, j, v))
	print("%s_d: %.8e"%(prestring,  d_mat))
	# for row in range(0,a_disc.shape[0]):
	# 	print("\t"+", ".join(["%.8e"%a for a in a_disc[row,:]])+',')
	# print("\t"+", ".join(["%.8e"%b for b in b_disc[:,0]])+',')
	# print("\t"+", ".join(["%.8e"%c for c in c_mat[0,:]])+',')
	# print("\t"+", ".join(["%.8e"%d for d in d_mat[:,0]]))

def cd_disc(A, B, time_step=0.001):
	C0 = np.array([[1, -B[0]/B[1]]])
	C = C0.dot(A.dot(A)-2*A+np.eye(A.shape[0]))/(time_step**2)
	D = C0.dot(A.dot(B))/(time_step**2)
	return C, D


def print_discrete_BK(B=1, K=1, time_step=.001, plotting=False, prestring=None):
	# equation: (s^2 + B* s + K) x = u
	a_mat = np.array([[0,1],[-K , -B]])
	b_mat = np.array([[0],[1]])
	c_mat = np.array([[-K , -B]])
	d_mat = np.array([[1]])

	a_disc, b_disc = discretize(a_mat, b_mat, time_step)
	c_disc, d_disc = cd_disc(a_disc, b_disc)

	if plotting:
		plot_TF(a_mat, b_mat)
		plot_DTF(a_disc, b_disc)
		plot_DTF(a_disc.T, b_disc)

		plot_TF_CD(a_mat, b_mat, c_mat, d_mat)
		plot_DTF_CD(a_disc, b_disc, c_disc, d_disc)
		plot_DTF_CD(a_disc.T, b_disc, c_disc, d_disc)

	if prestring is None: # C++ constructor arguments
		print("\n\t"+"//print_discrete_BK(B=%f, K=%f, time_step=%f)"%(B, K, time_step))
		render_ABCD(a_disc, b_disc, c_disc, d_disc)
	else: # YAML file output
		render_ABCD_YAML(prestring, a_disc, b_disc, c_disc, d_disc)
	



def main():
	# print_discrete_AB(alpha_hz=25, lambda_hz=10.0, time_step=0.001, plotting=True)
	# print_discrete_butterworth(alpha_hz = 10, time_step = 0.001, plotting=True)
	# print_discrete_bessel(alpha_hz=50, time_step=.001, plotting=True)
	# print_discrete_2(omega_hz=75, damping=.15, time_step=0.001, plotting=True)
	# print_discrete_BK(B=5, K=0, time_step=0.001, plotting=True)
	print_discrete_2(omega_hz=8.50000e+01, damping=1.50000e-01, time_step=0.001, plotting=True)
	# torque_filter_lp_1_a_00:

	plt.show()
if __name__ == '__main__':
	main()