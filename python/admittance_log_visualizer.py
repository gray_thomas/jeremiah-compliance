# Gray Thomas, NSTRF15AQ33H at NASA JSC August 2018
# Works in python 3

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from admittance_plotting_utils import *
from six_systems import *



def test_new():
#     data_off = DataSet("log_20181112T142843", csv=True, description=
# """\tThis experiment is the first one of its type. Presumably it is
# driven by a commanded current chirp signal, while the spring is held fixed
# by the frame mount device.""")
    data_set_0 = DataSet(0, csv=False)

    mag_ax, phase_ax = empty_bode_plot(suptitle="Actuator Compliance")
    tf_A_over_B_phase(data_set_0.joint, data_set_0.inf_motor_torque, 
        ax=phase_ax, label="joint / motor torque")
    tf_A_over_B(data_set_0.joint, data_set_0.inf_motor_torque, 
        ax=mag_ax, label="joint / motor torque")
    tf_A_over_B_phase(data_set_0.joint, data_set_0.torque, 
        ax=phase_ax, label="joint / spring torque")
    tf_A_over_B(data_set_0.joint, data_set_0.torque, 
        ax=mag_ax, label="joint / spring torque")
    tf_A_over_B_phase(data_set_0.joint, data_set_0.loadtorque, 
        ax=phase_ax, label="joint / load torque")
    tf_A_over_B(data_set_0.joint, data_set_0.loadtorque, 
        ax=mag_ax, label="joint / load torque")
    mag_ax.legend()

    # plot_admittance(data.force, data.infloadforce, suptitle="Spring")
    # plot_admittance(data.motor, data.infloadforce, suptitle="Motor")
    # plot_admittance(data.actuator, data.infloadforce, suptitle="Actuator")
    plt.show()

# LF testing:
# Find all the spring constants and motion relationships.
# 1: Central for the motor spring --> hang loose
# 2: Push up to equilibrium
# 3: Pull down to equilibrium
# difference in arm angle = difference in spring angle+difference in motor angle
# difference in cuff torque = difference in spring torque = difference in motor torque

# 4: lock output, zero current
# 5: lock output, high current
# 6: lock output, neg current
# difference in cuff torque = difference in spring torque = difference in motor torque = cuff spring constant * difference in arm angle
# difference in arm angle = difference in motor angle + difference in spring angle

def plot_dob_rhs_lhs(ds, name):
    sys_7_position = combine_data([ds.joint, ds.loadtorque], [1, -1./k2])
    sys_7_torque = combine_data([ds.loadtorque],[-1])
    sys_5_position = combine_data([ds.joint], [1])
    sys_5_torque = combine_data([ds.loadtorque],[-1])
    sys_4_position = combine_data([ds.joint], [1])
    sys_4_torque = combine_data([ds.torque],[-1])
    sys_1_position = combine_data([ds.motor], [-calib_motor_rad_2_meters])
    sys_1_torque = combine_data([ds.torque],[-1])
    motor_current = combine_data([ds.cmd], [1])
    position_error = ds.other["dob2_motor_position_error"]
    dob2_u_prime = ds.other['dob2_u_prime']
    dob1_u = ds.other['dob1_u']
    ref_motor_pos_m = ds.other["ref_motor_pos_m"]
    dob1_current_adjustment = ds.other["dob1_current_adjustment"]
    act_f = ds.force
    cmd = ds.cmd
    jacobian = ds.infjac
    reflected_motor_mass=231
    reflected_motor_damping=3465
    reflected_motor_constant=186.176
    alpha = .8
    x_dot = filter_lp(filter_lp_diff(ref_motor_pos_m, alpha=alpha, kd=1e3), alpha=alpha)
    x_ddot = filter_lp_diff(filter_lp_diff(ref_motor_pos_m, alpha=alpha, kd=1e3), alpha=alpha, kd=1e3)
    RHS_3_A = filter_lp(filter_lp(combine_data([cmd, act_f],[reflected_motor_constant, -1]),alpha=alpha), alpha=alpha)
    RHS_3_B = filter_lp(filter_lp(dob2_u_prime,alpha=alpha), alpha=alpha)
    RHS_3_C = filter_lp(filter_lp(dob1_u,alpha=alpha), alpha=alpha)
    LHS_3 = combine_data([x_dot, x_ddot], [reflected_motor_damping, reflected_motor_mass])

    dob_adjustment = filter_lp(filter_lp(combine_data([saturate_data(dob1_current_adjustment, 2)],[reflected_motor_constant]),alpha=alpha), alpha=alpha)


    motor_force_from_spring = combine_data([ds.force],[-1]) #This is measured by the spring deflection
    motor_force_from_current = combine_data([ds.cmd], [motor_constant/calib_motor_rad_2_meters]) #27 mNm/A is motor constant from Maxon for 305015 motor 
    net_motor_force = combine_data([motor_force_from_current, motor_force_from_spring],[1,-1]) 

    plt.plot(*plot_discrete(RHS_3_A), label="RHS-A %s"%name)
    plt.plot(*plot_discrete(RHS_3_B), label="RHS-B %s"%name)
    plt.plot(*plot_discrete(RHS_3_C), label="RHS-C %s"%name)
    plt.plot(*plot_discrete(dob_adjustment), label="current %s"%name)
    plt.plot(*plot_discrete(LHS_3), label="LHS %s"%name)




## Parameters ##
motor_constant = .0276 * .77 # 27.6 mNm/A is from Maxon
ma = 231 #originally 250
me = .1399 #moment of inertia
# me = .5 #moment of inertia
# me = .9 #moment of inertia
# me = 2.9 #moment of inertia
b = 3465 #originally 4500, then 6500 after pre-winterbreak tests
k1 = 730000 * .77# SEA spring, N/m ORIGINAL VALUE WAS 796958, then 643885.1721493489 after pre-winterbreak tests
# k1 /= 2
k2 = 250 # Cuff spring, Nm/rad
time_delay = .004
r = 0.02
kp = 0*1000000
kp = 1e2 /r/r*2
kd = 0 /r/r
kp_f = 0*10
b_robot = 1.1
alpha = 10
calib_motor_rad_2_meters = 0.00011415 #found from observing exact correlation between ds.motor and ds.actuator

def main():

    #################
    
    # rdouble_bode = DoubleBode()
    # double_bode = DoubleBode()

    # ma*motor_acc+b*motor_vel = time_delay*motor_force+spring_force
    system1 = (lambda s: (1/(b*s + ma*s*s)),
               lambda s: (np.exp(-time_delay * s)/(b*s + ma*s*s)))  
               
    parallel1 = virtual_parallel(system1, lambda s: -kp - kd*s-1e-7)
    
    system2 = combine_parallel(system1, parallel1)
    
    f_con_hp_w = 2*np.pi*28.3
    f_control = lambda s: kp_f*(1+s/f_con_hp_w)
    f_control = lambda s: 1e-7
    
    series2 = virtual_series(system2, f_control)
    
    system3 = combine_series(system2, series2)
    
    series3 = lambda s: 1/(k1+0*s)
    
    system4 = combine_series(system3, series3)

    rsystem1 = (lambda s: system1[0](s)/r**2, lambda s: system1[1](s)/r**2)
    rsystem2 = (lambda s: system2[0](s)/r**2, lambda s: system2[1](s)/r**2)
    rsystem3 = (lambda s: system3[0](s)/r**2, lambda s: system3[1](s)/r**2)
    
    rsystem4 = (lambda s: system4[0](s)/r**2, lambda s: system4[1](s)/r**2)
    
    rparallel4 = lambda s: 1/(me*s*s + b_robot*s)
    
    rsystem5 = combine_parallel(rsystem4, rparallel4)
    
    lp_w = 100*np.pi*2
    hp_w = 31*np.pi*2
    
    torque_control5 = lambda s: (f_control(s)+1)*alpha*(1+s/hp_w)*(1/((s/lp_w)**2+.9*(s/lp_w)+1))
    torque_control5 = lambda s: 1e-7

    rseries5 = virtual_series(rsystem5, torque_control5)
    
    rsystem6 = combine_series(rsystem5, rseries5)
    
    rseries6 = lambda s: 1/(0*s+k2)
    
    rsystem7 = combine_series(rsystem6, rseries6)

    test_system = (lambda s : 1/(ma*s*s + b*s + k1), lambda s : 1/(ma*s*s + b*s + k1)) #this is the system that is created when the arm is held fixed
    # double_bode.plot_system(test_system, name = 'Test System') #plot the analytical bode plot 
    
    series1 = (1/k1,0)
    
    system3 = combine_series(system2, series1)



    high_amplitude_locked_tests=[18, 19, 20]
    low_amplitude_locked_tests= [ 21, 22, 23]
    reflected_motor_mass=231
    reflected_motor_damping=3465
    reflected_motor_constant=186.176

    # ds = DataSet(22, csv=False, skip=1)
    # print(dir(ds))
    # print(ds.other.keys())
    # exit()
    bode_plot = BodePlot()

    for i in [33]:
    # for i in [23]:
        ds = DataSet(i, csv=False, skip=1)
        print(dir(ds))
        # plot_dob_rhs_lhs(ds, "%d"%i)
        kwargs = dict(alpha=.4, zorder=-5, marker='.', linestyle='none') 

        test_fit = (lambda s: 1/((me + .1415)*s*s + b_robot*s + k2))     


        torque = combine_data([ds.torque],[1])
        joint_pos = combine_data([ds.joint],[1])
        bode_plot.scatter(joint_pos, torque,label = 'Data Set ' + str(i), **kwargs)
        bode_plot.plot_system(test_fit, name = "Fit line")


        plt.legend()

        # double_bode.compliance_bode.scatter(sys_1_position, motor_force_from_current, label = 'Data Set ' + str(i), **kwargs)

        # double_bode.compliance_bode.scatter(motor_force_from_spring, motor_force_from_current, label = 'Data Set ' + str(i), **kwargs)
        # double_bode.compliance_bode.scatter(sys_1_position, motor_force_from_current, label = 'Data Set ' + str(i), **kwargs)

        # #FINDING k1 from linear fit of dataset 10
        # cross,power,f = get_cross_and_power(sys_1_position, motor_force_from_current)
        # transfer = cross / power
        # plt.figure()
        # x = f[11:900]
        # y = abs(transfer)[11:900]
        # plt.loglog(x, y,'.')
        # plt.ylim(1e-10,1e-1)
        # plt.xlim(1e-2,1e4)
        # plt.grid(1)
        # m, b = np.polyfit(x,y,1)
        # print(m, b)
        # print(1/b)
        # plt.loglog(x,m*x +b)

    #plotting the lines for controller design




    plt.show()


def main2(): #this main is for plotting lines for control design
    bode_plot = BodePlot()

    jacobian_est = .02

    cuff_spring_compliance = lambda s : 1/k2 + s*0
    sea_spring_compliance = lambda s : 1/(k1 * jacobian_est**2) + s*0

    sea_compliance = lambda s : 1/((ma*s*s + b*s) * jacobian_est **2)
    exo_compliance = lambda s : 1/(me*s*s + b_robot*s)

    bode_plot.plot_system(cuff_spring_compliance, name = "Cuff Spring")
    bode_plot.plot_system(sea_spring_compliance, name = "SEA Spring")
    bode_plot.plot_system(sea_compliance, name = "SEA")
    bode_plot.plot_system(exo_compliance, name = "Exo")

    plt.show()

def ATI_test(N): # main for checking ATI torque vs joint torque (from SEA spring)
    bode_plot = BodePlot()

    for i in [N]:

        ds = DataSet(i, csv=False, skip=1)


        #create new np array for ATI torque
        temp = -ds.fz[:,1] * .19264 + (ds.ty[:,1]*np.cos(np.pi/180*52.5) - ds.tx[:,1]*np.cos(np.pi/180*37.5)) #multiply torque values by .19264 m lever arm
        #and also add components of tx and ty
        temp = temp - np.mean(temp) #remove dc offest 
        ATI_torque = np.transpose( np.array( [ds.fz[:,0], temp ] ) ) 

        #plot in time domain
        plt.plot(ds.torque[:,0], ds.torque[:,1], label = "Spring Torque")
        plt.plot(ATI_torque[:,0], ATI_torque[:,1], label = "ATI Torque")
        plt.xlabel("Time (s)")
        plt.ylabel("Torque about joint (Nm)")
        plt.legend()

        #plot in frequency domain
        bode_plot.scatter(ATI_torque, ds.torque, label = "ATI Torque / Spring Torque") #actual data
        theoretical = lambda s: k2 / ((me + 0.1415)*s*s + b_robot*s + k2)
        bode_plot.plot_system(theoretical, name = "theoretical transfer function")
        plt.xlabel("Frequency (Hz)")


       

        plt.show()
        #ds.torque is the joint torque from the SEA spring converted
        #ds.loadtorque should be ignored for now, since it probably only uses Z force of ATI and I know know exactly what's going on with it

def discretize(a_mat, b_mat, time_step):
    a_disc = linalg.expm(a_mat*time_step)
    try:
        b_disc_analytical = (a_disc.dot(np.linalg.inv(a_mat).dot(
            np.eye(a_disc.shape[0])-np.linalg.inv(a_disc))
        ).dot(b_mat))
        return a_disc, b_disc_analytical
    except np.linalg.linalg.LinAlgError:
        b_disc = 0
        num_steps = 1000
        for subt in np.linspace(0,time_step, num_steps+1)[:-1]:
            b_disc += linalg.expm(a_mat*(time_step-subt)).dot(b_mat)*time_step/(num_steps)
        return a_disc, b_disc
def cd_disc(A, B, time_step=0.001):
    C0 = np.array([[1, -B[0]/B[1]]])
    C = C0.dot(A.dot(A)-2*A+np.eye(A.shape[0]))/(time_step**2)
    D = C0.dot(A.dot(B))/(time_step**2)
    return C, D

def jacobian_test(N): #just for plotting jacobian of a test

    for i in [N]:

        ds = DataSet(i, csv=False, skip=1)
        # print(ds.other['jacobian'])


        #Okay now recreate the jacobian based off of the if statement I have in the .cpp code 
        # if( abs(actuator_force)>1 && -0.03 < joint_torque/actuator_force && joint_torque/actuator_force < -0.004 ){
        # jacobian = joint_torque/actuator_force;
        # }
        used_jac = np.zeros(ds.torque.shape) #ds.torque can be substituted for any ds.*

        for i in range(ds.torque.shape[0]):
            temp_jac = ds.torque[i,1]/ds.force[i,1]
            if abs(ds.force[i,1]) > 1 and -0.0255 < temp_jac and temp_jac < -0.001:
                used_jac[i,1] = temp_jac
            elif i == 0:
                used_jac[i,1] = -.02
            else: 
                used_jac[i,1] = used_jac[i-1,1]

            #now do some additional stuff to clean it up further
            if i != 0:
                #if current point differs too much from previous point, make it equal previous point
                if abs(used_jac[i,1] - used_jac[i-1,1]) > .0001:
                    used_jac[i] = used_jac[i-1]


            used_jac[i,0] = ds.joint[i,1]

        # used_jac = used_jac[3000 + 400 :5123,:] #THIS IS SPECIFIC TO DATASET 37

        plt.plot(used_jac[:,0],used_jac[:,1], label = "Filtered back-calculation")
        # plt.plot(ds.joint[:,1], ds.other['gamma'][:,1]*180/np.pi, label = "gamma in deg")
        # plt.plot(ds.joint[:,1], ds.other['act_length'][:,1], label = "act_length")

        #calculate an analytical jacobian


        #input parameters
        hdist = 0.1143 #4.5 * .0254 #distance between the two points of rotation in m
        vdist = .012
        lever_arm = 0.025
        min_joint_pos = 1.590767




        #calculated parameters
        hyp = ((hdist**2) + (vdist**2))**0.5
        phi = np.arctan(vdist/hdist)

        joint_angles = np.copy(used_jac[:,0]) #create python list of all angles
        analytical_jac = np.zeros((len(joint_angles),2))#initial array
        beta = 0
        theta = 0
        gamma = 0
        act_length = 0

        for i in range(len(joint_angles)):
            analytical_jac[i,0] = joint_angles[i]
            joint_angles[i] = joint_angles[i]-min_joint_pos

            act_length = (lever_arm**2 + hyp**2 - 2*lever_arm*hyp*np.cos(gamma))**0.5
            gamma = np.pi/2 - joint_angles[i] - phi
            beta = 3/2*np.pi - phi - gamma - (np.pi/2 - phi) - np.arcsin(lever_arm/act_length * np.sin(gamma))
            theta = beta - np.pi/2


            # theta = ((np.sin(np.pi/2-phi-joint_angles[i]) * hyp)/(lever_arm**2 + hyp**2 - 2*lever_arm*hyp*np.cos(np.pi/2-phi-joint_angles[i]))**0.5) - np.pi/2
            analytical_jac[i,1] = -lever_arm * np.cos(theta) 


        plt.plot(analytical_jac[1:,0],analytical_jac[1:,1], label = "From Python")
        plt.plot(ds.joint[:,1], ds.other['jacobian'][:,1], label = "from c++")


        #figure out encoder steps
        min_step = 100.

        for i in range(1, len(ds.joint[:,1])):
            if abs(ds.joint[i,1] - ds.joint[i-1,1]) != 0 and abs(ds.joint[i,1] - ds.joint[i-1,1]) < min_step:
                min_step = abs(ds.joint[i,1] - ds.joint[i-1,1])

        print(min_step)
       

        plt.legend()

        # plt.figure()
        # plt.plot(ds.joint[:,1], ds.other['cosine'][:,1])
        plt.grid()
        plt.show()


def cuff_spring_calibration(N):

    ds = DataSet(N, csv=False, skip=1)


    bodeplot = BodePlot()

    bodeplot.scatter(ds.joint, ds.torque)

    plt.xlim([0.5, 3])

    plt.show()

def compliance_ratios(N):
    ds = DataSet(N, csv=False, skip=1)
    bode_plot = BodePlot()
    print(dir(ds))
    bode_plot.scatter(ds.joint, ds.ATI_torque)
    plt.show()

def time_domain(N):
    ds = DataSet(N, csv=False, skip=1)
    print(ds.other.keys())

    # plt.plot(*plot_discrete(ds.torque), label="spring torque")
    # plt.plot(*plot_discrete(ds.ATI_torque), label="ATI_torque")
    if False: # motor current and DOB debugging
        plt.plot(*plot_discrete(ds.other['motor_current_from_feedback']), label="motor_current_from_feedback") # too huge
        plt.plot(*plot_discrete(ds.other['saturated_feedback_current']), label="saturated_feedback_current")
        plt.plot(*plot_discrete(ds.other['dob::current_pre_deadzone']), label="pre_deadzone")
        plt.plot(*plot_discrete(ds.other['dob::current_adjustment']), label="dob_current_adjustment")
    if True: # 
    # plt.plot(*plot_discrete(ds.other['reflected_motor_pos_m']), label="reflected_motor_pos_m")
        # plt.plot(*plot_discrete(ds.other['tau_m_from_cuff_torque']), label="from cuff torque")
        # plt.plot(*plot_discrete(op_data(ds.other['calculated_cuff_torque'], lambda x: 1000*x)), label="1000 cuff torque")
        # plt.plot(*plot_discrete(op_data(ds.other['chirp'], lambda x: 1000*x)), label="1000 chirp")
        plt.plot(*plot_discrete(op_data(ds.other['test_noise'], lambda x: 1000*x)), label="1000 test_noise")
        # plt.plot(*plot_discrete(op_data(ds.other['spring_torque'],lambda x: 1000*x)), label="1000 spring_torque")
        plt.plot(*plot_discrete(op_data(ds.other['cuff_torque_filter::tau_1'], lambda x: 1000*x)), label="1000 tau 1")
        plt.plot(*plot_discrete(ds.other['cuff_torque_filter::tau_2']), label="tau 2")
        plt.plot(*plot_discrete(ds.other['cuff_torque_filter::tau_3']), label="tau 3")
        # plt.plot(*plot_discrete(ds.torque), label="torque")
    # plt.plot(*plot_discrete(ds.other['dob_u']), label="dob_u")

    # plt.plot(*plot_discrete(ds.other['tau_m_from_motor']), label="tau_m_from_motor")
    # plt.plot(*plot_discrete(ds.other['tau_m_from_spring']), label="tau_m_from_spring")
    # plt.plot(*plot_discrete(ds.other['tilde_B_2']), label="tilde_B_2")
    # plt.plot(*plot_discrete(op_data(ds.other['tilde_K_2'], lambda x:np.sqrt(x))), label="sqrt tilde_K_2")
    # jacobian = -0.02
    # G_m = 186
    # plt.plot(*plot_discrete(op_data(ds.other['B_1'], lambda x: x)), label="B_1")
    # plt.plot(*plot_discrete(op_data(ds.other['K_1'], lambda x: x)), label="K_1")
    # plt.plot(*plot_discrete(op_data(ds.other['B_2'], lambda x: x)), label="B_2")
    # plt.plot(*plot_discrete(op_data(ds.other['K_2'], lambda x: x)), label="K_2")
    # plt.plot(*plot_discrete(ds.other['jacobian']), label="jacobian")
    # plt.plot(*plot_discrete(op_data(ds.other['tau_m_from_cuff_torque'], lambda x: x)), label="tau_m_from_cuff_torque")
    # plt.plot(*plot_discrete(op_data(ds.other['calculated_cuff_torque'], lambda x: x)), label="calculated_cuff_torque")
    # plt.plot(*plot_discrete(ds.other['dob1_u']), label="dob1_u")
    # plt.plot(*plot_discrete(ds.other['dob1_u']), label="dob1_u")
    plt.legend()
    # plt.show()

def debug_controller(N):
        ## Parameters ##
    lin_m_m = 231 # reflected motor inertia (Kg = Ns^2/m)
    lin_b_m = 3465 # reflected motor damping (Ns/m)
    lin_g_m = 186.176 # reflected motor constant (N/amp)
    lin_k_s = 562100 # SEA spring constant (N/m)
    j_e = .1399 # exoskeleton inertia (Nm s^2/rad = Kg m^2)
    extra_j_e = .1415 # adding the 660 g weight to the arm (Nm s^2/rad = Kg m^2)
    # extra_j_e = 5*.1415 # even more
    j_e += extra_j_e
    # b_e = 1.1 # exoskeleton damping (Nm s/rad)
    k_c = 250 # Cuff spring (Nm/rad)
    jacobian = -0.025 # transmission ratio (m)
    j_h = .01377 # Kg m^2
    time_delay = .004 # time delay (s) (tuned by finding out when controllers go unstable (Kp1000 and Kd10 both unstable) (Kp700 + Kd7 stable))

    ## Adjusted parmeters ##
    j_m = lin_m_m * jacobian**2
    b_m = lin_b_m * jacobian**2
    k_s = lin_k_s * jacobian**2

    ## Filtering Parameters ##
    lp_1_w = 2*np.pi*35 # rad s
    lp_1_z = .2 # damping ratio
    lp_1 = lambda s: lp_1_w**2/(s**2+2*s*lp_1_z*lp_1_w+lp_1_w**2)
    lp_2_w = 2*np.pi*75 # rad s
    lp_2_z = .3 # damping ratio
    lp_2 = lambda s: lp_2_w**2/(s**2+2*s*lp_2_z*lp_2_w+lp_2_w**2)
    spring_force_derivative_pole_hz = 50
    lp_spring = lambda s: 1./((s/(2*np.pi*spring_force_derivative_pole_hz))+1)

    ## Controller Meta Parameters ##
    alpha = 3 # amplification ratio
    alpha_hat_J_m = j_e/1.5 / alpha # amplified inertia (Kg m^2)
    alpha_hat_B_m = alpha_hat_J_m * 2*np.pi*(0.75) # amplified damping (Nm s/rad)
    hat_zeta_nominal = .3 # damping ratio for amplified compliance zeros
    sea_zeta = .7 # damping ratio for SEA zeros

    ## Algebra to compute gains ##
    hat_J_m = alpha_hat_J_m*alpha
    hat_B_m = alpha_hat_B_m*alpha
    q_1 = (2*hat_zeta_nominal*np.sqrt(k_c*alpha_hat_J_m)-alpha_hat_B_m)/k_c # zero in cuff feedback
    # q_1 = 1/(2*np.pi*15)
    C_h_nominal = lambda s: 1/k_c + (1+q_1*s)/(alpha_hat_J_m*s*s+alpha_hat_B_m*s)
    C_h_test_1 = lambda s: 1/k_c + (
        (1+q_1*s)/(alpha_hat_J_m*s*s+alpha_hat_B_m*s)
        *np.exp(-s*time_delay)
        *lp_1(s)*lp_2(s)
        )
    S6_test = lambda s: (
        (1+q_1*s)/(alpha_hat_J_m*s*s+alpha_hat_B_m*s)
        *np.exp(-s*time_delay)
        *lp_1(s)*lp_2(s)
        )
    tilde_B_1 = alpha_hat_B_m/alpha_hat_J_m
    tilde_K_1 = 0 # based on exoskeleton no-spring objective
    tilde_B_2 = sea_zeta*np.sqrt(k_s/hat_J_m)
    tilde_K_2 = k_s/hat_J_m
    C_sea = lambda s: 1/(k_s)*(s**2+tilde_B_2*s+tilde_K_2)/(s**2+tilde_B_1*s+tilde_K_1)
    S4_test = lambda s: (
        1/(k_s) 
        + 1/(j_m*(s**2+tilde_B_1*s+tilde_K_1)) 
        + np.exp(-s*time_delay)*((tilde_B_2-tilde_B_1)*s*lp_spring(s) + (tilde_K_2-tilde_K_1-k_s/j_m))/(s**2+tilde_B_1*s+tilde_K_1)/(k_s)
        )



    S5_test = lambda s: 1/(j_e*s*s + 1/S4_test(s))

    K_1 = 0 # gain from joint-space motor displacement to joint-space motor torque
    B_1 = tilde_B_1*j_m-b_m # Nm s/rad
    K_2 = (tilde_K_2*j_m - K_1 - k_s)/(k_s) # Nm/Nm
    B_2 = (tilde_B_2*j_m - B_1 - b_m)/(k_s) # don't forget the b_m
    C_sea2 = lambda s: 1/(k_s)+(1+np.exp(-s*time_delay)*(K_2+B_2*s*lp_spring(s)))/(j_m*s**2+b_m*s + (s*B_1+K_1))

    ## Controller joint space transfer functions ##
    tau_m_per_js_motor_angle = lambda s: -K_1-B_1*s
    tau_m_per_js_spring_torque = lambda s: K_2+B_2*s*lp_spring(s)
    tau_m_per_js_cuff_torque = lambda s: (j_m/hat_J_m)*(1+
        j_e/(k_s)
        *(s**2*lp_2(s))
        *(s**2+tilde_B_2*s+tilde_K_2)/(s**2+tilde_B_1*s+tilde_K_1))*(
        (alpha-1)*(1+q_1*s)*lp_1(s))
    tau_m_per_js_cuff_torque = lambda s: (j_m/hat_J_m)*(1+
        j_e/(k_s)
        *(s*lp_2(s))
        *(s**2+tilde_B_2*s+tilde_K_2)*lp_2(s)*1/(s+tilde_B_1))*(
        (alpha-1)*(1+q_1*s)*lp_1(s))

    cuff_torque_pre_filter = lambda s: (j_m/hat_J_m)*(1+
        j_e/(k_s)
        *(s*lp_2(s))
        *(s**2+tilde_B_2*s+tilde_K_2)*lp_2(s)*1/(s+tilde_B_1))

    tau_1_per_js_cuff_torque = lambda s: ((alpha-1)*(1+q_1*s)*lp_1(s))
    tau_3_per_js_cuff_torque = lambda s: (
        j_e/(k_s)
        *(s**2*lp_2(s))
        *(s**2+tilde_B_2*s+tilde_K_2)/(s**2+tilde_B_1*s+tilde_K_1))*(
        (alpha-1)*(1+q_1*s)*lp_1(s))
    tau_2_per_js_cuff_torque = lambda s: ((s**2+tilde_B_2*s+tilde_K_2)/(s**2+tilde_B_1*s+tilde_K_1))*(
        (alpha-1)*(1+q_1*s)*lp_1(s))
    tau_2_per_tau_1 = lambda s: (s**2+tilde_B_2*s+tilde_K_2)/(s**2+tilde_B_1*s+tilde_K_1)
    tau_3_per_tau_2 = lambda s: j_e/(k_s)*(s**2*lp_2(s))

    ## Convert to amps
    motor_current_per_js_motor_angle = lambda s: 1/lin_g_m/jacobian*tau_m_per_js_motor_angle(s)
    motor_current_per_js_spring_torque = lambda s: 1/lin_g_m/jacobian*tau_m_per_js_spring_torque(s)
    motor_current_per_js_cuff_torque = lambda s: 1/lin_g_m/jacobian*tau_m_per_js_cuff_torque(s)

    S4_wrong_test = lambda s: (1/(k_s)*.75 
        + (1+np.exp(-s*time_delay)*K_2+B_2*s*1./((s/(2*np.pi*spring_force_derivative_pole_hz*.1))+1))/((j_m*s**2+0.8*b_m*s-np.exp(-s*time_delay)*tau_m_per_js_motor_angle(s))) 
        )

    reload_ds = True
    if reload_ds: ds = DataSet(N, csv=False, skip=1)
    if reload_ds: print(ds.other.keys())
    if False: # check compliances
        bode = BodePlot()
        # bode.scatter(ds.other['tau_m_from_cuff_torque'], ds.other['calculated_cuff_torque'], label="full")
        # if reload_ds: bode.scatter(ds.other['tau_m_from_motor'], ds.other['joint_angle_from_motor'], label="motor component", marker=",", lw=0)
        # if reload_ds: bode.scatter(ds.other['tau_m_from_spring'], ds.other['spring_torque'], label="spring component", marker=",", lw=0)
        if reload_ds: bode.scatter(ds.joint, ds.other['spring_torque'], label="actuator compliance", marker=".", ms=1, lw=0)
        if reload_ds: bode.scatter(ds.joint, ds.other['calculated_cuff_torque'], label="joint compliance", marker=".", ms=1, lw=0)
        # bode.plot_system(tau_m_per_js_motor_angle, label='expected motor')
        # bode.plot_system(tau_m_per_js_spring_torque, label='expected spring')
        bode.plot_system(S4_test, label='expected actuator')
        # bode.plot_system(S4_wrong_test, label='wrong actuator')
        bode.plot_system(S5_test, label='expected unamplified')
        bode.plot_system(S6_test, label='expected amplified')
    if True: # check cuff feedback
        bode = BodePlot()
        # if reload_ds: bode.scatter(ds.other['cuff_torque_filter::tau_1'], ds.other['calculated_cuff_torque'], label="tau_1", marker=".", ms=.75, lw=0)
        # if reload_ds: bode.scatter(ds.other['cuff_torque_filter::tau_2'], ds.other['calculated_cuff_torque'], label="tau_2/tau_c", marker=".", ms=.75, lw=0)
        # if reload_ds: bode.scatter(ds.other['cuff_torque_filter::tau_3'], ds.other['calculated_cuff_torque'], label="tau_2/tau_c", marker=".", ms=.75, lw=0)
        # if reload_ds:bode.scatter(ds.other['tau_m_from_cuff_torque'], ds.other['chirp'], label="tau_2/tau_c", marker=",", lw=0)
        # if reload_ds:bode.scatter(ds.other['cuff_torque_filter::tau_2'], ds.other['cuff_torque_filter::tau_1'], label="tau_2/tau_1", marker=",", lw=0)
        # if reload_ds:bode.scatter(ds.other['cuff_torque_filter::tau_3'], ds.other['cuff_torque_filter::tau_2'], label="tau_3/tau_2", marker=",", lw=0)
        if reload_ds:bode.scatter(ds.other['tau_m_from_cuff_torque'], ds.other['test_noise'], label="tau_m/noise", marker=",", lw=0)
        bode.plot_system(tau_m_per_js_cuff_torque, label="expected", suptitle="Motor torque per cuff torque")
        bode.plot_system(cuff_torque_pre_filter, label="pre_filter", suptitle="Motor torque per cuff torque")
        # bode.plot_system(tau_1_per_js_cuff_torque, label="tau_1", suptitle="Motor torque per cuff torque")
        # bode.plot_system(tau_2_per_js_cuff_torque, label="tau_2", suptitle="Motor torque per cuff torque")
        # bode.plot_system(tau_3_per_js_cuff_torque, label="tau_3", suptitle="Motor torque per cuff torque")
        # bode.plot_system(lambda s: s*lp_2(s), label="expected tau_2/tau_1", suptitle="Motor torque per cuff torque")
        # bode.plot_system(lambda s: (s**2+tilde_B_2*s+tilde_K_2)*lp_2(s), label="expected tau_3/tau_2", suptitle="Motor torque per cuff torque")

    if False:
        tau_2_per_tau_1 = lambda s: (tilde_B_2*s+tilde_K_2)/(s**2+tilde_B_1*s+tilde_K_1) # fits almost perfectly with acceleration disabled
        tau_3_per_tau_2 = lambda s: j_e/(k_s)*(lp_2(s)) # fits almost perfectly with acceleration disabled

        bode.plot_system(tau_2_per_tau_1, label="wrong tau_2/tau_1", suptitle="Motor torque per cuff torque")
        bode.plot_system(tau_3_per_tau_2, label="wrong tau_3/tau_2", suptitle="Motor torque per cuff torque")


    if False:
        ## Focus on lp_2 state (from tau_2)
        bode=BodePlot()
        bode.plot_system(tau_3_per_tau_2, label="expected tau_3/tau_2", suptitle="Motor torque per cuff torque")

        if reload_ds:
            bode.scatter(ds.other['cuff_torque_filter::lp_2.x0'], 
                ds.other['cuff_torque_filter::tau_2'], label="lp_2.x0/tau_1", marker=",", lw=0)
            bode.scatter(ds.other['cuff_torque_filter::lp_2.x1'], 
                ds.other['cuff_torque_filter::tau_2'], label="lp_2.x1/tau_1", marker=",", lw=0)
            bode.scatter(ds.other['cuff_torque_filter::lp_2.ul'], 
                ds.other['cuff_torque_filter::tau_2'], label="lp_2.ul/tau_1", marker=",", lw=0)
            lp_2_c_0= -2.57351887e+05
            lp_2_c_1= -2.79935874e+02
            lp_2_d= 2.57351887e+05
            res = combine_data([
                ds.other['cuff_torque_filter::lp_2.x0'],
                ds.other['cuff_torque_filter::lp_2.x1'],
                ds.other['cuff_torque_filter::lp_2.ul']], [lp_2_c_0, lp_2_c_1, lp_2_d])

            bode.scatter(res, 
                ds.other['cuff_torque_filter::tau_2'], label="res/tau_1", marker=",", lw=0)
            bode.scatter(ds.other['cuff_torque_filter::lp_2._acc'], 
                ds.other['cuff_torque_filter::tau_2'], label="lp_2._acc/tau_1", marker=",", lw=0)


    # bode = BodePlot()
    # bode.scatter(ds.other['tau_m_from_spring'], ds.other['spring_torque'], label="actual")
    # tau_m_per_js_spring_torque = lambda s: K_2+B_2*s*lp_spring(s)
    # bode.plot_system(tau_m_per_js_spring_torque, label="expected", suptitle="Motor torque per spring torque")

    # bode = BodePlot() # basically good
    # bode.scatter(ds.other['tau_m_from_motor'], ds.other['joint_angle_from_motor'], label="actual")
    # bode.plot_system(tau_m_per_js_motor_angle, label="expected", suptitle="Motor torque per motor angle")
    if False:
        plt.figure()
        plt.plot(*plot_discrete(ds.other['cuff_torque_filter::tau_1']), label="tau_1")
        plt.plot(*plot_discrete(ds.other['cuff_torque_filter::tau_2']), label="tau_2")
        plt.plot(*plot_discrete(ds.other['cuff_torque_filter::tau_3']), label="tau_3")
        plt.legend()

def C5_compliance(ds, bode):
    return cuff_compliance(ds, bode, C5=True)

def C7_compliance(ds, bode):
    return cuff_compliance(ds, bode, C7=True)

def cuff_compliance(ds, bode, C5=False, C7=False):
    ## Parameters ##
    lin_m_m = 231 # reflected motor inertia (Kg = Ns^2/m)
    lin_b_m = 3465 # reflected motor damping (Ns/m)
    lin_g_m = 186.176 # reflected motor constant (N/amp)
    lin_k_s = 562100 # SEA spring constant (N/m)
    j_e = .1399 # exoskeleton inertia (Nm s^2/rad = Kg m^2)
    # extra_j_e = .1415 # adding the 660 g weight to the arm (Nm s^2/rad = Kg m^2)
    # extra_j_e = 5*.1415 # even more
    extra_j_e = 4560/660*.1415 # even more
    j_e += extra_j_e
    # b_e = 1.1 # exoskeleton damping (Nm s/rad)
    k_c = 250 # Cuff spring (Nm/rad)

    kwargs=dict(window_size=1024*2, window_number=400)
    cuff_tau, f = hann_phasor(ds.other['calculated_cuff_torque'],ds.other['calculated_cuff_torque'], **kwargs)
    s = complex(0, np.pi*2)*f
    motor_theta, f = hann_phasor(ds.other['joint_angle_from_motor'],ds.other['calculated_cuff_torque'], **kwargs)
    motor_x, f = hann_phasor(ds.other['reflected_motor_pos_m'],ds.other['calculated_cuff_torque'], **kwargs)
    joint, f = hann_phasor(ds.joint,ds.other['calculated_cuff_torque'], **kwargs)
    force, f = hann_phasor(ds.force,ds.other['calculated_cuff_torque'], **kwargs)
    spring_torque, f = hann_phasor(ds.other['spring_torque'],ds.other['calculated_cuff_torque'], **kwargs)
    cmd, f = hann_phasor(ds.cmd,ds.other['calculated_cuff_torque'], **kwargs)
    dob_cmd, f = hann_phasor(ds.other['dob::current_adjustment'],ds.other['calculated_cuff_torque'], **kwargs)
    tau_m_motor, f = hann_phasor(ds.other['tau_m_from_motor'],ds.other['calculated_cuff_torque'], **kwargs)
    tau_m_spring, f = hann_phasor(ds.other['tau_m_from_spring'],ds.other['calculated_cuff_torque'], **kwargs)
    tau_m_cuff, f = hann_phasor(ds.other['tau_m_from_cuff_torque'],ds.other['calculated_cuff_torque'], **kwargs)
    avg_jac = np.sum(ds.other['jacobian'][:,1])/len(ds.other['jacobian'][:,1])

    print(avg_jac)# -0.02278 m

    J_m = avg_jac**2 * lin_m_m
    B_m = avg_jac**2 * lin_b_m
    K_s = avg_jac**2 * lin_k_s
    J_j = j_e
    K_c = k_c
    G_m = avg_jac*lin_g_m


    motor_current_from_feedback, f = hann_phasor(ds.other['motor_current_from_feedback'],ds.other['calculated_cuff_torque'], **kwargs)
    saturated_feedback_current, f = hann_phasor(ds.other['saturated_feedback_current'],ds.other['calculated_cuff_torque'], **kwargs)
    dob_current_adjustment, f = hann_phasor(ds.other['dob::current_adjustment'],ds.other['calculated_cuff_torque'], **kwargs)
    spring_force, f = hann_phasor(ds.other['dob::current_adjustment'],ds.other['calculated_cuff_torque'], **kwargs)
    # bode.scatter1(f, joint/cuff_tau+1/k_c, label="joint/cuff", marker=".", ms=3, lw=0)
    # bode.scatter1(f, (cuff_tau*15/9.5*np.sqrt(15/12)*1.06), label="(cuff_tau)", marker=".", ms=3, lw=0)
    # bode.scatter1(f, (spring_torque.real), label="(spring_torque)", marker=".", ms=3, lw=0)
    # bode.scatter1(f, ((s*joint).real*1.1), label="(s joint)", marker=".", ms=3, lw=0)
    # bode.scatter1(f, ((s*s*joint).real*j_e), label="(ss joint)", marker=".", ms=3, lw=0)
    # bode.scatter1(f, ((s*joint).real*1.1+cuff_tau*15/9.5*np.sqrt(15/12)*1.06), label="(s joint + cuff_tau)", marker=".", ms=3, lw=0)
    # bode.scatter1(f, (spring_torque-cuff_tau*15/9.5*np.sqrt(15/12)), label="(spring_torque)", marker=".", ms=3, lw=0)
    # bode.scatter1(f, cmd*lin_g_m/cuff_tau, label="cmd*lin_g_m/cuff", marker=".", ms=3, lw=0)
    # bode.scatter1(f, lin_m_m*s*s*motor_x/cuff_tau, label="lin_m_m*s*s*motor_x/cuff", marker=".", ms=3, lw=0)
    # bode.scatter1(f, lin_b_m*s*motor_x/cuff_tau, label="lin_b_m*s*motor_x/cuff", marker=".", ms=3, lw=0)
    # bode.scatter1(f, force/cuff_tau, label="force/cuff", marker=".", ms=3, lw=0)
    adjusted_error_force = (cmd-dob_cmd)*lin_g_m - force  +lin_m_m*s*s*motor_x  + lin_b_m*s*motor_x
    error_force = cmd*lin_g_m - force  +lin_m_m*s*s*motor_x  + lin_b_m*s*motor_x
    position_error_despite_DOB = motor_x - 1/(lin_m_m*s*s + lin_b_m*s )*((cmd-dob_cmd)*lin_g_m-force)
    angular_error = motor_theta - 1/(J_m*s*s + B_m*s )*((cmd-dob_cmd)*G_m+spring_torque)
    # bode.scatter1(f, adjusted_error_force/(-force), label="adjusted_error_force/force", marker=".", ms=3, lw=0)
    # bode.scatter1(f, lin_g_m*dob_current_adjustment/error_force, label="dob_adjustment/error_force", marker=".", ms=3, lw=0)
    # bode.scatter1(f, (motor_x-position_error_despite_DOB-force/lin_k_s)/(-force), label="lin actuator compliance", marker=".", ms=3, lw=0)
    if C5:
        # bode.scatter1(f, (motor_theta-angular_error+spring_torque/K_s)/(spring_torque), label="C_4", marker=".", ms=3, lw=0)
        bode.scatter1(f, 1/(1/((joint)/(spring_torque))+J_j*s**2), label="C_5", marker=".", ms=3, lw=0)
        # bode.scatter1(f, 1/(1/((motor_theta+spring_torque/K_s)/(spring_torque))+J_j*s**2), label="C_5 uncorrected", marker=".", ms=3, lw=0)
    # bode.scatter1(f, (joint-angular_error)/(cuff_tau), label="C_6 v2", marker=".", ms=3, lw=0)
    if C7:
        # bode.scatter1(f, (motor_theta-angular_error+spring_torque/K_s)/(cuff_tau), label="C_6", marker=".", ms=3, lw=0)
        bode.scatter1(f, (joint)/(cuff_tau)+1/K_c, label="C_7", marker=".", ms=3, lw=0)
        # bode.scatter1(f, tau_m_spring/spring_torque, label="Gs", marker=".", ms=3, lw=0)
        # bode.scatter1(f, tau_m_motor/motor_theta, label="Gm", marker=".", ms=3, lw=0)
        # bode.scatter1(f, tau_m_cuff/cuff_tau, label="Gs", marker=".", ms=3, lw=0)
        # bode.scatter1(f, (motor_theta+spring_torque/K_s)/(cuff_tau), label="C_6 v2", marker=".", ms=3, lw=0)
        # bode.scatter1(f, (joint)/(cuff_tau)+1/K_c, label="C_7 uncorrected", marker=".", ms=3, lw=0)
    # bode.scatter1(f, (joint)/(cuff_tau), label="C_6 v3", marker=".", ms=3, lw=0)
    # bode.scatter1(f, cuff_tau, label="cuff tau", marker=".", ms=3, lw=0)
    # bode.scatter1(f, joint, label="joint", marker=".", ms=3, lw=0)
    
    # bode.scatter1(f, angular_error/position_error_despite_DOB, label="ang over x for error", marker=".", ms=3, lw=0)
    
    # bode.scatter1(f, position_error_despite_DOB/(-force), label="position_error_despite_DOB/force", marker=".", ms=3, lw=0)


def controller_plot(N1, N2):
    ds1 = DataSet(N1, csv=False, skip=1)
    ds2 = DataSet(N2, csv=False, skip=1)
    bode_plot = BodePlot()
    print(dir(ds1))
    print(ds1.other.keys())
    # bode_plot.scatter(ds1.joint, ds1.ATI_torque, label="Cuff", marker=".", ms=1, lw=0)
    # bode_plot.scatter(ds2.joint, ds2.ATI_torque, label="Environment", marker=".", ms=1, lw=0)
    C5_compliance(ds2, bode_plot)
    C7_compliance(ds1, bode_plot)

def time_domain_results_plot(file_number, t0=0, tmax=-1, **kwargs):
    ds = DataSet(file_number, csv=False, skip=1)
    print(dir(ds))
    print(ds.other.keys())  
    grav = op_data(ds.joint,lambda x: np.sin(x))

    fig = plt.figure(figsize=(7.5,3.25))
    plt.plot(*plot_discrete(op_data(ds.other['calculated_cuff_torque'], lambda x: -7*x)[t0:tmax,:]), label="7 x cuff torque")
    plt.plot(*plot_discrete(combine_data([ds.torque, grav], [1,0])[t0:tmax,:]), label="load torque")
    plt.legend()
    fig.savefig("time_domain.png", dpi=600)

def time_domain_results_plot2(file_number, t0=1000, tmax=-1, **kwargs):
    ds = DataSet(file_number, csv=False, skip=1)
    print(dir(ds))
    print(ds.other.keys())  
    grav = op_data(ds.joint,lambda x: np.sin(x))

    alpha = np.exp(-0.001*2*np.pi*10)
    jvel = filter_lp_diff( ds.joint, alpha=alpha)
    jacc = filter_lp_diff( jvel, alpha=alpha)

    lp_cuff = filter_lp(filter_lp(ds.other['calculated_cuff_torque'], alpha=alpha), alpha=alpha)

    j_e = 0.2814
    alpha_hat_J_m = 2.79384091e-01
    tilde_B_1 = 4.71238898e+00
    alpha_hat_B_m = tilde_B_1*alpha_hat_J_m

    expected_torque = combine_data([jvel, jacc], [alpha_hat_B_m, alpha_hat_J_m])

    fig = plt.figure(figsize=(7.5,3.25))
    plt.plot(*plot_discrete(op_data(lp_cuff, lambda x: -x-3)[t0:tmax,:]), label="cuff torque")
    plt.plot(*plot_discrete(expected_torque[t0:tmax,:]), label="expected torque")
    plt.legend()
    fig.savefig("time_domain.png", dpi=600)

def main():
    # cuff_spring_calibration()
    # jacobian_test(52)
    # compliance_ratios(52)
    # time_domain(79)
    # time_domain(66)

    # debug_controller(79)

    # time_domain_results_plot(90, t0=85000, tmax=95000)
    # time_domain_results_plot2(93)
    time_domain_results_plot2(80)
    # time_domain_results_plot2(81)
    # time_domain_results_plot(92, t0=165000, tmax=178000) # up-down forces
    # time_domain_results_plot(92, t0=46000, tmax=49500) # fast oscillation
    # controller_plot(80,81) # alpha 8 ~passive
    # controller_plot(93,81) # alpha 8 ~passive
    # controller_plot(92,81) # alpha 8 ~passive
    # controller_plot(82,83) # alpha 4 ~passive
    # controller_plot(84,85) # alpha 4 not passive

    plt.show()


def main3(): #Show that DOB works (generate figures for paper)
    
    i = 83

    #Hysteresis plot (current vs joint angle)
    #This needs to be done for a clean, low frequency chirp test w/o the DOB, then a separate test that is exactly the same but w/o the DOB

    ds = DataSet(i, csv=False, skip=1)

    plt.plot(ds.joint[:,1], ds.other['cmd_motor_current'][:,1] - ds.other['dob::current_adjustment'][:,1], label = "Raw Current Command")
    plt.plot(ds.joint[:,1], ds.other['cmd_motor_current'][:,1], label = "DOB-Adjusted Current Command")

    plt.xlabel("Joint Position (rad)")
    plt.ylabel("Current (A)")

    plt.grid()
    plt.legend()



    #Frequency domain plot of adjustment/disturbance
    # ***Everything converted to linear actuator frame***

    i = 86
    ds = DataSet(i, csv=False, skip=1)

    bodeplot = BodePlot()
    reflected_motor_mass = 231.0 # in linear space (kg)
    reflected_motor_damping = 3465.0 # in linear space (Ns/m)
    reflected_motor_constant = 186.176 # in units of N/A
    MOTOR2LIN  = 1.2732e-4



    #first, convert motor vel to frequency domain
    kwargs=dict(window_size=1024*8, window_number=2)
    a, f = hann_phasor(ds.other['dob::current_adjustment'], ds.motor_vel, **kwargs)
    motor_vel, f = hann_phasor(ds.motor_vel,ds.motor_vel, **kwargs)
    dob_u, f = hann_phasor(ds.other['dob_u'], ds.motor_vel, **kwargs)
    s = complex(0, np.pi*2)*f
    bodeplot.scatter1(f, a*reflected_motor_constant/-((s*reflected_motor_mass + reflected_motor_damping)* motor_vel*MOTOR2LIN - dob_u))
    # bodeplot.scatter1(f, a*reflected_motor_constant/((s*reflected_motor_mass + reflected_motor_damping)* motor_vel*MOTOR2LIN - dob_u))

    omega = 25
    damping = 0.4
    Q = lambda s: omega**2 / (s*s + 2*damping*omega*s + omega**2) 
    bodeplot.plot_system(Q) 


    #Ok new plot of error in theta domain
    plt.figure()
    plt.title("Reduction in Commanded vs Actual Force Error using DOB")

    #Ok but actually, the measured force should be matched to the previous timestep's current and joint position
    #So make a version of force that moves all data down an index, gets rid of first item, and adds a zero to the last item 

    index_adjusted_force = np.zeros(ds.force.shape)

    for i in range(len(ds.force)-1):
        index_adjusted_force[i,0] = ds.force[i,0]
        index_adjusted_force[i,1] = ds.force[i+1,1]
 


    
    plt.plot(ds.joint[:,1], ds.other['motor_current_from_feedback'][:,1]* reflected_motor_constant - ds.force[:,1] + MOTOR2LIN*(ds.motor_vel[:,1]*reflected_motor_damping- filter_lp_diff(ds.motor_vel)[:,1]*reflected_motor_mass), label = "Force Error w/o DOB w/vel and acc correction")


    plt.plot(ds.joint[:,1], ds.other['cmd_motor_current'][:,1]* reflected_motor_constant - ds.force[:,1] + MOTOR2LIN*(ds.motor_vel[:,1]*reflected_motor_damping- filter_lp_diff(ds.motor_vel)[:,1]*reflected_motor_mass), label = "Force Error w/ DOB")

    plt.grid()
    plt.legend()

    plt.figure()
    plt.plot(ds.motor[:,0],ds.motor[:,1], label = "Motor pos")

    plt.plot(ds.motor[:,0],filter_lp_diff(ds.motor)[:,1], label = "Motor vel from filter")
    plt.plot(ds.motor_vel[:,0], ds.motor_vel[:,1])
    plt.legend()
    plt.grid()

    # plt.figure()
    # plt.plot(ds.joint[:,1],ds.other['cmd_motor_current'][:,1] - ds.other['dob::current_adjustment'][:,1])
    # plt.plot(ds.joint[:,1],ds.other['motor_current_from_feedback'][:,1] )


    # a = np.transpose(np.array([ds.other['dob::current_adjustment'][:,0] ,ds.other['dob::current_adjustment'][:,1] * reflected_motor_constant]))

    # plt.figure()
    # plt.plot(ds.other['dob_u'][:,0], ds.other['dob_u'][:,1])

    # #find dirty derivative of motor_vel
    # motor_acc = np.zeros(ds.motor_vel.shape)
    # motor_acc[0,0] = ds.motor_vel[0,0]

    # for j in range(1, len(ds.motor_vel)):
    #     motor_acc[j,1] = (ds.motor_vel[j,1] - ds.motor_vel[j-1,1])/(ds.motor_vel[j,0] - ds.motor_vel[j-1,0])
    #     motor_acc[j,0] = ds.motor_vel[j,0]




    # # plt.plot(motor_acc[:,0], motor_acc[:,1]/100)
    # # plt.plot(ds.motor_vel[:,0], ds.motor_vel[:,1])

    # pn_inv_y = np.transpose(np.array([motor_acc[:,0], motor_acc[:,1] * reflected_motor_mass * MOTOR2LIN + ds.motor_vel[:,1] * reflected_motor_damping * MOTOR2LIN]))
    # plt.plot(pn_inv_y[:,0], pn_inv_y[:,1])

    # d = np.transpose(np.array([pn_inv_y[:,0], -(pn_inv_y[:,1] - ds.other['dob_u'][:,1])])) 


    # bodeplot.scatter(a,d)

    # omega = 25
    # damping = 0.4
    # Q = lambda s: omega**2 / (s*s + 2*damping*omega*s + omega**2) 
    # bodeplot.plot_system(Q) 


    #New figure comparing two tests


def main4():
    reflected_motor_mass = 231.0 # in linear space (kg)
    reflected_motor_damping = 3465.0 # in linear space (Ns/m)
    reflected_motor_constant = 186.176 # in units of N/A
    MOTOR2LIN  = 1.2732e-4

    ll = 20000
    ul = -20000

    if False:
        ds = DataSet(88, csv=False, skip=1)
        ds_dob = DataSet(89, csv=False, skip=1)

        plt.figure()
        plt.title("Commanded vs Measured Joint Force Error (with damping and acceleration subtracted)")
        plt.xlabel("Joint Position (rad)")
        plt.ylabel("Force (N)")
        plt.plot(ds.joint[ll:ul,1], ds.other['cmd_motor_current'][ll:ul,1]* reflected_motor_constant - ds.force[ll:ul,1] 
        - MOTOR2LIN*(ds.motor_vel[ll:ul,1]*reflected_motor_damping- filter_lp_diff(ds.motor_vel)[ll:ul,1]*reflected_motor_mass), label = "Force Error w/o DOB")

        plt.plot(ds_dob.joint[ll:ul,1], (ds_dob.other['cmd_motor_current'][ll:ul,1] - ds_dob.other['dob::current_adjustment'][ll:ul,1])* reflected_motor_constant - ds_dob.force[ll:ul,1] 
        - MOTOR2LIN*(ds_dob.motor_vel[ll:ul,1]*reflected_motor_damping- filter_lp_diff(ds_dob.motor_vel)[ll:ul,1]*reflected_motor_mass), label = "Force Error w DOB")



        plt.grid()
        plt.legend()


    #Now try a hyteresis plot
    #So for the hyteresis plot of work, we need to slap a position controller 
    if False:
        plt.figure()
        plt.title("Force vs Position")
        plt.xlabel("Joint Position (rad)")
        plt.ylabel("Spring Force (N)")
        plt.plot(ds.joint[ll:ul,1], ds.force[ll:ul,1], label = "No DOB")
        plt.plot(ds_dob.joint[ll:ul,1], ds_dob.force[ll:ul,1], label = "With DOB")


        plt.legend()
        plt.grid()

    if False:
        #Plot error with DOB on and DOB off
        ds1 = DataSet(89, csv=False, skip=1)    
        fig = plt.figure(figsize=(3.5,2.5))
        plt.title("Equation Error")
        plt.xlabel("Joint Position (rad)")
        plt.ylabel("Force (N)")

        plt.plot(ds1.joint[ll:ul,1], ds1.other['cmd_motor_current'][ll:ul,1]* reflected_motor_constant - ds1.force[ll:ul,1] 
        - MOTOR2LIN*(ds1.motor_vel[ll:ul,1]*reflected_motor_damping + filter_lp_diff(ds1.motor_vel, alpha = 0.95)[ll:ul,1]*reflected_motor_mass), lw=0, marker='.', ms=2.0, alpha=.5, mec='None',  label = "Force Error w/o DOB")

        plt.plot(ds1.joint[ll:ul,1], ds1.other['dob::current_adjustment'][ll:ul,1] * reflected_motor_constant, lw=0, marker='.', ms=2.0, alpha=.5, mec='None', label = 'DOB Adjustment')

        plt.plot(ds1.joint[ll:ul,1], (ds1.other['cmd_motor_current'][ll:ul,1] - ds1.other['dob::current_adjustment'][ll:ul,1])* reflected_motor_constant - ds1.force[ll:ul,1] 
        - MOTOR2LIN*(ds1.motor_vel[ll:ul,1]*reflected_motor_damping + filter_lp_diff(ds1.motor_vel, alpha = 0.95)[ll:ul,1]*reflected_motor_mass),lw=0, marker='.',  ms=2.0, alpha=.5, mec='None', label = "Force Error w DOB")



        # plt.legend()
        plt.grid()
        # plt.tight_layout()
        fig.savefig("DataSet89_DOB_plot.png",dpi=600)

    plt.show()


if __name__ == '__main__':
    main()


    # ds0 = DataSet(0, csv=False) # motor forced, locked ouptut
    # ds1 = DataSet(1, skip=1, csv=False) # cuff forced, no current
    # ds1 = DataSet(2, csv=False, skip=1) # arm forced, no current
    # ds1 = DataSet(3, csv=False, skip=1) # arm forced, kp=100, kd=0
    # ds1 = DataSet(4, csv=False, skip=1) # steady state cuff forced, kp=100, kd=0
    # ds1 = DataSet(5, csv=False, skip=1) # steady state cuff forced, kp=100, kd=0
    # ds1 = DataSet(6, csv=False, skip=1) # arm forced, no feedback kp = 0, kd = 0
    # ds1 = DataSet(7, csv=False, skip=1)
    # ds1 = DataSet(8, csv=False, skip=1) #first test on 12/21/2018, arm fixed with chirp_amplitude = 4
    # ds1 = DataSet(9, csv=False, skip=1) #test with arm fixed in upper position, chirp amplitude = 1 from .01 to 100 Hz
    # ds1 = DataSet(10, csv=False, skip=1)
    # ds1 = DataSet(11, csv=False, skip=1)
    '''
    ss_time_partition = [()]

    ds=ds1
    sys_7_position = combine_data([ds.joint, ds.loadtorque], [1, -1./k2])
    sys_7_torque = combine_data([ds.loadtorque],[-1])
    sys_5_position = combine_data([ds.joint], [1])
    sys_5_torque = combine_data([ds.loadtorque],[-1])
    sys_4_position = combine_data([ds.joint], [1])
    sys_4_torque = combine_data([ds.torque],[-1])
    sys_1_position = combine_data([ds.motor], [-calib_motor_rad_2_meters])
    sys_1_torque = combine_data([ds.torque],[-1])
    motor_current = combine_data([ds.current], [1])
    motor_force_from_spring = combine_data([ds.force],[-1]) #This is measured by the spring deflection
    motor_force_from_current = combine_data([ds.current], [0.0276/calib_motor_rad_2_meters]) #27 mNm/A is motor constant from Maxon for 305015 motor 
 
    # inf_motor_force = combine_data([ds.infloadforce], [1]) #this is actually from the load cell, so is all noise

    bode = rdouble_bode.compliance_bode 
    kwargs = dict(alpha=.4, zorder=-5, marker='.', linestyle='none')
    # bode.scatter(ds.joint, ds.inf_motor_torque, label="joint / motor torque", **kwargs)
    # bode.scatter(ds.joint, ds.torque, label="joint / spring torque", **kwargs)
    # bode.scatter(ds.joint, ds.loadtorque, label="joint / load torque", **kwargs)
    # bode.scatter(sys_1_position, sys_1_torque, label="System 2 Exp.", **kwargs)
    # bode.scatter(sys_4_position, sys_4_torque, label="System 4 Exp.", **kwargs)
    # bode.scatter(sys_5_position, sys_5_torque, label="System 5 Exp.", **kwargs)
    # bode.scatter(sys_7_position, sys_7_torque, label="System 7 Exp.", **kwargs)
    # print (ds.ss_time_cuts)
    # for start_t, end_t in ds.ss_time_cuts:


    # double_bode.compliance_bode.scatter(sys_1_position, motor_force_from_spring, label = 'force from spring', **kwargs)
    double_bode.compliance_bode.scatter(sys_1_position, motor_force_from_current, label = 'force from current', **kwargs)
    #system 1 torque same as system 4 torque

    # exit(0)
    # plt.figure()
    # plt.plot(*plot_discrete(ds.torque), label="torque")
    # plt.plot(*plot_discrete(ds.loadtorque), label="loadtorque")
    # plt.plot(*plot_discrete(ds.current), label="current")
    # plt.plot(*plot_discrete(ds.cmd), label="cmd")
    # plt.plot(*plot_discrete(ds.actuator), label="actuator")
    # plt.plot(*plot_discrete(ds.motor), label="motor")
    # plt.plot(*plot_discrete(ds.motor_vel), label="motor_vel")
    # plt.plot(*plot_discrete(ds.joint), label="joint")
    # plt.legend()


    #stuff for finding out position vs torque 
    # linear_pos = combine_data([ds.actuator],[1])
    # maxon_pos = combine_data([ds.motor],[1])
    # print(linear_pos.shape)
    # plt.figure()
    # plt.plot(maxon_pos[:][1], linear_pos[:][1], label = 'asdf')
    # print(np.polyfit(maxon_pos[:][1], linear_pos[:][1],1)) 
    # plt.legend()
    # plt.grid(1)
'''