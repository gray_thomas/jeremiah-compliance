""" Plots transfer functions representing time-delay proportional feedback."""
import numpy as np
import matplotlib.pyplot as plt
from admittance_plotting_utils import get_cross_and_power, fft_plot

def combine_series(sys_in,series):
    compliance,input_compliance = sys_in
    compliance_out = lambda s: compliance(s) + series(s)
    
    
    return (compliance_out,input_compliance)
    
def combine_parallel(sys_in,parallel):
    compliance,input_compliance = sys_in
    compliance_out = lambda s: 1/(1/compliance(s) + 1/parallel(s))
    
    input_compliance_out = lambda s: (compliance_out(s)/compliance(s)
                                     * input_compliance(s))
    
    
    return (compliance_out, input_compliance_out)
    
def virtual_parallel(sys_in, position_controller):
    compliance, input_compliance = sys_in
    parallel_out = lambda s: -1/position_controller(s) * compliance(s)/input_compliance(s)
    return parallel_out
    
def virtual_series(sys_in, force_controller):
    compliance, input_compliance = sys_in
    series_out = lambda s: input_compliance(s) * force_controller(s)
    return series_out

class DoubleBode(object):
    def __init__(self):
        self.compliance_bode = BodePlot()
        self.input_bode = BodePlot()
    
    def plot_system(self,system, name = 'name', **kwargs):
        
        self.compliance_bode.plot_system(system[0], suptitle = 'Compliance', name = name, **kwargs)
        self.input_bode.plot_system(system[1], suptitle = 'Input Compliance', name = name, **kwargs)
        
        
class BodePlot(object):
    def __init__(self):
        self.mag_ax = None
        self.phase_ax = None
        self.fig = None
        self.frequency = None
        self.omegas = None
        self.laplace_s_list = None
    
    def plot_system(self, system, suptitle = 'title', **kwargs):
        
        self.lazy_init_plot()
        self.mag_ax.loglog(self.frequency, abs(system(self.laplace_s_list)), **kwargs)
        self.phase_ax.semilogx(self.frequency, 180/np.pi*np.angle(system(self.laplace_s_list)), **kwargs)

        self.mag_ax.set_ylabel("magnitude")
        self.phase_ax.set_ylabel("phase (deg)")
        self.phase_ax.set_xlabel("frequncy (Hz)")
        self.phase_ax.set_yticks([-180, -90, 0, 90, 180])
        self.phase_ax.grid(True, 'both')
        self.mag_ax.grid(True, 'both')

        self.fig.suptitle(suptitle)
        self.phase_ax.legend()
        
    def scatter(self, signalA, signalB, **kwargs):
        self.lazy_init_plot()
        cross, power, f = get_cross_and_power(signalA, signalB)
        transfer = cross / power
        self.mag_ax.loglog(f, abs(transfer), **kwargs)
        self.phase_ax.semilogx(f, 180./np.pi*np.angle(transfer), **kwargs)  
        self.phase_ax.set_yticks([-180, -90, 0, 90, 180])
        self.phase_ax.grid(True, 'both')
        self.mag_ax.grid(True, 'both')
        self.phase_ax.legend()

    def scatter1(self, f, transfer, **kwargs):
        self.lazy_init_plot()
        self.mag_ax.loglog(f, abs(transfer), **kwargs)
        self.phase_ax.semilogx(f, 180./np.pi*np.angle(transfer), **kwargs) 
        self.phase_ax.set_yticks([-180, -90, 0, 90, 180])
        self.phase_ax.grid(True, 'both')
        self.mag_ax.grid(True, 'both') 
        self.phase_ax.legend()
    
    def lazy_init_plot(self):
        if np.any(np.array([var is None for var in [
                self.mag_ax, self.phase_ax, self.fig, self.frequency, 
                self.omegas, self.laplace_s_list
                ]])):
            self.fig, (self.mag_ax, self.phase_ax) = plt.subplots(2,1,
                                                sharex=True)
            self.frequency = np.logspace(-1,4,4000)
            self.omegas = np.pi*2*self.frequency
            self.laplace_s_list = complex(0, 1) * self.omegas
   

def butterworth_poly_2(butterworth_lambda_hz):
    l = butterworth_lambda_hz*2*np.pi
    return lambda s: (s/l)**2+1.414*(s/l)+1

def butterworth_poly_3(butterworth_lambda_hz):
    l = butterworth_lambda_hz*2*np.pi
    return lambda s: (s/l)**3+2*(s/l)**2+2*(s/l)+1

def main():
    ## Parameters ##
    j_m = 231 # reflected motor inertia (Kg = Ns^2/m)
    b_m = 3465 # reflected motor damping (Ns/m)
    g_m = 186.176 # reflected motor constant (N/amp)
    k_s = 562100 # SEA spring constant (N/m)
    j_e = .1399 # exoskeleton inertia (Nm s^2/rad = Kg m^2)
    b_e = 1.1 # exoskeleton damping (Nm s/rad)
    k_c = 250 # Cuff spring (Nm/rad)

    # k_c = 250*.25 # Cuff spring (Nm/rad) # softer spring (easier)
    extra_j_e = .1415 # adding the 660 g weight to the arm (Nm s^2/rad = Kg m^2)
    # extra_j_e = .1415 * 2420/660 # heavier weight (easier)
    k_e = 0*250000 # exo connection to rigid world (usually zero. Simulates contact)
    j_e += extra_j_e
    j_h = .01377 # Kg m^2

    time_delay = .004 # time delay (s) (tuned by finding out when controllers go unstable (Kp1000 and Kd10 both unstable) (Kp700 + Kd7 stable))
    trans_ratio = 0.02 # transmission ratio (m)
    print("kc", k_c, "k_s/r/r", k_s*trans_ratio**2)

    ## Controller parameters
    K_1 = 0e6 +1e-12 # virtual motor spring N/m  [< 6.5e6 (9e6!) ]
    B_1 = 0+1e-12  #-3.0e3 # virtual motor damper Ns/m [< 6.5e4 (9e4!) ] 
    K_2 = 0+1e-12  #1.0 # spring amplificaiton, N per N
    B_2 = 0+1e-12 # 1e-1 # spring kd, N s per N

    # B_3 = .05# cuff kd, Nm s per Nm

    tilde_K_1 = K_1/j_m
    tilde_K_2 = (k_s+K_1+K_2)/j_m
    tilde_B_1 = (b_m+B_1)/j_m
    tilde_B_2 = (b_m+B_1+B_2)/j_m

    hat_K_1 = K_1*k_s/(k_s+K_1+K_2) # choose zero
    hat_B_1 = (b_m+B_1)*k_s/(k_s+K_1+K_2) # choose 0.067
    # intersection with j_e s*s: j_e * w = hat_B_1 ->w = hat_B_1/j_e
    w_intersection = hat_B_1/j_e
    # zeros should be above this

    
    # force_adjustment = lambda s: (j_m*s*s+(b_m+B_1)*s+(k_s+K_1))/(k_s*tau_butt(s))
    # force_adjustment = lambda s: ((k_s+K_1))/(k_s*tau_butt(s))

    # K_1, B_1, K_2, B_2, K_3, B_3 = 0, 1e4, -.9, 1e-7,0,0 # more damped than normally stable

    motor_v_filt_freq_hz = 500
    spring_v_filt_freq_hz = 120
    cuff_v_filt_freq_hz = 120

    print("spring filter alpha", np.exp(-spring_v_filt_freq_hz*2*np.pi*0.001))
    print("cuff filter alpha", np.exp(-cuff_v_filt_freq_hz*2*np.pi*0.001))
    
    b_robot = 0
    alpha = 10
    #################
    
    double_bode = DoubleBode()
    rdouble_bode = DoubleBode()

    
    system1 = (lambda s: (1/(b_m*s + j_m*s*s)),
               lambda s: (np.exp(-time_delay * s)/(b_m*s + j_m*s*s))) 
    rsystem1 =  (lambda s: system1[0](s)/trans_ratio**2, lambda s: system1[1](s)/trans_ratio**2)
               
    parallel1 = virtual_parallel(system1, lambda s: -K_1 - B_1*s/(s/(2*np.pi*motor_v_filt_freq_hz)+1))
    
    system2 = combine_parallel(system1, parallel1)
    rsystem2 =  (lambda s: system2[0](s)/trans_ratio**2, lambda s: system2[1](s)/trans_ratio**2)
    
    f_control = lambda s: (K_2+B_2*s/(s/(2*np.pi*spring_v_filt_freq_hz)+1))
    
    series2 = virtual_series(system2, f_control)
    
    system3 = combine_series(system2, series2)

    rsystem3 =  (lambda s: system3[0](s)/trans_ratio**2, lambda s: system3[1](s)/trans_ratio**2)
    
    series3 = lambda s: 1/(k_s+0*s)
    rseries3 = lambda s: 1/(k_s+0*s)/trans_ratio**2
    
    system4 = combine_series(system3, series3)
    
    rsystem4 = (lambda s: system4[0](s)/trans_ratio**2, lambda s: system4[1](s)/trans_ratio**2)
    
    rparallel4 = lambda s: 1/(j_e*s*s + b_e*s+ k_e)
    
    rsystem5 = combine_parallel(rsystem4, rparallel4)
    
    # lp_w = 100*np.pi*2
    # hp_w = 31*np.pi*2
    # torque_control5 = lambda s: (f_control(s)+1)*alpha*(1+s/hp_w)*(1/((s/lp_w)**2+.9*(s/lp_w)+1))

    # Cuff filter design

    K_3 = 5 # cuff kp, Nm per Nm
    # B_3 = K_3/(2*np.pi*8) #0.0795774
    butterworth_lambda_hz=100

    so_w = 2*np.pi*100
    so_w2 = 2*np.pi*120
    so_w3 = 2*np.pi*150
    tau_butt = butterworth_poly_3(butterworth_lambda_hz)
    # accounting for the difference in gain definitions in my paper: B_2 is B_2*k_s (since it multiplies spring force, not spring deflection)
    force_adjustment2 = lambda s: (j_m*s*s+(b_m+B_1+B_2*k_s)*s+(k_s+K_1+K_2*k_s))/(k_s*tau_butt(s))#/(s/(2*np.pi*3)+1)*(s/(2*np.pi*10)+1)
    force_adjustment2 = lambda s: (
        (j_m*s*s+(b_m+B_1+B_2*k_s)*s+(k_s+K_1+K_2*k_s))/(k_s) 
        * so_w*so_w/(s*s+2*s*.5*so_w+so_w*so_w)
        * so_w2*so_w2/(s*s+2*s*.15*so_w2+so_w2*so_w2)
        # * so_w3*so_w3/(s*s+2*s*so_cuff_filt_damp*so_w3+so_w3*so_w3)
        )

    force_adjustment= lambda s: rsystem5[0](s)/rsystem5[1](s)*np.exp(-time_delay * s)
    torque_control5 = lambda s: (K_3+2*np.pi*8*K_3*s/(s/(2*np.pi*cuff_v_filt_freq_hz)+1))*force_adjustment2(s)
    torque_control5 = lambda s: K_3*(1+2*np.pi*8*s)*(
        (j_m*s*s+(b_m+B_1+B_2*k_s)*s+(k_s+K_1+K_2*k_s))/(k_s) 
        * so_w*so_w/(s*s+2*s*.5*so_w+so_w*so_w)
        * so_w2*so_w2/(s*s+2*s*.15*so_w2+so_w2*so_w2)
        # * so_w3*so_w3/(s*s+2*s*so_cuff_filt_damp*so_w3+so_w3*so_w3)
        ) ## assume filter is used for deriv.
    tcz = 2*np.pi*8
    tcp = 2*np.pi*9
    tcp2 = 2*np.pi*25
    torque_control5 = lambda s: (K_3*(s*s+2*.25*tcz*s+tcz*tcz)/tcz/tcz*
    1/(s+tcp)*tcp*
    1/(s+tcp2)*tcp2*
    
        (j_m*s*s+(b_m+B_1+B_2*k_s)*s+(k_s+K_1+K_2*k_s))/(k_s) 
        * so_w*so_w/(s*s+2*s*.2*so_w+so_w*so_w)
        # * so_w2*so_w2/(s*s+2*s*.15*so_w2+so_w2*so_w2)
        # * so_w3*so_w3/(s*s+2*s*so_cuff_filt_damp*so_w3+so_w3*so_w3)
        ) # now with the hard zero
    
    rseries5 = virtual_series(rsystem5, torque_control5)

    # rseries5 = lambda s: (K_3+B_3*s)*rsystem5[0](s)*np.exp(-time_delay * s)
    
    rsystem6 = combine_series(rsystem5, rseries5)
    
    rseries6 = lambda s: 1/(0*s+k_c)
    
    rsystem7 = combine_series(rsystem6, rseries6)

    amplification = lambda s: (rsystem6[0](s)-rsystem5[0](s))/rsystem5[0](s)
    

    # double_bode.plot_system(system1, name = 'System 1')
    # double_bode.plot_system(system2, name = 'System 2')
    # double_bode.plot_system(system3, name = 'System 3')
    # double_bode.plot_system(system4, name = 'System 4')
    rdouble_bode.plot_system(rsystem1, name = 'Rotary System 1')
    rdouble_bode.plot_system(rsystem2, name = 'Rotary System 2')
    rdouble_bode.plot_system(rsystem3, name = 'Rotary System 3')
    rdouble_bode.plot_system(rsystem4, name = 'Rotary System 4')
    rdouble_bode.plot_system(rsystem5, name = 'Rotary System 5', lw=3, dashes=(4,1,.01,1), dash_capstyle="round")
    rdouble_bode.plot_system(rsystem6, name = 'Rotary System 6')
    rdouble_bode.plot_system(rsystem7, name = 'Rotary System 7', lw=3, dashes=(3,1,.01,1,.01,1), dash_capstyle="round")
    # rdouble_bode.compliance_bode.plot_system(rseries3, name="SEA Spring", linestyle=":", suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(rparallel4, name="Exo Mass", linestyle=":", suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(rseries6, name="Cuff Spring", linestyle=":", suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(amplification, name="Amplification", linestyle=":", suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(force_adjustment, name="Force Adjustement", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    # rdouble_bode.compliance_bode.plot_system(force_adjustment2, name="Force Adjustement?", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    rdouble_bode.compliance_bode.plot_system(rseries5, name="Rotary series 5", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    rdouble_bode.compliance_bode.plot_system(lambda s: 1/(j_h*s*s), name="Human inertia", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    rdouble_bode.compliance_bode.plot_system(lambda s: -np.exp(-.004*s), name="-time delay", linestyle=":",dash_capstyle="round",  suptitle="Compliance")
    


    plt.show()


if __name__ == '__main__':
    main()
