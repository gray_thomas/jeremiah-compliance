# Gray Thomas, NSTRF15AQ33H at NASA JSC August 2018
# Works in python 3
import h5py
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import os, os.path
from admittance_plotting_utils import *
from collections import namedtuple

logfolder = "/home/apptronik/log/"
# logfolder = "/home/gray/wk/jeremiah-compliance/data/"
logfolder = "../data/"
# logfolder = "C:/Users/j_coh/Box/Compliant P0 Data from UT/data/" #allows Jeremiah to "telecommute" or "work from home"

class DataSet(object):
    def __init__(self, filename, csv=True, **kwargs):
        if isinstance(filename, str):
            if csv: 
                self.get_data_csv(filename)
                f=self.save_hdf5(filename, **kwargs)
            else:
                self.get_data_hdf5(filename, **kwargs)
        else:
            self.get_data_hdf5_by_number(int(filename), **kwargs)
        self.inferr_additional_data()

    def get_data_csv(self, filename):
        with open('/home/apptronik/log/'+filename+".csv", 'r') as f:
            labels = [st.strip() for st in next(f).split(",")]
            print (labels)
            data = np.genfromtxt(f, delimiter=",", skip_header=200)
        
        # ['time'
        # '/Axon_proto_v1/miso/diag__faults__x'
        # '/Axon_proto_v1/miso/actuator__force__N'
        # '/Axon_proto_v1/miso/js__joint__effort__Nm'
        # '/Axon_proto_v1/miso/cuff__ati__torque__Nm'
        # '/Axon_proto_v1/miso/js__joint__position__rad'
        # '/Axon_proto_v1/miso/motor__current__A'
        # '/Axon_proto_v1/miso/actuator__position__m'
        # '/Axon_proto_v1/miso/motor__position__Rad'
        # '/Axon_proto_v1/miso/motor__velocity__Radps'
        # '/Axon_proto_v1/miso/ati__Fx__N'
        # '/Axon_proto_v1/miso/ati__Fy__N'
        # '/Axon_proto_v1/miso/ati__Fz__N'
        # '/Axon_proto_v1/miso/ati__Tx__Nm'
        # '/Axon_proto_v1/miso/ati__Ty__Nm'
        # '/Axon_proto_v1/miso/ati__Tz__Nm'
        # '/Axon_proto_v1/mosi/cmd__motor__effort__a',
        # 'dob2_u_prime',
        # 'dob2_desired_motor_pos',
        # 'dob2_motor_position_error',
        # 'dob2_Q_pos',
        # 'dob2_force_adjustment',
        # 'dob2_motor_adjustment_pos',
        # 'dob2_current_adjustment']


        for i in range(1,
            len(labels)):
            x = np.array(list(data[:,i]))
            xi = np.array(np.arange(len(x)))
            mask = np.isfinite(x)
            xfiltered = np.interp(xi, xi[mask], x[mask])
            data[:,i] = xfiltered

        self.force = np.array(data[:, [0, 2]]) # Newtons (spring)
        self.torque = np.array(data[:, [0, 3]]) # Newton meters (spring)
        self.loadtorque = np.array(data[:, [0, 4]]) # Newton meters (cell)
        self.joint = np.array(data[:, [0, 5]]) # rad
        self.current = np.array(data[:, [0, 6]]) # amps
        self.actuator = np.array(data[:, [0, 7]]) # meters
        self.motor = np.array(data[:, [0, 8]]) # Rad (motor-side)
        self.motor_vel = np.array(data[:, [0, 9]]) # Rad / s (motor-side)
        self.fx = np.array(data[:, [0,10]]) # '/Axon_proto_v1/miso/ati__Fx__N'
        self.fy = np.array(data[:, [0,11]]) # '/Axon_proto_v1/miso/ati__Fy__N'
        self.fz = np.array(data[:, [0,12]]) # '/Axon_proto_v1/miso/ati__Fz__N'
        self.tx = np.array(data[:, [0,13]]) # '/Axon_proto_v1/miso/ati__Tx__Nm'
        self.ty = np.array(data[:, [0,14]]) # '/Axon_proto_v1/miso/ati__Ty__Nm'
        self.tz = np.array(data[:, [0,15]]) # '/Axon_proto_v1/miso/ati__Tz__Nm'
        self.cmd = np.array(data[:, [0, 16]]) # Amps (desired)

        self.other={}
        for n in range(17, len(labels)):
            self.other[labels[n]]=np.array(data[:, [0,n]]) # all internal control variables (use names from code)

    def get_data_hdf5(self, filename, skip=10):
        newfilename = self.get_hdf5_filename(filename)
        self.get_data_hdf5_by_truename(newfilename, skip=skip)


    def get_data_hdf5_by_number(self, desired_number, skip=10):
        logs = sorted([name for name in os.listdir(logfolder) if os.path.isfile(logfolder+name)])
        hdf5_xlogs = [name for name in logs if name[-5:]==".hdf5" and name[:5]=="xlog-"]
        hdf5_xlog_numbers = [name[5:8] for name in hdf5_xlogs]
        for number, newfilename in zip(hdf5_xlog_numbers, hdf5_xlogs):
            if desired_number==int(number):
                return self.get_data_hdf5_by_truename(newfilename, skip=skip)

        raise RuntimeError("No such file")


    def get_data_hdf5_by_truename(self, newfilename, skip=10):
        with h5py.File(logfolder+newfilename, 'r') as f:
            print(f)
            self.print_hello_message(newfilename, f)
            if 'ss_time_cuts' in f.attrs: self.ss_time_cuts = np.array(f.attrs['ss_time_cuts'])
            self.time = np.array(f['time'])
            self.force = np.array(f['force'])[::skip,:]
            self.torque = np.array(f['torque'])[::skip,:]
            self.loadtorque = np.array(f['loadtorque'])[::skip,:]
            self.joint = np.array(f['joint'])[::skip,:]
            self.current = np.array(f['current'])[::skip,:]
            self.actuator = np.array(f['actuator'])[::skip,:]
            self.motor = np.array(f['motor'])[::skip,:]
            self.motor_vel = np.array(f['motor_vel'])[::skip,:]
            if 'fx' in f:
                self.fx = np.array(f['fx'])[::skip,:]
                self.fy = np.array(f['fy'])[::skip,:]
                self.fz = np.array(f['fz'])[::skip,:]
                self.tx = np.array(f['tx'])[::skip,:]
                self.ty = np.array(f['ty'])[::skip,:]
                self.tz = np.array(f['tz'])[::skip,:]


            self.cmd = np.array(f['cmd'])[::skip,:]
            if 'other' in f:
                self.other = dict()
                for name, value in f['other'].items():
                    self.other[name] = np.array(value)[::skip,:]


    def print_hello_message(self, newfilename, f):
        print(f)
        print(f['time'])
        print(np.array(f['time/day']))
        print(":::::Opening hdf5 %s"%newfilename)
        self.filename=newfilename
        print("\tfrom %d:%d on %d %s %d" % (
            np.array(f['time/hour']), 
            np.array(f['time/minute']), 
                np.array(f['time/day']), 
            {
            1:"January", 2:"February", 3:"March", 4:"April", 5:"May", 6:"June",
            7:"July", 8:"August", 9:"September", 10:"October", 11:"November", 12:"December"
            }[int(np.array(f['time/month']))], 
            np.array(f['time/year'])))
        print(f.attrs["description"])
        print(list(f.keys()))
        print(":::::")

    def get_hdf5_filename(self, filename):
        hdf5_xlog_formatter = "xlog-%03d-(%s).hdf5"
        # print(filename)
        tloc = filename.find("T")
        logfolder = "/home/apptronik/log/"
        logs = sorted([name for name in os.listdir(logfolder) if os.path.isfile(logfolder+name)])
        hdf5_xlogs = [name for name in logs if name[-5:]==".hdf5" and name[:5]=="xlog-"]
        hdf5_xlog_timestrings = [name[10:25] for name in hdf5_xlogs]
        hdf5_xlog_numbers = [name[5:8] for name in hdf5_xlogs]
        logtimestring = filename[tloc-8:tloc+7]
        # print(logs)
        # print(hdf5_xlogs)
        # print(hdf5_xlog_timestrings)
        # print(hdf5_xlog_numbers)
        # print(logtimestring)
        newlog=True
        newnumber=len(hdf5_xlogs)
        for number, time in zip(hdf5_xlog_numbers, hdf5_xlog_timestrings):
            if time==logtimestring:
                newlog=False
                newnumber=int(number)
                break
            if int(number)>=newnumber:
                newnumber=int(number)+1
        # print("newlog", newlog, "newnumber", newnumber)
        # print(hdf5_xlog_formatter%(1, logtimestring))

        newfilename = hdf5_xlog_formatter%(newnumber, logtimestring)
        return newfilename


    def save_hdf5(self, filename, **kwargs):

        newfilename = self.get_hdf5_filename(filename)
        print(newfilename)

        f = h5py.File('/home/apptronik/log/'+newfilename, 'w')
        tloc = filename.find("T")
        logtimestring = filename[tloc-8:tloc+7]
        f['time/year'] = int(filename[(tloc-8):(tloc-4)])
        f['time/month'] = int(filename[(tloc-4):(tloc-2)])
        f['time/day'] = int(filename[(tloc-2):(tloc)])
        f['time/hour'] = int(filename[tloc+1:tloc+3])
        f['time/minute'] = int(filename[tloc+3:tloc+5])
        f['time/second'] = int(filename[tloc+5:tloc+7])
        f.attrs['name'] = logtimestring

        for key, value in kwargs.items():
            f.attrs[key] = value

        optlevel = 4
        dset_kwargs = dict(dtype='f8', chunks=True, compression="gzip", compression_opts=9)

        dforce = f.create_dataset("force", self.force.shape, **dset_kwargs)
        dforce[:,:] = self.force
        f['force'].attrs['name'] = "spring force"
        f['force'].attrs['units']='Newtons'
        f['force'].attrs['ultimate sensor']='spring'
        f['force'].attrs['reference']='actuator frame'
        dtorque = f.create_dataset("torque", self.torque.shape, **dset_kwargs)
        dtorque[:,:] = self.torque
        f['torque'].attrs['name'] = "spring torque"
        f['torque'].attrs['units']='Newton meters'
        f['torque'].attrs['ultimate sensor']='spring'
        f['torque'].attrs['reference']='joint frame'
        dloadtorque = f.create_dataset("loadtorque", self.loadtorque.shape, **dset_kwargs)
        dloadtorque[:,:] = self.loadtorque
        f['loadtorque'].attrs['name'] = "load cell torque"
        f['loadtorque'].attrs['units']='Newton meters'
        f['loadtorque'].attrs['ultimate sensor']='load cell'
        f['loadtorque'].attrs['reference']='joint frame'
        djoint = f.create_dataset("joint", self.joint.shape, **dset_kwargs)
        djoint[:,:] = self.joint
        f['joint'].attrs['name'] = "f angle"
        f['joint'].attrs['units']='radians'
        f['joint'].attrs['ultimate sensor']='joint encoder'
        f['joint'].attrs['reference']='joint frame'
        dcurrent = f.create_dataset("current", self.current.shape, **dset_kwargs)
        dcurrent[:,:] = self.current
        f['current'].attrs['name'] = "measured current"
        f['current'].attrs['units']='amperes'
        f['current'].attrs['ultimate sensor']='current measurement on the elmo'
        f['current'].attrs['reference']='electrical frame'
        dactuator = f.create_dataset("actuator", self.actuator.shape, **dset_kwargs)
        dactuator[:,:] = self.actuator
        f['actuator'].attrs['name'] = "actuator length"
        f['actuator'].attrs['units']='meters'
        f['actuator'].attrs['ultimate sensor']='motor encoder'
        f['actuator'].attrs['reference']='actuator frame'
        dmotor = f.create_dataset("motor", self.motor.shape, **dset_kwargs)
        dmotor[:,:] = self.motor
        f['motor'].attrs['name'] = "motor angle"
        f['motor'].attrs['units']='rad'
        f['motor'].attrs['ultimate sensor']='motor encoder'
        f['motor'].attrs['reference']='motor frame'
        dmotor_vel = f.create_dataset("motor_vel", self.motor_vel.shape, **dset_kwargs)
        dmotor_vel[:,:] = self.motor_vel
        f['motor_vel'].attrs['name'] = "motor angular velocity"
        f['motor_vel'].attrs['units']='rad per second'
        f['motor_vel'].attrs['ultimate sensor']='motor encoder'
        f['motor_vel'].attrs['reference']='motor frame'
        dfx = f.create_dataset("fx", self.fx.shape, **dset_kwargs)
        dfx[:,:] = self.fx
        f['fx'].attrs['name'] = "force sensor fx"
        f['fx'].attrs['units']='Newtons'
        f['fx'].attrs['ultimate sensor']='ATI six axis'
        f['fx'].attrs['reference']='ATI frame'
        dfy = f.create_dataset("fy", self.fy.shape, **dset_kwargs)
        dfy[:,:] = self.fy
        f['fy'].attrs['name'] = "force sensor fy"
        f['fy'].attrs['units']='Newtons'
        f['fy'].attrs['ultimate sensor']='ATI six axis'
        f['fy'].attrs['reference']='ATI frame'
        dfz = f.create_dataset("fz", self.fz.shape, **dset_kwargs)
        dfz[:,:] = self.fz
        f['fz'].attrs['name'] = "force sensor fz"
        f['fz'].attrs['units']='Newtons'
        f['fz'].attrs['ultimate sensor']='ATI six axis'
        f['fz'].attrs['reference']='ATI frame'
        dtx = f.create_dataset("tx", self.tx.shape, **dset_kwargs)
        dtx[:,:] = self.tx
        f['tx'].attrs['name'] = "force sensor tx"
        f['tx'].attrs['units']='Newton meters'
        f['tx'].attrs['ultimate sensor']='ATI six axis'
        f['tx'].attrs['reference']='ATI frame'
        dty = f.create_dataset("ty", self.ty.shape, **dset_kwargs)
        dty[:,:] = self.ty
        f['ty'].attrs['name'] = "force sensor ty"
        f['ty'].attrs['units']='Newton meters'
        f['ty'].attrs['ultimate sensor']='ATI six axis'
        f['ty'].attrs['reference']='ATI frame'
        dtz = f.create_dataset("tz", self.tz.shape, **dset_kwargs)
        dtz[:,:] = self.tz
        f['tz'].attrs['name'] = "force sensor tz"
        f['tz'].attrs['units']='Newton meters'
        f['tz'].attrs['ultimate sensor']='ATI six axis'
        f['tz'].attrs['reference']='ATI frame'
        dcmd = f.create_dataset("cmd", self.cmd.shape, **dset_kwargs)
        dcmd[:,:] = self.cmd
        f['cmd'].attrs['name'] = "commanded current"
        f['cmd'].attrs['units']='amperes'
        f['cmd'].attrs['ultimate sensor']='software'
        f['cmd'].attrs['reference']='electrical frame'

        other_group = f.create_group("other")
        for key, value in self.other.items():
            dtmp = other_group.create_dataset(key, value.shape, **dset_kwargs)
            dtmp[:,:] = value
            f['/other/'+key].attrs['name'] = "internal control variable:"+key

        return f

    def inferr_additional_data(self):

        if "jacobian" in self.other.keys():
            self.infjac = np.array(self.other["jacobian"])
            self.infloadforce = np.array(self.loadtorque)
            self.inf_motor_torque = np.array(self.cmd)
            for i in range(len(self.force)):
                jacobian = self.infjac[i,1]
                self.infloadforce[i,1] = -self.loadtorque[i,1]/jacobian
                self.inf_motor_torque[i,1]= self.cmd[i,1]*jacobian*150
        else:
            self.infjac = np.array(self.force)
            self.infloadforce = np.array(self.force)
            self.inf_motor_torque = np.array(self.force)
            for i in range(len(self.force)):
                jacobian = self.torque[i,1]/self.force[i,1]
                self.infjac[i,1] = jacobian
                self.infloadforce[i,1] = -self.loadtorque[i,1]/jacobian
                self.inf_motor_torque[i,1]= self.cmd[i,1]*jacobian*150

        if hasattr(self, "fz"):
            temp = -self.fz[:,1] * .19264 + (self.ty[:,1]*np.cos(np.pi/180*52.5) - self.tx[:,1]*np.cos(np.pi/180*37.5)) 
            temp = temp - np.mean(temp) #remove dc offest 
            self.ATI_torque = np.transpose( np.array( [self.fz[:,0], temp ] ) ) 
        

# f = h5py.File('2018_08_06_183828_test_logger[-0.0,-0.0,-0.0,-0.0].hdf5')
# open('/home/gray/hdf5_logs/2018_08_06_184256_test_logger.hdf5', 'r')
# f = h5py.File('/home/gray/hdf5_logs/2018_08_06_184256_test_logger.hdf5')
sample_rate = 1000.0 # both at nasa and UT (apptronik p0)
def fft_plot(signal):
    sample_rate = 1000.0
    sample_rate = len(signal)/(signal[-1,0]-signal[0,0])
    print(sample_rate)
    A = np.fft.rfft(signal[:,1])/(sample_rate)
    f = np.fft.rfftfreq(len(signal), d=1./sample_rate)
    return A, f

def angle(n):
    ang = [a if a<=1e-6 else a-2*np.pi for a in np.angle(n)]
    return np.array(ang)

def fix_overphase(freqs, phase):
    nfreqs = []
    nphase = []
    op=phase[0] # oldphase
    for f, p in zip(freqs, phase):
        if abs(op-p)>89:
            nfreqs.append(f)
            nphase.append(float("nan"))
        nfreqs.append(f)
        nphase.append(p)
        op=p
    return nfreqs, nphase

def plot_discrete(data):
    time = [0.0]
    value = [data[0,1]]
    time0 = data[0,0]
    for t, v in data[1:]:
        time.append(t-time0)
        time.append(t-time0)
        value.append(value[-1])
        value.append(v)
    return time, value

def average_tf(ins, n=10):
    # this doesn't seem to help
    kern = np.ones((n,))/n
    outs = []
    for ith_input in ins:
        outs.append(np.convolve(ith_input[1:], kern))
    return outs

def get_cross_and_power(signalA, signalB):
    fftA, f = fft_plot(signalA)
    fftB, fB = fft_plot(signalB)
    assert len(f)==len(fB)
    return fftA*fftB.conjugate(), (fftB*fftB.conjugate()), f

def tf_A_over_B(signalA, signalB, eps=(1e4, 1e8), ax=plt, **kwargs):
    # fftA, f = fft_plot(signalA)
    # fftB, fB = fft_plot(signalB)
    # assert len(f)==len(fB)
    # transfer = fftA*fftB.conjugate() / (fftB*fftB.conjugate())
    cross, power, f = get_cross_and_power(signalA, signalB)
    transfer = cross / power
    scatter_bode_mag(f, transfer, power, eps=eps, ax=ax, **kwargs)

def scatter_bode_mag(f, transfer, power, eps=(1e4, 1e8), ax=plt, **kwargs):
    normalizer = colors.Normalize(vmin=np.log(eps[0]), vmax=np.log(eps[1]), clip=False)
    input_power = normalizer(np.log(np.abs(power)))
    # transfer = np.abs(fftA) / np.abs(fftB)
    ax.loglog(f, abs(transfer), alpha=0.5, **kwargs)
    # ax.scatter(f, abs(transfer), marker='.', c=input_power, cmap="binary")

def scatter_bode_phase(f, transfer, power, eps=(1e4, 1e8), ax=plt, **kwargs):
    normalizer = colors.Normalize(vmin=np.log(eps[0]), vmax=np.log(eps[1]), clip=False)
    input_power = normalizer(np.log(np.abs(power)))
    # transfer = np.abs(fftA) / np.abs(fftB)
    ax.semilogx(f, 180./np.pi*angle(transfer), alpha=.5, **kwargs)
    # ax.scatter(f, 180./np.pi*angle(transfer), marker='.', c=input_power, cmap="binary")

    if ax is plt:
        ax.yticks([-360,-270,-180, -90, 0])
        ax.gca().yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(30))
    else:
        ax.set_yticks([-360,-270,-180, -90, 0])
        ax.yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(30))

def tf_A_over_Bf_split(signalA, signalB, split=500, eps=(1e4, 1e8)):
    n_splits = int(len(signalA)/split)
    cross, power = 0., 0.
    for i in range(n_splits):
        fftA, f = fft_plot(signalA[i*split:(i+1)*split])
        fftB, fB = fft_plot(signalB[i*split:(i+1)*split])
        cross = (fftA*fftB.conjugate())[1:] + cross
        power = (fftB*fftB.conjugate())[1:] + power
        
        assert len(f)==len(fB)
    transfer = cross/power
    normalizer = colors.Normalize(vmin=np.log(eps[0]), vmax=np.log(eps[1]), clip=False)
    input_power = normalizer(np.log(np.abs(power)))
    # transfer = np.abs(fftA) / np.abs(fftB)
    plt.loglog(f[1:], abs(transfer), lw=3, alpha=.8)
    # plt.scatter(f[1:], abs(transfer), marker='.', c=input_power, cmap="bone")

def tf_A_over_B_phase(signalA, signalB, eps=(1e4, 1e8), ax=plt, **kwargs):
    cross, power, f = get_cross_and_power(signalA, signalB)
    transfer = cross / power
    scatter_bode_phase(f, transfer, power, eps=eps, ax=ax, **kwargs)

def tf_A_over_Bf_split_phase(signalA, signalB, split=500, eps=(1e4, 1e8)):
    n_splits = int(len(signalA)/split)
    cross, power = 0., 0.
    for i in range(n_splits):
        fftA, f = fft_plot(signalA[i*split:(i+1)*split])
        fftB, fB = fft_plot(signalB[i*split:(i+1)*split])
        cross = fftA*fftB.conjugate() + cross
        power = fftB*fftB.conjugate() + power
        
        assert len(f)==len(fB)
    transfer = cross/power
    normalizer = colors.Normalize(vmin=np.log(eps[0]), vmax=np.log(eps[1]), clip=False)
    input_power = normalizer(np.log(np.abs(power)))
    # transfer = np.abs(fftA) / np.abs(fftB)
    plt.semilogx(f[1:], 180./np.pi*angle(transfer[1:]), lw=3, alpha=.8)
    # plt.scatter(f, 180./np.pi*angle(transfer), marker='.', c=input_power, cmap="bone")
    plt.yticks([-360,-270,-180, -90, 0])
    plt.gca().yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(30))





# my_data = genfromtxt('my_file.csv', delimiter=',')


# print(dir(g["JointAPS_Angle_Rad"]))
# print(ang[0])
# print(np.array(ang))
# plt.plot(np.array(ang)[:,0]-np.array(ang)[0,0], np.array(ang)[:,1])

def test_fft():
    N = 500000
    x = np.cos(np.linspace(0, 2*np.pi, N+1)[:-1])
    A = np.fft.rfft(x)/(N/2.)
    print (A[1])
    # This demonstrates the need for scaling by 1/(N/2) to preserve intuition about fft magnitudes.
    # But is normally a measure of the integral, so I'm adjusting it to be in units of time*signalunits
    # this also hands the 1/2 factor, since the integral of a squared sinusoid is 1/2 t.


def exp_noise_power_plot(exp, noise, title):
    plt.figure()
    A, f = fft_plot(exp)
    plt.loglog(f, np.abs(A)**2, label="Experiment")
    A, f = fft_plot(noise)
    plt.loglog(f, np.abs(A)**2, label="Noise")
    plt.legend()
    plt.title(title)

def two_label_power_plot(exp, noise, title, label1, label2):
    plt.figure()
    A, f = fft_plot(exp)
    plt.loglog(f, np.abs(A)**2, label=label1)
    A, f = fft_plot(noise)
    plt.loglog(f, np.abs(A)**2, label=label2)
    plt.legend()
    plt.title(title)



def fft_power_info_plots(exp_file, noise_file):
    node, ang, vel, torque, load, current, _ = get_data(exp_file)
    node1, ang1, vel1, torque1, load1, current1, _ = get_data(noise_file)
    
    exp_noise_power_plot(torque, torque1, "Torque (Spring) Power Spectrum")
    exp_noise_power_plot(load, load1, "Torque (Load Cell) Power Spectrum")
    exp_noise_power_plot(ang, ang1, "Position Power Spectrum")

def empty_admittance_plots(suptitle=""):
    figure_1 = plt.figure()
    eps = (1e-1, 1e4)
    plt.title("Position (Admittance) Magnitude")
    plt.suptitle(suptitle)
    plt.grid(True)

    figure_2 = plt.figure()

    # transfer = np.abs(fftA) / np.abs(fftB)

    plt.yticks([-360,-270,-180, -90, 0])
    plt.gca().yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(30))

    plt.title("Position (Admittance) Phase")
    plt.suptitle(suptitle)
    plt.grid(True)
    return figure_1, figure_2

def empty_bode_plot(suptitle=""):
    fig, (ax_mag, ax_phase) = plt.subplots(2, 1, sharex=True)
    ax_mag.set_title("Position-Admittance Magnitude")
    plt.suptitle(suptitle)
    ax_mag.grid(True)

    # transfer = np.abs(fftA) / np.abs(fftB)

    ax_phase.set_yticks([-360,-270,-180, -90, 0])
    ax_phase.yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(30))
    ax_phase.set_xlabel("Frequency (Hz)")

    ax_phase.set_title("Position-Admittance Phase")
    ax_phase.grid(True)
    return ax_mag, ax_phase

def plot_admittance(ang, load, suptitle=""):

    figure_1 = plt.figure()
    eps = (1e-1, 1e4)
    tf_A_over_B(ang, load, eps=eps)
    # tf_A_over_Bf_split(ang, load, split=3000, eps=eps)
    # tf_A_over_Bf_split(ang, load, split=1000, eps=eps)
    # tf_A_over_Bf_split(ang, load, split=300, eps=eps)
    # tf_A_over_Bf_split(ang, load, split=100, eps=eps)
    # tf_A_over_Bf_split(ang, load, split=30, eps=eps)
    plt.title("Position (Admittance) Magnitude")
    plt.suptitle(suptitle)
    plt.grid(True)

    figure_2 = plt.figure()
    tf_A_over_B_phase(ang, load, eps=eps)
    # tf_A_over_Bf_split_phase(ang, load, split=3000, eps=eps)
    # tf_A_over_Bf_split_phase(ang, load, split=1000, eps=eps)
    # tf_A_over_Bf_split_phase(ang, load, split=300, eps=eps)
    # tf_A_over_Bf_split_phase(ang, load, split=100, eps=eps)
    # tf_A_over_Bf_split_phase(ang, load, split=30, eps=eps)
    plt.title("Position (Admittance) Phase")
    plt.suptitle(suptitle)
    plt.grid(True)
    return figure_1, figure_2


def plot_spring_admittance(torque, load, suptitle=""):
    plt.figure()
    eps = (1e-1, 1e4)
    tf_A_over_B(torque, load, eps=eps)
    plt.title("Spring (Position Admittance) Magnitude")
    plt.suptitle(suptitle)

    plt.figure()

    tf_A_over_B_phase(torque, load, eps=eps)
    plt.title("Spring (Position Admittance) Phase")
    plt.suptitle(suptitle)

def comparison_of_test_quality(file1, file2, name1, name2):
    _, ang, _, torque, load, current, _ = get_data(file1)
    _, ang1, _, torque1, load1, current1, _ = get_data(file2)
    two_label_power_plot(torque, torque1,"Torque (Spring) Power Spectrum", name1, name2)
    two_label_power_plot(load, load1,"Torque (Load Cell) Power Spectrum", name1, name2)
    two_label_power_plot(ang, ang1,"Position Power Spectrum", name1, name2)
    two_label_power_plot(current, current1,"Current Power Spectrum", name1, name2)

def n_label_power_plot(title, list_of_data):
    plt.figure()
    for data, label in list_of_data:
        A, f = fft_plot(data)
        plt.loglog(f, np.abs(A)**2, label=label)
    plt.legend()
    plt.title(title)

def n_way_comparison_of_test_quality(list_in):
    files, names = zip(*list_in)
    _, angs, _, torques, loads, currents, _ = zip(*[get_data(file) for file in files])

    n_label_power_plot("Torque (Spring) Power Spectrum", zip(torques, names))
    n_label_power_plot("Torque (Load Cell) Power Spectrum", zip(loads, names))
    n_label_power_plot("Position Power Spectrum", zip(angs, names))
    n_label_power_plot("Current Power Spectrum", zip(currents, names))


def saturation_noise(file, gains, axmag, axphase):
    m_o = .29

    # work well on the no control tests
    m_m = .88/2
    m_s = 0.0
    b_s = 0.0

    k_s = 1180.
    b_m = 15.9+2 # pretty non linear
    deflection, ang, inf_ang, torque, load, current, motor = get_data(file)
    sat_error = saturation_error(file, gains, threshold=8)
    cross, power, f = get_cross_and_power(sat_error, load)
    w = np.pi*2*f
    s = complex(0, 1)*w
    lam = -np.log(.5)*5000
    lp_s = s/(s/lam+1)
    delay = np.exp(-s*0.0002)
    Zm_prime = m_m*s*s+b_m*s-mgain*(kd*lp_s+kp)
    Zsensitivity = mgain/Zm_prime
    transfer = cross / power * Zsensitivity

    scatter_bode_mag(f, transfer, power, ax=axmag)
    scatter_bode_phase(f, transfer, power, ax=axphase)

def filter_lp_diff(data, alpha=.5, kd=1e3): # default to the ones used on the chip
    # on the chip: .5, 5e3, 5000Hz
    # through the system we only get data at 1000Hz
    res = np.array(data)
    filter_x = data[0,1]
    for i in range(1, len(data)):
        y = data[i, 1]
        update = (1-alpha)*(y-filter_x)
        filter_x = alpha*filter_x+(1-alpha)*y
        res[i,1]= kd*update
    return res

def filter_lp(data, alpha=.5): # default to the ones used on the chip
    # on the chip: .5, 5e3, 5000Hz
    # through the system we only get data at 1000Hz
    res = np.array(data)
    filter_x = data[0,1]
    for i in range(1, len(data)):
        y = data[i, 1]
        update = (1-alpha)*(y-filter_x)
        filter_x = alpha*filter_x+(1-alpha)*y
        res[i,1]= filter_x
    return res

def combine_data(datalist, gainlist):
    res = np.array(datalist[0])
    res[:,1]*=0.
    for data, gain in zip(datalist, gainlist):
        res[:,1]+=gain*data[:,1]
    return res

def saturate_data(data, lim=1):
    ndata = np.array(data)
    for i in range(len(data)):
        ndata[i,1]= -lim if data[i,1]<-lim else lim if data[i,1]>lim else data[i,1]
    return ndata

def op_data(data, op):
    ndata = np.array(data)
    for i in range(len(data)):
        ndata[i,1]= op(data[i,1])
    return ndata

def time_section_data(data, start_time, end_time):
    low_ndx = find_ge(data[:,0], start_time)
    high_ndx = find_le(data[:,0], end_time)
    return np.array(data[low_ndx:high_ndx,:])

def average_data(data):
    return np.average(data, axis=0)

def find_le(a, x):
    'Find rightmost value less than or equal to x'
    i = bisect_right(a, x)
    if i:
        return a[i-1]
    raise ValueError

def find_ge(a, x):
    'Find leftmost item greater than or equal to x'
    i = bisect_left(a, x)
    if i != len(a):
        return a[i]
    raise ValueError

def saturation_error(test, gains, threshold=8):
    deflection1, ang1, inf_ang1, torque1, load1, current1, motor1 = get_data(test)
    motordot = filter_lp_diff(motor1)
    deflectiondot = filter_lp_diff(deflection1)
    current_expected = combine_data([motor1, deflection1, motordot, deflectiondot],gains)
    satcurrent = saturate_data(current_expected, lim=threshold)
    saturation_error = combine_data([satcurrent, current_expected],[1, -1])
    return saturation_error


def ez_saturation_plot(test, gains, threshold=8):
    name = test[test.find("["):]
    deflection1, ang1, inf_ang1, torque1, load1, current1, motor1 = get_data(test)
    motordot = filter_lp_diff(motor1)
    deflectiondot = filter_lp_diff(deflection1)
    current_expected = combine_data([motor1, deflection1, motordot, deflectiondot],gains)
    satcurrent = saturate_data(current_expected, lim=threshold)
    plt.figure()
    cross, power, f = get_cross_and_power(satcurrent, current_expected)
    transfer = cross / power
    scatter_bode_mag(f, transfer, power)
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Magnitude")
    plt.title("Saturation Describing Function")
    plt.grid(True)
    plt.suptitle("Gains: [%.1f,%.1f,%.1f,%.1f]"%tuple(gains))
    
def hann_phasor(signal, reference, window_size=1024, window_number=120, **kwargs):
    # weighting power:
    #   0.5 --- unweighted (standard averaging, corrected for phase of input)
    window = hann_window(window_size)
    total_length = len(signal)
    active_length = total_length-window_size
    print ("nominal window number", active_length//window_size)
    window_number*=active_length//window_size
    increment = active_length//(window_number-1)
    sample_rate = 1000.0
    f = np.fft.rfftfreq(window_size, d=1./sample_rate)
    phasor = complex(0,0)*np.zeros((len(f),))
    for n in range(window_number):
        A = np.fft.rfft(window*signal[n*increment:(n*increment+window_size),1])/(sample_rate)
        B = np.fft.rfft(window*reference[n*increment:(n*increment+window_size),1])/(sample_rate)
        phasor+=A*B.conjugate()/np.abs(B)
    return phasor, f
def hann_window(M):
    x = np.linspace(-.5, .5, M)
    y = .5*np.cos(2*np.pi*x)
    return y

# log_20181127T112047

def main():
    pass

if __name__ == '__main__':
    main()