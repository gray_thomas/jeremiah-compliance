import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from admittance_plotting_utils import *
from six_systems import *



def plot_hysteresis(ds):
    print(dir(ds))
    reflected_motor_mass= 300
    reflected_motor_damping= 6500
    reflected_motor_constant= 200
    # 'actuator', 'cmd', 'current', 'force', 
    # 'fx', 'fy', 'fz', 
    # 'infjac', 'infloadforce', 'joint', 'loadtorque', 'motor', 'motor_vel', 
    # 'time', 'torque', 'tx', 'ty', 'tz'
    # ['dob2_Q_pos', 'dob2_current_adjustment', 'dob2_desired_motor_pos', 'dob2_force_adjustment', 
    #  'dob2_motor_adjustment_pos', 'dob2_motor_position_error', 'dob2_u_prime']
    alpha=.95
    mot_pos = filter_lp(filter_lp(ds.actuator, alpha=alpha))
    mot_vel = filter_lp_diff(filter_lp(ds.actuator, alpha=alpha), alpha=alpha, kd=1e3)
    mot_acc = filter_lp_diff(filter_lp_diff(ds.actuator, alpha=alpha, kd=1e3), alpha=alpha, kd=1e3)

    dmot_pos = filter_lp(filter_lp(ds.other['dob2_desired_motor_pos'], alpha=alpha))
    dmot_vel = filter_lp_diff(filter_lp(ds.other['dob2_desired_motor_pos'], alpha=alpha), alpha=alpha, kd=1e3)
    dmot_acc = filter_lp_diff(filter_lp_diff(ds.other['dob2_desired_motor_pos'], alpha=alpha, kd=1e3), alpha=alpha, kd=1e3)

    flt_cmd = filter_lp(filter_lp(ds.cmd, alpha=alpha), alpha=alpha)
    flt_frc = filter_lp(filter_lp(ds.force, alpha=alpha), alpha=alpha)
    flt_cmd_prime = filter_lp(filter_lp(ds.other['dob2_u_prime'], alpha=alpha), alpha=alpha)

    print(ds.other.keys())
    # plt.plot(ds.joint[2000:,1],ds.cmd[2000:,1])
    # plt.plot(ds.joint[2000:,1],ds.cmd[2000:,1]-1/reflected_motor_constant*ds.force[2000:,1])
    # plt.plot(ds.joint[2000:,1],reflected_motor_mass*mot_acc[2000:,1]+reflected_motor_damping*mot_vel[2000:,1])
    # plt.plot(flt_cmd[2000:,1]*reflected_motor_constant-1*flt_frc[2000:,1],reflected_motor_mass*mot_acc[2000:,1]+reflected_motor_damping*mot_vel[2000:,1])
    # plt.plot(flt_cmd_prime[2000:,1],reflected_motor_mass*mot_acc[2000:,1]+reflected_motor_damping*mot_vel[2000:,1])
    # plt.plot(flt_cmd_prime[2000:,1],reflected_motor_mass*dmot_acc[2000:,1]+reflected_motor_damping*dmot_vel[2000:,1])
    
    # plt.axis('square')
    # plt.plot(ds.joint[2000:,1],-ds.current[2000:,1])
    plt.plot(ds.joint[2000:,1],200*ds.cmd[2000:,1]-ds.force[2000:,1], label="motor force less spring force")
    plt.plot(ds.joint[2000:,1],ds.other['dob2_u_prime'][2000:,1], label="u prime (dob)")
    plt.legend()
    # plt.plot(ds.joint[2000:,1],)
    # plt.plot(ds.joint[2000:,1],-1/100*ds.infloadforce[2000:,1])
    # plt.plot(ds.other["ref_motor_pos_m"][2000:,1],ds.other["dob2_desired_motor_pos"][2000:,1])
    # plt.plot(ds.actuator[2000:,1],ds.other["ref_motor_pos_m"][2000:,1])
    # plt.plot(ds.actuator[2000:,1],ds.actuator[2000:,1]-ds.other["ref_motor_pos_m"][2000:,1]) # tiny, possibly rounding error based difference.
    # plt.plot(ds.actuator[2000:,1],ds.force[2000:,1]/796958)
    # plt.plot(ds.actuator[2000:,1],-1/100*ds.other["dob2_u_prime"][2000:,1])
    # plt.plot(ds.joint[2000:,1],-1/100*ds.infloadforce[2000:,1])

def first_dob_performance_plot(ds):
    # originally tested for dataset 15
    plt.plot(ds.joint[2000:,1],200*ds.cmd[2000:,1]-ds.force[2000:,1], label="motor force less spring force")
    plt.plot(ds.joint[2000:,1],ds.other['dob2_u_prime'][2000:,1], label="u prime (dob)")
    plt.legend()
    plt.savefig("../figs/dob_perf_plot_1_ds%s.png"%ds.filename)


def main():
    ds1 = DataSet(17, csv=False, skip=1) # first hysteresis test. 1196 gram weight
    first_dob_performance_plot(ds1)
    plt.show()



    
if __name__ == '__main__':
    main()