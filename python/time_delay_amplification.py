import matplotlib.pyplot as plt
import numpy as np
from admittance_plotting_utils import fix_overphase, angle
# from uniform_style import *

freqs = np.logspace(0,3,10000)
s = complex(0, 1)*freqs*2*np.pi
j_m = 1
b_m = 0.01
k_2 = 2.5
dt = 0.004
base = lambda s: 1/(j_m*s*s+b_m*s)
delay = lambda s: np.exp(-dt*s)
lp = lambda s: 1/((s/(2*np.pi*50))**2+2*0.4*(s/(2*np.pi*50))+1)
virtual_series = lambda s: base(s) * k_2 *delay(s) * lp(s)
tf = lambda s: virtual_series(s) + base(s)

fig, (ax1, ax2) = plt.subplots(2,1,sharex=True,figsize=(3.5, 3))
ax1.loglog(freqs, abs(base(s)))
ax2.semilogx(*fix_overphase(freqs, 180/np.pi*np.angle(base(s))))
ax1.loglog(freqs, abs(virtual_series(s)))
ax2.semilogx(*fix_overphase(freqs, 180/np.pi*np.angle(virtual_series(s))))
ax1.loglog(freqs, abs(tf(s)))
ax2.semilogx(*fix_overphase(freqs, 180/np.pi*np.angle(tf(s))))
ax2.grid(True)
ax1.grid(True)
ax2.set_yticks([-180, -90, 0, 90, 180])
plt.show()
