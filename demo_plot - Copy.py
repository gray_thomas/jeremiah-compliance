""" Plots transfer functions representing time-delay proportional feedback."""
import numpy as np
import matplotlib.pyplot as plt

def combine_series(sys_in,series):
	compliance,input_compliance = sys_in
	compliance_out = lambda s: compliance(s) + series(s)
	
	
	return (compliance_out,input_compliance)
	
def combine_parallel(sys_in,parallel):
	compliance,input_compliance = sys_in
	compliance_out = lambda s: 1/(1/compliance(s) + 1/parallel(s))
	
	input_compliance_out = lambda s: (compliance_out(s)/compliance(s)
	                                 * input_compliance(s))
	
	
	return (compliance_out, input_compliance_out)
	
def virtual_parallel(sys_in, position_controller):
	compliance, input_compliance = sys_in
	parallel_out = lambda s: -1/position_controller(s) * compliance(s)/input_compliance(s)
	return parallel_out
	
def virtual_series(sys_in, force_controller):
	compliance, input_compliance = sys_in
	series_out = lambda s: input_compliance(s) * force_controller(s)
	return series_out
	
	

def main():
	## Parameters ##
	ma = 50
	me = 100
	b = 10
	k1 = 100
	k2 = 1000
	time_delay = .2
	#################
	
	frequency = np.logspace(-2,3,1000)
	omegas = np.pi*2*frequency
	laplace_s_list = complex(0, 1) * omegas
	
	
	
	damper = lambda s: 1/(b*s)
	mass1 = lambda s: 1/(ma*s*s)
	total1 = lambda s: 1/(1/damper(s)+ 1/mass1(s))
	
	spring1 = lambda s: 1/k1
	total2 = lambda s: total1(s) + spring1(s)
	
	mass2 = lambda s: 1/(me*s*s)
	total3 = lambda s: 1/(1/total2(s) + 1/mass2(s)) 
	
	spring2 = lambda s: 1/k2
	total4 = lambda s: total3(s) + spring2(s)	
	

	
	

	fig, (mag_ax, phase_ax) = plt.subplots(2,1, sharex=True)
	#mag_ax.loglog(frequency, abs(physical(laplace_s_list)))
	#mag_ax.loglog(frequency, abs(virtual(laplace_s_list)))
	mag_ax.loglog(frequency, abs(total4(laplace_s_list)))
	# mag_ax.loglog(frequency, [abs(transfer_function(s)) for s in laplace_s_list])
	#phase_ax.semilogx(frequency, 180/np.pi*np.angle(physical(laplace_s_list)))
	#phase_ax.semilogx(frequency, 180/np.pi*np.angle(virtual(laplace_s_list)))
	phase_ax.semilogx(frequency, 180/np.pi*np.angle(total4(laplace_s_list)))

	mag_ax.set_ylabel("magnitude")
	phase_ax.set_ylabel("phase (deg)")
	phase_ax.set_xlabel("frequncy (Hz)")

	fig.suptitle("Bode Plot without actuator force")


	plt.show()


if __name__ == '__main__':
	main()
