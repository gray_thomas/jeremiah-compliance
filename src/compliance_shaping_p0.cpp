#include <compliance_shaping_p0/compliance_shaping_p0.hpp>
#include <cmath>

#define MOTOR2LIN 1.2732e-4 // m/rad

static const double pi = 3.14159265358979323846;
static const double e =  2.71828182845904523536;

namespace compliance_shaping_p0
{
  double get_jacobian(double joint_position)
  {
    //parameters for calculating jacobian
    static const double h_dist = 0.1143;
    static const double v_dist = 0.012;
    static const double lever_arm = 0.025;
    static const double min_joint_position = 1.590767;
    static const double hyp = 0.114928;
    static const double phi = 0.104604;

    double gamma = pi/2 - (joint_position - min_joint_position)- phi;
    double act_length = pow(pow(lever_arm, 2) + pow(hyp,2) - 2*lever_arm*hyp*cos(gamma), 0.5);
    return -lever_arm *cos(( pi - gamma -  asin(lever_arm/act_length* sin(gamma)) ) - pi/2);
  }

  using namespace apptronik_system;

  // For Nodelets, use onInit() method instead of constructor
  ComplianceShapingController::ComplianceShapingController()
   :/*output_Q(
  //print_discrete_butterworth(alpha_hz=10.000000, time_step=0.001000)
  9.99959941e-01, 9.98714799e-04, 4.79382969e-07,
  -1.18911048e-01, 9.96174885e-01, 9.38473759e-04,
  -2.32788616e+02, -7.52880284e+00, 8.78242794e-01,
  4.01186249e-05, 1.19027439e-01, 2.32773515e+02),
   input_Q(
  //print_discrete_butterworth(alpha_hz=10.000000, time_step=0.001000)
  9.99959941e-01, 9.98714799e-04, 4.79382969e-07,
  -1.18911048e-01, 9.96174885e-01, 9.38473759e-04,
  -2.32788616e+02, -7.52880284e+00, 8.78242794e-01,
  4.01186249e-05, 1.19027439e-01, 2.32773515e+02),*/
  //  dob2_motor_position_error_Q(
  // //print_discrete_butterworth(alpha_hz=10.000000, time_step=0.001000)
  // 9.99959941e-01, 9.98714799e-04, 4.79382969e-07,
  // -1.18911048e-01, 9.96174885e-01, 9.38473759e-04,
  // -2.32788616e+02, -7.52880284e+00, 8.78242794e-01,
  // 4.01186249e-05, 1.19027439e-01, 2.32773515e+02),

//    compliance_shaping_Q(
// //print_discrete_butterworth(alpha_hz=50.000000, time_step=0.001000)
//   9.95594362e-01, 9.70814945e-04, 4.03246552e-07,
//   -1.25031742e+01, 9.15996683e-01, 7.17447664e-04,
//   -2.22453808e+04, -1.54121667e+02, 4.65211021e-01,
//   4.41189189e-03, 1.25142955e+01, 2.22370903e+04),
   spring_force_filter( std::pow(e, (-50 *2*pi*0.001)) , 0), //alpha = exp(-a * dt), x0 = 0)
   cuff_torque_filter() // new cuff filter object. Set up at initialization!
  // ,
  //   hp_force(.999995, 0.0)
  //   ,
  //   lp_current(.99995, 0.0)
  //   ,
  //   lp_force(.99995, 0.0)
    {}


  ComplianceShapingController::~ComplianceShapingController()
  {
    sys->stop();
    systethread->join();
    ros::Duration(0.1).sleep(); //make sure everyone's done using our pointers
  }

  // onInit() function should not block. It should initialize and then return.
  void ComplianceShapingController::onInit()
  {
    systethread.reset(new boost::thread(boost::bind(&ComplianceShapingController::systemThread, this)));
    cmd_motor_current = 0;
    // cmd_actuator_effort = 0;
    // current_adjustment = 0;
  }

  //set up ctrl interface here
  void ComplianceShapingController::systemThread()
  {
    std::string sn_elbow = std::string("Axon_proto_v1");
    ros::NodeHandle nh = getPrivateNodeHandle();
    print_slave_info(sn_elbow);

    NODELET_INFO_STREAM("pre-fault clearing===================\n");

    std::string server_name = "/" + sn_elbow + "/clear_faults/set";
    while(ros::ok() && !ros::service::exists(server_name, false))
    {
      ROS_WARN_THROTTLE(1.0, "Waiting for slave fault clearing server at %s", server_name.c_str());
    }
    apptronik_srvs::UInt16 clear_faults;
    clear_faults.request.set_data = 1;
    ros::service::call(server_name, clear_faults);


    NODELET_INFO_STREAM("post fault clearing==================\n");
    // emit setStatus(QString("Faults cleared!"));

    // User calls SystemLoop Constructor:
    sys.reset(new SystemLoop(boost::bind(&ComplianceShapingController::loop, this, _1, _2), nh, {sn_elbow}, true));

    NODELET_INFO_STREAM("post system constructor");

    // User sets run mode for each slave:
    // 6 is torque mode
    // 1 is current mode
    // according to the gui order.
    //  0-fault48, 1-fault48 , 2-fault48,  6-nofaults
    cmd_motor_current = 0;
    cmd_motor_current = 0;
    sys->setRunMode(static_cast<AxonMode>(1), sn_elbow);
    // sys->setRunMode(ACTUATOR_FORCE, sn_elbow);
    // sys->setRunMode(static_cast<AxonMode>(6), sn_elbow);

    // User registers a state ptr for each MISO topic with desired state info
    // sys->registerStatePtr( &fault_status , "diag__faults__x" );

    // time', 
    //         # '/Axon_proto_v1/miso/diag__faults__x', 
    //         # '/Axon_proto_v1/miso/actuator__force__N', 
    //         # '/Axon_proto_v1/miso/js__joint__effort__Nm', 
    //         # '/Axon_proto_v1/miso/cuff__ati__torque__Nm', 
    //         # '/Axon_proto_v1/miso/js__joint__position__rad', 
    //         # '/Axon_proto_v1/miso/motor__current__A', 
    //         # '/Axon_proto_v1/miso/actuator__position__m', 
    //         # '/Axon_proto_v1/miso/motor__position__Rad', 
    //         # '/Axon_proto_v1/miso/motor__velocity__Radps', 
    //         # '/Axon_proto_v1/mosi/cmd__joint__effort__Nm'


    sys->registerStatePtr( &fault_status , "diag__faults__x" ,sn_elbow);
    sys->registerStatePtr( &actuator_force , "actuator__force__N" ,sn_elbow);
    sys->registerStatePtr( &joint_torque , "js__joint__effort__Nm" ,sn_elbow);
    sys->registerStatePtr( &cuff_torque , "cuff__ati__torque__Nm" ,sn_elbow); //This is wrong - only uses Fz
    sys->registerStatePtr( &joint_position , "js__joint__position__rad" ,sn_elbow);
    sys->registerStatePtr( &motor_current , "motor__current__A",sn_elbow);
    sys->registerStatePtr( &actuator_position , "actuator__position__m",sn_elbow );
    sys->registerStatePtr( &motor_pos, "motor__position__Rad", sn_elbow);
    sys->registerStatePtr( &motor_vel, "motor__velocity__Radps", sn_elbow);
    // sys->registerStatePtr( &actuator__velocity__mps, "actuator__velocity__mps", sn_elbow);
    sys->registerStatePtr( &ati__Fx__N, "ati__Fx__N", sn_elbow);
    sys->registerStatePtr( &ati__Fy__N, "ati__Fy__N", sn_elbow);
    sys->registerStatePtr( &ati__Fz__N, "ati__Fz__N", sn_elbow);
    sys->registerStatePtr( &ati__Tx__Nm, "ati__Tx__Nm", sn_elbow);
    sys->registerStatePtr( &ati__Ty__Nm, "ati__Ty__Nm", sn_elbow);
    sys->registerStatePtr( &ati__Tz__Nm, "ati__Tz__Nm", sn_elbow);
    // sys->registerStatePtr( &diag__rtt_counter_mirror__x, "diag__rtt_counter_mirror__x", sn_elbow);
    // sys->registerStatePtr( &energetics__bus_voltage__V, "energetics__bus_voltage__V", sn_elbow);
    // sys->registerStatePtr( &js__joint__velocity__radps, "js__joint__velocity__radps", sn_elbow);
    // sys->registerStatePtr( &motor__case_temp__C, "motor__case_temp__C", sn_elbow);
    // sys->registerStatePtr( &motor__core_temp_est__C, "motor__core_temp_est__C", sn_elbow);
    // sys->registerStatePtr( &timestamp__ns, "timestamp__ns", sn_elbow);
    // sys->registerStatePtr( &timestamp__s, "timestamp__s", sn_elbow);



    // User registers a command ptr for each MOSI topic corresponding to the desired mode
    // sys->registerCommandPtr( &cmd_actuator_effort, "cmd__actuator__effort__n" ,sn_elbow);
    // sys->registerCommandPtr( &cmd_cuff_effort, "cmd__cuff__effort__Nm" ,sn_elbow);
    // sys->registerCommandPtr( &cmd_joint_torque, "cmd__joint__effort__Nm" ,sn_elbow);
    sys->registerCommandPtr( &cmd_motor_current, "cmd__motor__effort__a" ,sn_elbow);




    // User should initialize commands to a reasonable value
    // cmd_joint_torque = 0;
    // cmd_cuff_effort = 0;
    cmd_motor_current = 0;
    // cmd_actuator_effort = 0;
    prev_fault_status = 0;
    initial_motor_angle = -44.0; // doesn't work

    jacobian = -.02; // sane default

    ConcurrentServiceClient::addServerConnection<apptronik_srvs::UInt16>("/"+sn_elbow+"/clear_faults/set");

    //## Limit parameters ##
    nh.param("max_dob_current",max_dob_current, 0.);    
    nh.param("max_feedback_current",max_feedback_current, 2.);
    nh.param("ignore_cuff", ignore_cuff, false);
    //## Chirp Parameters ##
    nh.param("do_chirp",do_chirp, true);
    nh.param("chirp_low_freq_hz",chirp_low_freq_hz, .5);
    nh.param("chirp_high_freq_hz",chirp_high_freq_hz, 3.);
    nh.param("chirp_amplitude",chirp_amplitude, 0.);
    nh.param("chirp_offset",chirp_offset, 0.);
    nh.param("chirp_duration_m",chirp_duration_m, 1.);
    //## Model parameters ##
    nh.param("reflected_motor_mass",reflected_motor_mass, 231. );// # in linear space (kg)
    nh.param("reflected_motor_damping",reflected_motor_damping, 3465. );// # in linear space (Ns/m)
    nh.param("reflected_motor_constant",reflected_motor_constant, 186.176 );// # in units of N/A
    nh.param("spring_k",spring_k, 562100. );// # SEA spring stiffness in linear frame N/m
    nh.param("exo_inertia",exo_inertia, 0.2814 );// # (Kg m^2)
    //## Controller Behavior ##
    nh.param("desired_attenuated_inertia",desired_attenuated_inertia, 0.1407);// # \alpha^{-1} \hat M_m (Kg m^2)
    nh.param("desired_amplification_q_1",desired_amplification_q_1, 0.0160468);// # amplification derivative component q_1 (1/s)
    nh.param("desired_amplification_gain",desired_amplification_gain, 4.);// # amplification gain  (\alpha-1) (dimensionless)
    nh.param("desired_SEA_zeta",desired_SEA_zeta, 0.7);// # damping ratio of SEA's second order zero pair (dimensionless)
    nh.param("desired_tilde_K_1",desired_tilde_K_1, 0.0);// # virtual motor spring over mass (typically zero)
    nh.param("desired_tilde_B_1",desired_tilde_B_1, 9.424777961);// # virtual motor damping pole (rad s)
    //## Filter Parameters ##
    // OK, this is tedious, I know. But we can set these from the yaml now.
    double a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d;
    nh.param("torque_filter_lp_1_a_00", a_00, 0.0); 
    nh.param("torque_filter_lp_1_a_01", a_01, 0.0); 
    nh.param("torque_filter_lp_1_a_10", a_10, 0.0); 
    nh.param("torque_filter_lp_1_a_11", a_11, 0.0); 
    nh.param("torque_filter_lp_1_b_0", b_0, 0.0); 
    nh.param("torque_filter_lp_1_b_1", b_1, 0.0); 
    nh.param("torque_filter_lp_1_c_0", c_0, 0.0); 
    nh.param("torque_filter_lp_1_c_1", c_1, 0.0); 
    nh.param("torque_filter_lp_1_d", d, 0.0); 
    cuff_torque_filter.set_lp_1(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);
    nh.param("torque_filter_lp_2_a_00", a_00, 0.0); 
    nh.param("torque_filter_lp_2_a_01", a_01, 0.0); 
    nh.param("torque_filter_lp_2_a_10", a_10, 0.0); 
    nh.param("torque_filter_lp_2_a_11", a_11, 0.0); 
    nh.param("torque_filter_lp_2_b_0", b_0, 0.0); 
    nh.param("torque_filter_lp_2_b_1", b_1, 0.0); 
    nh.param("torque_filter_lp_2_c_0", c_0, 0.0); 
    nh.param("torque_filter_lp_2_c_1", c_1, 0.0); 
    nh.param("torque_filter_lp_2_d", d, 0.0); 
    cuff_torque_filter.set_lp_2(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);
    nh.param("torque_filter_bk_1_a_00", a_00, 0.0); 
    nh.param("torque_filter_bk_1_a_01", a_01, 0.0); 
    nh.param("torque_filter_bk_1_a_10", a_10, 0.0); 
    nh.param("torque_filter_bk_1_a_11", a_11, 0.0); 
    nh.param("torque_filter_bk_1_b_0", b_0, 0.0); 
    nh.param("torque_filter_bk_1_b_1", b_1, 0.0); 
    nh.param("torque_filter_bk_1_c_0", c_0, 0.0); 
    nh.param("torque_filter_bk_1_c_1", c_1, 0.0); 
    nh.param("torque_filter_bk_1_d", d, 0.0); 
    cuff_torque_filter.set_bk_1(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);
    nh.param("dob_Q_a_00", a_00, 0.0); 
    nh.param("dob_Q_a_01", a_01, 0.0); 
    nh.param("dob_Q_a_10", a_10, 0.0); 
    nh.param("dob_Q_a_11", a_11, 0.0); 
    nh.param("dob_Q_b_0", b_0, 0.0); 
    nh.param("dob_Q_b_1", b_1, 0.0); 
    nh.param("dob_Q_c_0", c_0, 0.0); 
    nh.param("dob_Q_c_1", c_1, 0.0); 
    nh.param("dob_Q_d", d, 0.0); 
    nh.param("dob_deadzone", dob_deadzone, 0.0); 
    nh.param("position_k", position_k, 0.0);
    dob.set_Q(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);

    // One time torque filter setup with the constant parameters
    cuff_torque_filter.setup(desired_amplification_gain, desired_amplification_q_1, exo_inertia, desired_attenuated_inertia*(desired_amplification_gain+1));
    dob.setup(reflected_motor_mass, reflected_motor_damping, reflected_motor_constant, max_dob_current, dob_deadzone);

    // Must call start to start loop
    global_time=0.0;
    clear_faults_latch=false;
    sys->start();
  }


  void ComplianceShapingController::change_mode(uint mode)
  {
    std::string sn_elbow = std::string("Axon_proto_v1");
    std::string srv_name = "/" + sn_elbow + "/mode/set";
    apptronik_srvs::UInt16 mode_srv = apptronik_srvs::UInt16();
    mode_srv.request.set_data = mode; // put it in current mode
    boost::shared_ptr<ServiceRequestWrapper<apptronik_srvs::UInt16> > rqst( new ServiceRequestWrapper<apptronik_srvs::UInt16>(srv_name, mode_srv) );
    ConcurrentServiceClient::pushBackSrvRequest(rqst, false); // allow blocking always false
  }

  void ComplianceShapingController::clear_faults()
  {
    std::string sn_elbow = std::string("Axon_proto_v1");
    std::string srv_name = "/" + sn_elbow + "/clear_faults/set";
    apptronik_srvs::UInt16 clear_faults;
    clear_faults.request.set_data = 1; // 1 for actually do it?
    boost::shared_ptr<ServiceRequestWrapper<apptronik_srvs::UInt16> > rqst( 
      new ServiceRequestWrapper<apptronik_srvs::UInt16>(srv_name, clear_faults) );
    ConcurrentServiceClient::pushBackSrvRequest(rqst, false); // allow blocking always false
  }

  //define control behavior here
  void ComplianceShapingController::loop(const double& time, const dBitset& fault_bitmap)
  {
    // Basic state information used in the DOBs
    double reflected_motor_pos_m = motor_pos*MOTOR2LIN;
    if (initial_motor_angle==-44.0) {
      // Sensor-based initialization goes here.
      initial_motor_angle = motor_pos;
    }

    

    jacobian = get_jacobian(joint_position);

    cmd_motor_current = 0;

    static double last_print_time = 0.0;
    static double last_run_time   = 0.0;

    //## Generate Chirp ##
    static double waveforstart_time = 0.0;
    if(do_chirp && time-waveforstart_time < 60 * chirp_duration_m) {
      exponentialChirpSignal( time-waveforstart_time, chirp);
    }
    else{
      if (time - last_print_time > 0.25){
        NODELET_INFO_STREAM("Experiment Finished!!!");}
      chirp=0.0;
    }

    //## generate noise (similar to chirp) ##
    Eigen::Matrix3d test_noise_mat = Eigen::Matrix3d::Random();
    test_noise_mat.setRandom();
    double test_noise = chirp_amplitude * test_noise_mat(0,0);

    double dt = time - last_run_time;
    global_time+=dt;
    if (!clear_faults_latch){
      if (global_time>0.15){
        clear_faults();
        clear_faults_latch=true;
      }
    }

    last_run_time = time;


    //## Construct Feedback Gains (As they depend on the Jacobian) ##
    double K_1, B_1, K_2, B_2, K_s, J_m, tilde_B_2, tilde_K_2, B_m, hat_J_m;
    double Jac2 = jacobian*jacobian;
    B_m = Jac2*reflected_motor_damping;
    J_m = Jac2*reflected_motor_mass;
    K_s = Jac2*spring_k;
    hat_J_m = desired_attenuated_inertia * (1+desired_amplification_gain);
    tilde_K_2 = K_s/hat_J_m;
    tilde_B_2 = desired_SEA_zeta*sqrt(tilde_K_2);
    K_1 = desired_tilde_K_1*J_m;
    B_1 = desired_tilde_B_1*J_m - B_m; // solid
    K_2 = (tilde_K_2*J_m-K_1-K_s)/K_s;
    B_2 = (tilde_B_2*J_m-B_1-B_m)/K_s; // too high by a factor of 4 (=alpha-1)

    cuff_torque_filter.update_jacobian_terms(K_s, J_m, tilde_B_2, tilde_K_2);

    //## Setup correct feedback signals ##
    double joint_angle_from_motor = (1.0/jacobian)*MOTOR2LIN*(motor_pos-initial_motor_angle);
    double joint_velocity_from_motor = (1.0/jacobian)*MOTOR2LIN*motor_vel;
    double spring_torque = -joint_torque;
    double spring_torque_veloctiy = spring_force_filter.next_hp(spring_torque)/0.001;
    //calculate the cuff torque based off of ATI readings
    calculated_cuff_torque = -ati__Fz__N * .19264 + (ati__Ty__Nm*0.608761 //cos(52.5)
         - ati__Tx__Nm*0.793353); //cos(37.5) //TODO: Zero these based on ICs.


    //## Compliance Shaping Controller ##
    double tau_m_from_motor = -K_1*joint_angle_from_motor - B_1*joint_velocity_from_motor;
    double tau_m_from_spring = K_2*spring_torque + B_2*spring_torque_veloctiy;
    double tau_m_from_cuff_torque = cuff_torque_filter.next(ignore_cuff ? 0.0 : -calculated_cuff_torque);
    // double tau_m_from_cuff_torque = cuff_torque_filter.next(test_noise);
    double tau_m = tau_m_from_motor + tau_m_from_spring + tau_m_from_cuff_torque;
    double motor_current_from_feedback =  (1.0/jacobian)*(1.0/reflected_motor_constant)*tau_m; 

    

    //## Apply (pre-DOB) Current Command and Saturate ##
    double saturated_feedback_current = motor_current_from_feedback;
    if (saturated_feedback_current>max_feedback_current){
      saturated_feedback_current=max_feedback_current;
    }
    if (saturated_feedback_current<-max_feedback_current){
      saturated_feedback_current=-max_feedback_current;
    }
    cmd_motor_current = saturated_feedback_current + chirp;              

    //Adding my own position controller here



    // cmd_motor_current = chirp - position_k * (1.5 - joint_position); // Jeremiah's position controller for chirp tests

    // saturate feedback control
    if (cmd_motor_current>max_feedback_current){
      cmd_motor_current=max_feedback_current;
    }
    if (cmd_motor_current<-max_feedback_current){
      cmd_motor_current=-max_feedback_current;
    }

    // //## DOB ##
    // double dob_u_prime = (reflected_motor_constant*(cmd_motor_current)-actuator_force);
    // double dob1_u = reflected_motor_constant*(cmd_motor_current+current_adjustment)-actuator_force;
    // double filtered_net_force_from_input = input_Q.next(dob1_u);
    // double filtered_motor_position = output_Q.next(reflected_motor_pos_m);
    // double filtered_motor_velocity = output_Q.vel();
    // double filtered_motor_acceleration = output_Q.acc();
    // double filtered_net_force_from_output = // Pn inverse
    //   reflected_motor_mass*filtered_motor_acceleration
    //   +reflected_motor_damping*filtered_motor_velocity;
    // double observed_force_disturbance = filtered_net_force_from_output-filtered_net_force_from_input;
    // double force_adjustment = -observed_force_disturbance;
    // double DOB_soft_start_scale_factor = time*1 > 1 ? 1 : 0;
    // current_adjustment = force_adjustment/reflected_motor_constant * DOB_soft_start_scale_factor;
    // // saturate dob adjustment
    // if (current_adjustment>max_dob_current){
    //   current_adjustment=max_dob_current;
    // }
    // if (current_adjustment<-max_dob_current){
    //   current_adjustment=-max_dob_current;
    // }
    // // Apply the DOB
    // cmd_motor_current += current_adjustment;
    double dob_u = (reflected_motor_constant*(cmd_motor_current)-actuator_force);
    cmd_motor_current += dob.next(reflected_motor_pos_m, dob_u, time);
    //
    //## Log Data ##
    LoggingManager::logData("dob_u", dob_u);
    // LoggingManager::logData("dob1_u", dob1_u);
    LoggingManager::logData("chirp", chirp);
    LoggingManager::logData("dob::current_adjustment", dob.current_adjustment);
    LoggingManager::logData("dob::current_pre_deadzone", dob.current_pre_deadzone);
    LoggingManager::logData("reflected_motor_pos_m", reflected_motor_pos_m);
    LoggingManager::logData("calculated_cuff_torque", calculated_cuff_torque);
    LoggingManager::logData("jacobian", jacobian);
    LoggingManager::logData("tau_m_from_motor", tau_m_from_motor);
    LoggingManager::logData("tau_m_from_spring", tau_m_from_spring);
    LoggingManager::logData("tau_m_from_cuff_torque", tau_m_from_cuff_torque);
    LoggingManager::logData("motor_current_from_feedback", motor_current_from_feedback);
    LoggingManager::logData("cmd_motor_current", cmd_motor_current);
    LoggingManager::logData("saturated_feedback_current", saturated_feedback_current);
    LoggingManager::logData("K_1",K_1);
    LoggingManager::logData("B_1",B_1);
    LoggingManager::logData("K_2",K_2);
    LoggingManager::logData("B_2",B_2);
    LoggingManager::logData("K_s",K_s);
    LoggingManager::logData("J_m",J_m);
    LoggingManager::logData("tilde_B_2",tilde_B_2);
    LoggingManager::logData("tilde_K_2",tilde_K_2);
    LoggingManager::logData("B_m",B_m);
    LoggingManager::logData("joint_angle_from_motor", joint_angle_from_motor);
    LoggingManager::logData("joint_velocity_from_motor", joint_velocity_from_motor);
    LoggingManager::logData("spring_torque", spring_torque);
    LoggingManager::logData("spring_torque_velocity", spring_torque_veloctiy );
    LoggingManager::logData("cuff_torque_filter::tau_1", cuff_torque_filter.tau_1 );
    LoggingManager::logData("cuff_torque_filter::tau_2", cuff_torque_filter.tau_2 );
    LoggingManager::logData("cuff_torque_filter::bk_1.ul", cuff_torque_filter.bk_1.ul );
    LoggingManager::logData("cuff_torque_filter::bk_1.x0", cuff_torque_filter.bk_1.x0 );
    LoggingManager::logData("cuff_torque_filter::bk_1.x1", cuff_torque_filter.bk_1.x1 );
    LoggingManager::logData("cuff_torque_filter::bk_1._acc", cuff_torque_filter.bk_1._acc );
    LoggingManager::logData("cuff_torque_filter::lp_2.ul", cuff_torque_filter.lp_2.ul );
    LoggingManager::logData("cuff_torque_filter::lp_2.x0", cuff_torque_filter.lp_2.x0 );
    LoggingManager::logData("cuff_torque_filter::lp_2.x1", cuff_torque_filter.lp_2.x1 );
    LoggingManager::logData("cuff_torque_filter::lp_2._acc", cuff_torque_filter.lp_2._acc );
    LoggingManager::logData("cuff_torque_filter::tau_3", cuff_torque_filter.tau_3 );
    LoggingManager::logData("test_noise", test_noise );

    //## Screen Printing @ 4 Hz ##
    if (time - last_print_time > 0.25)
    {
      // NODELET_INFO_STREAM("Torque, force, ratio is... "  << cuff_torque << ", " << actuator_force<< ", " << cuff_torque/actuator_force << ")" );
      NODELET_INFO_STREAM("fault: " << std::setw(2) << fault_status <<
        " time step " << std::fixed << std::setw(7) << std::setprecision(5) << dt <<
        " global time " << std::fixed << std::setw(7) << std::setprecision(2) << global_time <<
        ", force (sign corrected)" << std::fixed << std::setw(7) << std::setprecision(2) << -actuator_force <<
        // ", filtered_net_force_from_output " << std::fixed << std::setw(7) << std::setprecision(2) << filtered_net_force_from_output <<
        // ", filtered_net_force_from_input " << std::fixed << std::setw(7) << std::setprecision(2) << filtered_net_force_from_input <<
        // ", current_adjustment " << std::fixed << std::setw(7) << std::setprecision(2) << current_adjustment <<
        ", current " << std::fixed << std::setw(7) << std::setprecision(2) << cmd_motor_current
        );
      last_print_time = time;
    }
    //## Fault Printing @ 4 Hz ##
    if(fault_bitmap.any())
    {
      cmd_motor_current=0.0;
      if (time - last_print_time > 0.25)
      {
        NODELET_INFO_STREAM("System Faulted... (Fault code: " << fault_status << ")" );
        NODELET_INFO_STREAM("fault: " << fault_status <<"chirp"<<chirp<< ")" );
        last_print_time = time;
      }
    }
    prev_fault_status = fault_status;
  }

  void ComplianceShapingController::exponentialChirpSignal(const double& elapsed_time, double& signal)
  {
    static double prev_sample_time = 0;
    // static double prev_effective_angle = 0;

    double effective_angle;
    // double range = 2*3.14159*(chirp_high_freq_hz - chirp_low_freq_hz);
    double period = elapsed_time - prev_sample_time;

    double k = pow(chirp_high_freq_hz / chirp_low_freq_hz, 1.0 / (60.0 * chirp_duration_m));

    prev_sample_time = elapsed_time;

    if(period > 0.0)
    {
      //exponential chirp
      if (abs(chirp_high_freq_hz-chirp_low_freq_hz)<1e-7)
        {effective_angle = chirp_low_freq_hz*2*3.14159 *elapsed_time;}
      else
        {effective_angle = chirp_low_freq_hz*2*3.14159 * (pow(k, elapsed_time) - 1) / log(k);}
      signal = chirp_amplitude * sin(effective_angle) + chirp_offset;
    }
  }


  void ComplianceShapingController::print_slave_info(std::string& slave_name)
  {
    apptronik_srvs::GetSlaveInfo slave_info;
    std::string slave_info_server_name = "/" + slave_name + "/interface_info";
    while(ros::ok() && !ros::service::exists(slave_info_server_name, false))
    {
      ROS_WARN_THROTTLE(1.0, "Waiting for slave interface info at %s", slave_info_server_name.c_str());
    }
    ros::service::call(slave_info_server_name, slave_info); // populates the slave_info

    for(auto info : slave_info.response.miso)
    {
      // addMISOInterface(info);
      NODELET_INFO_STREAM("miso: "+ info.name);

    }

    for(auto info : slave_info.response.mosi)
    {
      // addMOSIInterface(info);
      NODELET_INFO_STREAM("mosi: "+  info.name);
    }

    for(auto info : slave_info.response.params)
    {
      // addParamInterface(info);
      NODELET_INFO_STREAM("param: "+ info.name);
    }
  }
}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(compliance_shaping_p0::ComplianceShapingController, nodelet::Nodelet)