#ifndef compliance_shaping_p0_HPP
#define compliance_shaping_p0_HPP

#include <apptronik_system/system.hpp>
#include <Eigen/Dense>

/**  A clean and empty example Nodelet Controller for you to copy and customize. */
namespace compliance_shaping_p0
{
  using namespace apptronik_system;


  class FirstOrderPole{
  public:
    FirstOrderPole(double alpha, double x0):x(x0),alpha(alpha){}
    double x;
    double alpha;
    inline double peek_hp(double u){
        return (1-alpha)*(u-x);
    }
    inline double next_hp(double u){
        double ret =  peek_hp(u);
        next(u);
        return ret;
    }
    inline double next(double u){
        x = alpha * x + (1-alpha)*u;
        return x;
    }
  };

  class ThirdOrderQ{
    Eigen::Vector3d x;
    Eigen::Matrix3d A;
    Eigen::Vector3d B;

  public:
    ThirdOrderQ(double a00, double a01, double a02,
        double a10, double a11, double a12,
        double a20, double a21, double a22,
        double b0, double b1, double b2)
    {
        x = Eigen::Vector3d();
        A = Eigen::Matrix3d();
        B = Eigen::Vector3d();
        A << a00, a01, a02, a10, a11, a12, a20, a21, a22;
        B << b0, b1, b2;
    }
    inline double next(double u){
        x = A*x + B*u;
        return x[0];
    }
    inline double vel(){
        return x[1];
    }
    inline double acc(){
        return x[2];
    }
  };

class SecondOrderQ{
    Eigen::Matrix2d A;
    Eigen::Vector2d B;
    Eigen::Vector2d C;
    double d;
  public:
    Eigen::Vector2d x;
    double _acc;
    double ul;
    double x0;
    double x1;
    SecondOrderQ(double a00, double a01,
        double a10, double a11,
        double b0, double b1,
        double c0, double c1,
        double d)
    {
        x = Eigen::Vector2d::Zero();
        A = Eigen::Matrix2d();
        B = Eigen::Vector2d();
        C = Eigen::Vector2d();
        this->d = d; // previously d=d; this line took hours to debug
        A << a00, a01, a10, a11;
        B << b0, b1;
        C << c0, c1;
        _acc = 0.0;
    }
    inline double next(double u){
        this->ul=u;
        this->x0 = x(0);
        this->x1 = x(1);
        _acc = C.dot(x) + d*u;
        x = A*x + B*u;
        return x[0];
    }
    inline double vel(){
        return x[1];
    }
    inline double acc(){
        return _acc;
    }
  };

class CuffTorqueFilter
/*
    Gray --- This is a new class that implements a fancy cuff torque filter.
    It should be similar to the filter Jeremiah wrote, but it has a slightly 
    different compensator based on my notes. In latex notation we have
    \tau_m^\prime = \frac{M_m}{\hat M_m}\left[1+\frac{J_e}{K_s}\frac{s^2}{Lp_2(s)}
    \frac{s^2 + \tilde B_2 s + \tilde K_2}{s^2 + \tilde B_1 s + \tilde K_1}\right]
    \frac{1+s q_1}{Lp_1(s)} (\alpha-1) \tau_c

    where 
    \tau_m^\prime  is the additional motor current
    \tau_c  is the cuff torque (ATI torque)
    M_m  is the motor inertia reflected in joint space
    \hat M_m  is the desired virtual motor inertia (achieved by the tuning of the other gains)
    \alpha  is the amplification rate
    \tilde B_2  is (B_m + B_1 + K_s B_2)/M_m  (the sum of the closed loop SEA zeros)
    \tilde K_2  is (K_s + K_1 + K_s K_2)/M_m  (the product of the closed loop SEA zeros)
    J_e  is the exoskeleton inertia
    K_s  is the SEA spring constant projected into joint space (changes with jacobian)
*/
  {
public:
    SecondOrderQ lp_1; // (replaces 3rd order)
    SecondOrderQ lp_2; // (replaces 3rd order)
    SecondOrderQ lp_3; // (replaces 3rd order)
    SecondOrderQ bk_1; // 1/(s^2 + s B_tilde_1 + K_tilde_1) // use as first order
    // FirstOrderPole fo_1; // 1/(s+B_tilde_1)
protected:
    double alpha_minus_1; // amplification gain
    double q_1; // amplification zero location
    double J_e; // exo joint inertia (Kg m^2)
    double K_s; // SEA spring stiffness (Nm/rad)
    double J_m; // SEA motor inertia (reflected in joint space) (Kg m^2)
    double J_hat_m; // virtual SEA desired motor inertia (Kg m^2);
    double tilde_B_2; // SEA zero damping (1/s)
    double tilde_K_2; // SEA zero stiffness (1)
public:
    double tau_1, tau_2, tau_3;
    inline CuffTorqueFilter():
        lp_1(//print_discrete_2(omega_hz=75.000000, damping=0.150000, time_step=0.001000)
        8.95947709e-01, 8.98409282e-04,
        -1.99506244e+02, 7.68938089e-01,
        1.04052291e-01, 1.99506244e+02,
        -2.22066099e+05, -1.41371669e+02,
        2.22066099e+05),
        lp_2(//print_discrete_2(omega_hz=75.000000, damping=0.150000, time_step=0.001000)
        8.95947709e-01, 8.98409282e-04,
        -1.99506244e+02, 7.68938089e-01,
        1.04052291e-01, 1.99506244e+02,
        -2.22066099e+05, -1.41371669e+02,
        2.22066099e+05),
        lp_3(//print_discrete_2(omega_hz=75.000000, damping=0.150000, time_step=0.001000)
        8.95947709e-01, 8.98409282e-04,
        -1.99506244e+02, 7.68938089e-01,
        1.04052291e-01, 1.99506244e+02,
        -2.22066099e+05, -1.41371669e+02,
        2.22066099e+05),
        bk_1(   //print_discrete_BK(B=5.000000, K=0.000000, time_step=0.001000)
        1.00000000e+00, 9.97504161e-04,
        0.00000000e+00, 9.95012479e-01,
        4.99666459e-07, 9.97501668e-04,
        0.00000000e+00, -5.00000000e+00,
        1.00000000e+00),
        alpha_minus_1(0.0),
        q_1(0.0),
        J_e(0.0),
        K_s(0.0),
        J_m(0.0),
        J_hat_m(0.0),
        tilde_B_2(0.0),
        tilde_K_2(0.0)
    {}
    inline void setup(double alpha_minus_1, double q_1, double J_e, double J_hat_m)
    {
        this->alpha_minus_1=alpha_minus_1;
        this->q_1 = q_1;
        this->J_e = J_e;
        this->J_hat_m = J_hat_m;
    }
    inline void set_lp_1(double a_00, double a_01,
        double a_10, double a_11,
        double b_0, double b_1,
        double c_0, double c_1,
        double d)
    {
        this->lp_1 = SecondOrderQ(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);
    }
    inline void set_lp_2(double a_00, double a_01,
        double a_10, double a_11,
        double b_0, double b_1,
        double c_0, double c_1,
        double d)
    {
        this->lp_2 = SecondOrderQ(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);
        this->lp_3 = SecondOrderQ(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);
    }
    inline void set_bk_1(double a_00, double a_01,
        double a_10, double a_11,
        double b_0, double b_1,
        double c_0, double c_1,
        double d)
    {
        this->bk_1 = SecondOrderQ(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);
    }
    inline void update_jacobian_terms(double K_s, double J_m, double tilde_B_2, double tilde_K_2)
    {
        this->K_s = K_s;
        this->J_m = J_m;
        this->tilde_B_2 = tilde_B_2;
        this->tilde_K_2 = tilde_K_2;
    }
    /*inline double next(double tau_cuff)
    { // This version has numerical trouble
        tau_1 = lp_1.next(tau_cuff); 
        // TODO: consider not low passing direct component.
        tau_1 += q_1*lp_1.vel(); // amplification zero
        tau_1 *= alpha_minus_1; // multiply by kp
        tau_2 = tilde_K_2*bk_1.next(tau_1); // Complex chain...
        tau_2 += tilde_B_2*bk_1.vel(); // ...here goes...
        tau_2 += bk_1.acc(); // ...the second order zero.
        tau_3 = lp_2.next(tau_2); // just needed to get acceleration
        tau_3 = lp_2.acc(); // here is the acceleration.
        tau_3 *= J_e/K_s ; // this part is adjusted by the jacobian;
        double tau_m_prime = tau_1+tau_3; // the chains converge
        tau_m_prime *= J_m/J_hat_m; // final adjustment
        return tau_m_prime;
    }*/
    inline double next(double tau_cuff)
    {
        tau_1 = lp_1.next(tau_cuff); 
        // TODO: consider not low passing direct component.
        tau_1 += q_1*lp_1.vel(); // amplification zero
        tau_1 *= alpha_minus_1; // multiply by kp
        // branch
        lp_2.next(tau_1);
        tau_2 = lp_2.vel();
        tau_3 = tilde_K_2*lp_3.next(tau_2);
        tau_3 += tilde_B_2*lp_3.vel();
        tau_3 += lp_3.acc();
        bk_1.next(tau_3);
        double tau_4 = bk_1.x1;
        bk_1.x(0)=0.0; // unused state. Keep it from growing without bound.
        tau_4 *= J_e/K_s;
        double tau_m_prime = tau_1+tau_4; // the chains converge
        tau_m_prime *= J_m/J_hat_m; // final adjustment
        return tau_m_prime;
    }

  };

  class FrictionDOB
/*
    Gray --- This is a new class that implements a fancy disturbance observer.
    It should be similar to the earlier one, but with more choices for the filter, 
    and the ability to load it from the YAML coefficients.
*/
  {
public:
    SecondOrderQ input_Q; // (replaces 3rd order)
    SecondOrderQ output_Q; // (replaces 3rd order)
protected:
    double m_m; // motor_mass
    double b_m; // transmission_damping
    double g_m; // motor gain
    double limit; // max_dob_current
    double deadzone; // amps
public:
    double current_adjustment;
    double tau_1, tau_2, tau_3;
    inline FrictionDOB():
        input_Q(//print_discrete_2(omega_hz=75.000000, damping=0.150000, time_step=0.001000)
        8.95947709e-01, 8.98409282e-04,
        -1.99506244e+02, 7.68938089e-01,
        1.04052291e-01, 1.99506244e+02,
        -2.22066099e+05, -1.41371669e+02,
        2.22066099e+05),
        output_Q(//print_discrete_2(omega_hz=75.000000, damping=0.150000, time_step=0.001000)
        8.95947709e-01, 8.98409282e-04,
        -1.99506244e+02, 7.68938089e-01,
        1.04052291e-01, 1.99506244e+02,
        -2.22066099e+05, -1.41371669e+02,
        2.22066099e+05),
        m_m(0.0),
        b_m(0.0),
        g_m(1.0),
        current_adjustment(0.0),
        limit(0.0),
        deadzone(0.0)
    {}
    inline void setup(double m_m, double b_m, double g_m, double limit, double deadzone)
    {
        this->m_m = m_m;
        this->b_m = b_m;
        this->g_m = g_m;
        this->limit = limit;
        this->deadzone = deadzone;
        this->current_adjustment = 0.0;
    }
    inline void set_Q(double a_00, double a_01,
        double a_10, double a_11,
        double b_0, double b_1,
        double c_0, double c_1,
        double d)
    {
        this->input_Q = SecondOrderQ(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);
        this->output_Q = SecondOrderQ(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d);
    }
    double current_pre_deadzone;
    inline double next(double y, double u, double time)
    {
        // // expected call in setup:
        // dob.set_Q(a_00, a_01, a_10, a_11, b_0, b_1, c_0, c_1, d)
        // dob.setup(reflected_motor_mass, reflected_motor_damping, reflected_motor_constant, max_dob_current)
        
        // // expected call in loop:
        // double dob_u = (reflected_motor_constant*(cmd_motor_current)-actuator_force);
        // cmd_motor_current += dob.next(reflected_motor_pos_m, dob_u, time);

        double input = u+g_m*this->current_adjustment;
        double u_in = input_Q.next(input);
        output_Q.next(y);
        double u_out = m_m*output_Q.acc() + b_m*output_Q.vel();
        double u_err = u_out-u_in;
        double force_adjustment = -u_err;
        double scale = time*1 > 1 ? 1 : 0;
        this->current_pre_deadzone = force_adjustment/g_m * scale;
        if (current_pre_deadzone<deadzone and current_pre_deadzone>-deadzone) {current_adjustment=0;}
        else if (current_pre_deadzone>deadzone){current_adjustment=current_pre_deadzone-deadzone;}
        else {current_adjustment=current_pre_deadzone+deadzone;}
        // saturate dob adjustment
        if (current_adjustment>limit){
          current_adjustment=limit;
        }
        if (current_adjustment<-limit){
          current_adjustment=-limit;
        }
        return current_adjustment;
    }

  };


  // Usecase
  // Init stage
  // FirstOrderPole hp_current(.995, 0.0);
  // FirstOrderPole hp_force(.995, 0.0);
  // FirstOrderPole lp_current(.95, 0.0);
  // FirstOrderPole lp_force(.95, 0.0);

  // loop stage
  // double filt_current = lp_current.next(hp_current.next_hp(current))
  // double filt_force = lp_force.next(hp_force.next_hp(force))
  // print << filt_force/filt_current

  /**  An empty Nodelet controller that does nothing. */
  class ComplianceShapingController: public nodelet::Nodelet
  {
  public:
    boost::shared_ptr<SystemLoop> sys;
    boost::shared_ptr<boost::thread> systethread;

    //## Inputs (states) ##
    unsigned int fault_status;
    unsigned int prev_fault_status;
    double actuator_force;
    double joint_torque;
    double cuff_torque;
    double actuator_position;
    double motor_current;
    double joint_position;
    double motor_pos;
    double motor_vel;

    // integrator states (do we need?)
    double x_1; // integrating error
    double x_2; // integrating error
    double x_3; // integrating error
    double x_4; // integrating error

    //## Force Sensor Inputs ##
    double ati__Fx__N;
    double ati__Fy__N;
    double ati__Fz__N;
    double ati__Tx__Nm;
    double ati__Ty__Nm;
    double ati__Tz__Nm;

    //## Command outputs ##
    // double cmd_joint_torque; // needed?
    // double cmd_cuff_effort; // needed?
    double cmd_motor_current; // REAL
    // double cmd_actuator_effort; // needed?
    // double cmd; // needed?
    double chirp; // needed?

    //## Waveform Params ##
    bool do_chirp;
    double chirp_low_freq_hz;
    double chirp_high_freq_hz;
    double chirp_amplitude;
    double chirp_duration_m;
    double chirp_offset;

    //## Limit parameters ##
    double max_dob_current;//:0
    double max_feedback_current;//:2
    double dob_deadzone;
    bool ignore_cuff;//:False

    //## Model Parameters ##
    double reflected_motor_mass;//: 231 # in linear space (kg)
    double reflected_motor_damping;//: 3465 # in linear space (Ns/m)
    double reflected_motor_constant;//: 186.176 # in units of N/A
    double spring_k;//: 562100. # SEA spring stiffness in linear frame N/m
    double exo_inertia;//: 0.2814 # (Kg m^2)

    //## Controller Behavior ##
    double desired_attenuated_inertia;//: 0.1407 # \alpha^{-1} \hat M_m (Kg m^2)
    double desired_amplification_q_1;//: 0.0160468 # amplification derivative component q_1 (1/s)
    double desired_amplification_gain;//: 4 # amplification gain  (\alpha-1) (dimensionless)
    double desired_SEA_zeta;//: 0.7 # damping ratio of SEA's second order zero pair (dimensionless)
    double desired_tilde_K_1;//: 0.0 # virtual motor spring over mass (typically zero)
    double desired_tilde_B_1;//: 9.424777961 # virtual motor damping pole (rad s)

    //## Kinematics Parameters ##
    double jacobian;
    double initial_motor_angle;

    //## DOB state ##
    // double current_adjustment;

    double global_time;
    bool clear_faults_latch;

    //compliance shaping controller
    // double Q_filtered_cuff_torque;
    // double Q_filtered_cuff_torque_dot;
    // double Q_filtered_cuff_torque_ddot;
    // double filtered_cuff_torque;

    double calculated_cuff_torque;
    double tau_m_prime;

    double position_k;

    // ThirdOrderQ compliance_shaping_Q;
    // FirstOrderPole cuff_torque_filter;


    //## Filter Objects ##
    FirstOrderPole spring_force_filter;
    // ThirdOrderQ input_Q;
    // ThirdOrderQ output_Q;
    CuffTorqueFilter cuff_torque_filter;
    FrictionDOB dob;

    // Alternate format DOB
    // double dob2_u_prime;
    // double dob2_motor_position_error;
    // double dob2_adjusted_position_error;
    // ThirdOrderQ dob2_motor_position_error_Q;
    // double dob2_force_adjustment;
    // double dob2_desired_motor_pos;
    // double dob2_desired_motor_vel;
    // double dob2_motor_adjustment_pos;
    // double dob2_motor_adjustment_vel;

    ComplianceShapingController();
    ~ComplianceShapingController();
    void onInit();
    void systemThread();
    void loop(const double& time, const dBitset& fault_bitmap );
    void exponentialChirpSignal(const double& elapsed_time, double& signal);
    void print_slave_info(std::string& slave_name);
    void clear_faults();
    void change_mode(uint mode);
  };
}

#endif // compliance_shaping_p0_HPP
