\section{Modeling}\label{sec:model}
The model shown in Fig.~\ref{fig:schematics} is a series-elastic-actuator-driven exoskeleton with a series elastic cuff. While the series elastic joint (interface $\tau_j$--$\theta_j$) is a fourth order system, the actuator itself (interface $\tau_s$--$\theta_j$ ) is only second order. Motor dynamics are approximated with inertia and linear damping. No model of imperfect backdrivability is used (backdrivability will be enforced later using the disturbance observer). While the system is part prismatic and part rotational, we use a linear schematic for the whole system for simplicity. Later we employ a gain scheduling technique to keep the controller behavior similar even as the transmission ratio changes.


\subsection{Open Loop Linear Model}
%% introduce symbols glossary
\begin{table}\vspace{.1in}
	\caption{Symbol Glossary}\label{tab:notation}
	\centering\resizebox{\columnwidth}{!}{%
	\begin{tabular}{rl}
		\toprule
		Symbol & Meaning\\
		\midrule
		$\theta_m(s)$ & reflected motor deflection angle (signal)\\
		$\tau_m(s)$ & reflected motor input torque (signal)\\
		$J_m$, $B_m$ & reflected motor inertia and damping (constant)\\
		$\theta_s(s)$, $\tau_s(s)$ & spring deflection angle and torque (signal)\\
		$K_s$, $K_c$ & SEA spring and exo cuff stiffnesses (constant)\\
		$\theta_j(s)$, $\tau_j(s)$ & exoskeleton joint angle, external torque (signal)\\
		$J_j$ & exoskeleton joint inertia (constant)\\
		$\theta_c(s)$, $\tau_c(s)$ & cuff spring deflection and torque (signal)\\
		$K_1,\ B_1,\ K_2,\ B_2$ & SEA controller parameters (constant)\\
		$\tilde K_1,\ \tilde B_1,\ \tilde K_2,\ \tilde B_2$ & SEA compliance shape parameters (constant)\\
		$\hat K_1,\ \hat B_1,\ \hat K_2,\ \hat B_2$ & meta-SEA (cuff) controller parameters (constant)\\
		$\zeta,\  \hat \zeta$ & zero-pair damping ratios for SEA and meta-SEA\\
		$\hat B_m,\ \hat J_m$ & virtual motor damping and inertia (constant)\\
		$\alpha$ & amplification ratio (constant)\\
		$G_\theta(s)$& motor position feedback (transfer function)\\
		$G_s(s)$& spring torque feedback (transfer function)\\
		$G_c(s)$& cuff torque feedback (transfer function)\\
		$G_v(s)$& virtual motor input filter (transfer function)\\
		$\hat C_5(s)$ & virtual motor system compliance (trans. function)\\
		$\hat \tau_m$ & virtual motor torque (signal)\\
%		$S_i=(C_i(s),\ H_i(s))$& the i$^\mathrm{th}$ system (pair of transfer functions)\\
		\bottomrule
	\end{tabular}}
%	\rule{\columnwidth}{3pt}
\end{table}


Fig.~\ref{fig:schematics} and Tab.~\ref{tab:interconnections} describe the actuator as a sequence of series and parallel interconnections of elements with different dynamic compliances. There are seven systems here, including the feedback controllers for each of the three sensed signals: motor position $\theta_m$, spring torque $\tau_s$, and cuff torque $\tau_c$ (all reflected into the joint space using the transmission Jacobian). These seven systems are built up as follows: 1) the passive motor compliance, 2) the closed loop motor system with position feedback, or system 1's parallel interconnection with motor position feedback, 3) 2's series interconnection with spring torque feedback, 4) 3's series interconnection with the SEA spring, 5) 4's parallel interconnection with the exoskeleton, 6) 5's series interconnection with cuff torque feedback, and 7) 6's series interconnection with the cuff spring.

At each level of interconnection, we keep track of not only the system compliance, but also the effect of the motor current on the output position. We define a ``system'' to be a pair of an ``external'' compliance (position per external torque) and a ``motor'' compliance (position per motor torque), both of which are transfer functions. At each level of interconnection the meaning of position, external force, and motor torque change slightly as more components are grouped together, as shown in Tab.~\ref{tab:interconnections}.

\begin{table*}\vspace{.1in}
	\caption{Seven System Interconnections Model of a Series Elastic Exoskeleton With a Compliant Cuff}\label{tab:interconnections}\centering\resizebox{\textwidth}{!}{%
	\begin{tabular}{clccc}
		\toprule
		System (Compliance Pair) & Definition & Position & External Torque & Motor Torque\\
		\midrule
		$S_1 = (C_1(s),\ H_1(s))$ & $S_1=(\frac{1}{J_m s^2+ B_m s},\ \frac{e^{-s T}}{J_m s^2+ B_m s})$ & $\theta_m$ & $\tau_s$ & $\tau_m$\\
		$S_2 = (C_2(s),\ H_2(s))$ & $S_2 = \Parallel(S_1, [-1/G_\theta(s)C_1(s)H_1(s)])$  & $\theta_m$ & $\tau_s$ & $\tau_2 = \tau_m - G_\theta(s) \theta_m$\\
		$S_3 = (C_3(s),\ H_2(s))$ & $S_3= \Series\left(S_2 , [G_s(s)H_2(s)]\right)$ & $\theta_m$ & $\tau_s$ & $\tau_3 = \tau_2 - G_s(s) \tau_s$\\
		$S_4 = (C_4(s),\ H_2(s))$ & $S_4= \Series\left(S_3 , [1/K_s]\right)$ & $\theta_j=\theta_m+K_s^{-1}\tau_s$ & $\tau_s$ & $\tau_3$\\
		$S_5 = (C_5(s),\ H_5(s))$ & $S_5= \Parallel\left(S_4 , [1/(J_j s^2)]\right)$ & $\theta_j$ & $\tau_j=\tau_s + J_j s^2 \theta_j$ & $\tau_3$\\
		$\hat S_5 = (\hat C_5(s),\ \hat C_5(s))$ & $\hat S_5= \left(1/(\hat J_m s^2+\hat B_m s) ,\ 1/(\hat J_m s^2+\hat B_m s) \right)$ & $\theta_j$ & $\tau_j$ & $\hat \tau_m = G_v^{-1}(s) \tau_3$\\
		$S_6 = (C_6(s),\ H_5(s))$ & $S_6= \Series\left(S_5 , [G_c(s)H_5(s)]\right)$ & $\theta_j$ & $\tau_c=\tau_j$ & $\tau_6=\tau_3 - G_c(s) \tau_c$\\
		$S_7 = (C_7(s),\ H_5(s))$ & $S_7= \Series\left(S_6 , [1/K_c]\right)$ & $\theta_h=\theta_j+K_c^{-1}\tau_c$ & $\tau_c$ & $\tau_6$\\
		\bottomrule
		\end{tabular}}
\end{table*}

We then use the following two propositions to define the behavior of series and parallel interconnections.
\begin{proposition}[Systems in Parallel]
	The parallel interconnection of the system $S_1=\left(C_1(s),\ H_1(s)\right)$ and the compliance $C_2(s)$, denoted $S=\Parallel(S_1,\ C_2(s))$, is
	\begin{align}
	S=\big(&[C_1^{-1}(s) +C_2^{-1}(s) ]^{-1},\nonumber\\ &[C_1^{-1}(s) +C_2^{-1}(s)]^{-1} C_1^{-1}(s) H_1(s)\big)\end{align}
\end{proposition}

\begin{proposition}[Systems in Series]
	The series interconnection of the system $\left(C_1(s),\ H_1(s)\right)$ and the compliance $C_2(s)$, denoted $S=\Series(S_1,\ C_2(s))$, is
	\begin{align}
	S=\big([C_1(s)+C_2(s)],\  H_1(s)\big)
	\end{align}
\end{proposition}

It is also possible to represent feedback control as a virtual parallel or series system, using the following two propositions.

\begin{proposition}[Virtual Parallel Systems]
	The system $\left(C_1(s),\ H_1(s)\right)$ under the position controller $G(s)$ is equivalent to the parallel interconnection of that system and the virtual parallel system
	\begin{align}
	C^\prime(s) = -\frac{C_1(s)}{H_1(s)G(s)}
	\end{align}
\end{proposition}

\begin{proposition}[Virtual Series Systems]
	The system $\left(C_1(s),\ H_1(s)\right)$ under the force controller $G(s)$ is equivalent to the series interconnection of that system and the virtual series system
	\begin{align}
	C^\prime(s) = G(s) H_1(s)
	\end{align}
\end{proposition}

\begin{figure}[t]%
	\centering\resizebox{\columnwidth}{!}{%
	{\footnotesize%
		\input{sea_diagrams.pdf_tex}%
	}}
	\caption{Double-SEA model of a compliant exoskeleton. Here the inner SEA can be tuned via compliance shaping in order to have the desired dominant second order pole locations when combined with the joint inertia. Together, these are then approximated as second order for the purpose of designing the controller of the second (meta-) SEA. A virtual motor torque input signal is adjusted by an appropriately chosen filter. Compliance shaping can therefore be applied to tuning the behavior of the meta-SEA, just like any other SEA. }\label{fig:schematics}
\end{figure}

These propositions can be used to model both the compliance and output compliance of an arbitrarily complex assemblage of passive elements and sensor feedbacks. Tab.~\ref{tab:interconnections} demonstrates their application to the series elastic exoskeleton with the compliant cuff. Note that the time delay $e^{-sT}$ in the first system's input compliance is neglected in finding the nominal model. This time delay will later be critical for finding the performance limits.


The angles, torques, and dynamics in Tab.~\ref{tab:notation} (the symbol glossary) are all mechanically reflected through the applicable gear ratios into the reference frame of the joint, such that the frequency domain signals\footnote{This paper's notation neglects the explicit functional dependence on the Laplace variable~$s$ for the lower-case greek letter variables, which represent signals, except where they are introduced in Tab.~\ref{tab:notation}.} are related as follows:
\begin{gather}
\theta_j = \theta_m + \theta_s, \qquad \tau_j = J_j s^2 \theta _j + \tau_s, \nonumber\\
\tau_s = K_s \theta_s, \qquad (J_m s^2 + B_m s) \theta_m = \tau_s + \tau_m,\nonumber\\
\theta_h = \theta_j + \theta_c, \qquad \tau_j=\tau_c=K_c\theta_c.\label{eq:model}
\end{gather}
The six equations above are a complete description of this simple linear model.

\subsection{Compliance Shaping for SEAs}\label{sec:compliance_shaping}
%% introduce full state feedback controller parameterization. Wall of math
Compliance shaping for SEAs is a control strategy that dictates the gains of a controller based on the desired closed loop SEA compliance. This builds on our earlier work which introduced compliance shaping and applied it to the design of high-stiffness position control of SEAs \cite{ThomasMehlingHolleySentisXXXX}. The controller (which is also closely related to the full-state feedback controller of \cite{AlbuschafferHirzinger2000IROS}) is
\begin{equation}
	\tau_m = -(K_1 + B_1 s) \theta_m + (K_2 + B_2 s) \theta_s,
\end{equation}
with controller gain variables $K_1$, $B_1$, $K_2$, and $B_2$. 

Using the parallel and series interconnections of Tab.~\ref{tab:interconnections}, with $G_\theta(s)=-(K_1 + B_1 s)$ and $G_s(s) =  (K_2 + B_2 s)$ yields the SEA compliance 
\begin{align}
\frac{\theta_j}{\tau_s} &= \frac{J_m s^2 + (B_m+B_1+B_2) s +(K_s+K_1+K_2)}{(J_m s^2 + (B_m+B_1) s +K_1)K_s},\label{eq:long_actuator_compliance}
\end{align}
which we can rewrite in terms of a ratio of second order polynomials 
\begin{equation}
\frac{\theta_j}{\tau_s}= C_4(s) = \frac{s^2 + \tilde B_2 s +\tilde K_2}{(s^2 + \tilde B_1 s +\tilde K_1)K_s},\label{eq:motspring_compliance}
\end{equation}
in order to extract the gains of the controller as a function of the shape of the compliance:
%\begin{gather}
%\tilde K_1 = K_1/J_m, \quad \tilde K_2 = (K_s+K_1+K_2)/J_m, \nonumber\\
%\tilde B_1 = (B_m + B_1)/J_m, \quad
%\tilde B_2 =( B_m + B_1 + B_2)/J_m.\label{eq:hyper_params}
%\end{gather}
\begin{gather}
K_1=J_m \tilde K_1, \quad K_2 =J_m (\tilde K_2-\tilde K_1)-K_s, \nonumber\\
B_1=J_m \tilde B_1-B_m, \quad
B_2 =J_m (\tilde B_2-\tilde B_1).\label{eq:extracted}
\end{gather}
Thus we consider this controller to shape the compliance of the closed loop SEA.

The chosen compliance shape then determines $C_5(s)$,
\begin{equation}
\frac{\theta_j}{\tau_j}=\frac{s^2 + \tilde B_2 s + \tilde K_2}{J_j s^2(s^2 + \tilde B_2 s + \tilde K_2)+ K_s (s^2 + \tilde B_1 s + \tilde K_1)},\label{eq:total_compliance}
\end{equation}
which is often a primary consideration when choosing the two poles and two zeros of $C_4(s)$.

\subsection{Virtual Motor System}
As shown in Fig.~\ref{fig:schematics}, we treat $S_5$ as a virtual motor for the meta-SEA. However, $S_5$ is fourth order rather than second order, and has a compliance with respect to motor torque, $H_5(s)$, which is different from its compliance with respect to external torques, $C_5(s)$. Therefore, some manipulation is needed to force it to behave like a motor system.

First, we introduce the system $\hat S(s)$ which we intend to make $S_5(s)$ resemble. This desired virtual motor system has the same compliance with respect to external torques and motor torques: $1/(\hat J_m s^2 + \hat B_m s)$.

For our purpose it is sufficient for $C_5(s)$ to only approximately match $\hat C_5(s)$ at low frequencies. We apply the following approximation of \eqref{eq:total_compliance},
\begin{equation}
\hat C_5=\frac{1}{K_s/\tilde K_2 \cdot (s^2 + \tilde B_1 s + \tilde K_1)},
\end{equation}
which is a reasonable estimate for angular frequencies satisfying $\tilde K_2 \gg \|s^2+\tilde B_2 s\|$ and for $\hat J_m$ such that $\hat J_m\gg J_e$. This relationship then determines parameters of the SEA compliance $C_4$: $\tilde K_2 =K_s/\hat J_m $, $\tilde K_1=0$, and $\tilde B_1 = \tilde K_2/K_s \cdot \hat B_m$. 

Compared to accurately matching $C_5(s)$ to $\hat C_5(s)$, compensating for the behavior of $H_5(s)$ is very important. This is accomplished with a special purpose filter $G_v(s)$,
\begin{equation}
G_v(s) = J_m/\hat J_m \left[1 + \frac{J_j}{K_s}\frac{s^2(s^2 +\tilde B_2 s + \tilde K_2)}{s^2 + \tilde B_1 s + \tilde K_1}\right],
\end{equation}
which we use to define a new virtual motor torque variable $\hat \tau_m$, s.t. $G_v \hat \tau_m = \tau_3$. And with this compensator, we can reasonably expect the transfer function $\theta_j/\hat{\tau_m}$ to be $1/(\hat J_m s^2 \hat B_m s)$. However, in order to implement this non-causal $G_v(s)$ filter, we will need to introduce low pass filters. These, in conjunction with the feedback delay, contribute phase lag to this transfer function in practice.



\subsection{Compliance Ratios and the Amplification Rate}
In our linear model, the joint angle of an exoskeleton which is in contact with both an amplified operator and an environment is the superposition of the two joint signals from the operator and environment. In the special case where the output motion is locked,
\begin{gather}
\theta_j = 0 = C_5 \tau_{\mathrm{environmnet}}+C_6 \tau_{\mathrm{human}},\\
\tau_{\mathrm{environment}} = -C_6/C_5 \tau_{\mathrm{human}}.
\end{gather}
That is to say, the ratio $C_6(s)/C_5(s)$ is the amplification of the human. If both $C_5$ and $C_6$ are minimum phase and stable, then this amplification will also be minimum phase and stable. Note that the cuff spring is irrelevant to this ratio.





