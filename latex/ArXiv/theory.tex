\section{Transmission Disturbance Observer}

The motor dynamics have been, up to this point in the paper, modeled as a linear system. However, in truth there are highly nonlinear friction effects in the transmission which would prevent easy back-driving of the device under normal circumstances. Let us consider a model of the motor dynamics alone which treats this deviation from linearity as a torque disturbance $\delta_f$:
\begin{equation}
\tau_s+\tau_m +\delta_f = J_m \ddot \theta_m + B_m \dot \theta_m.\label{eq:dob_eq}
\end{equation}
We call this the autonomous motor model, since it has no relationship to the unknown environment. Allowing the controller to treat the transmission as an autonomous system is a lesser known advantage of SEAs.

A recent advancement in SEA control is the disturbance observer (DOB), which estimates and then compensates for some unsensed (disturbance) input to the plant. Different cascade control structures result in widely varying DOB behaviors, as these cascade structures have different SISO plants in their design models. DOB structures applied to SISO models of motor position \cite{KongBaeTomizuka2009TMech} cancel the combination of friction and back-driving forces that disturb the otherwise current-driven motor model. When applied to rejecting disturbances in a closed-loop force-control plant, the DOB must cancel out the transmission friction and output acceleration inputs which act alongside the force set-point to influence the spring force \cite{PaineOhSentis2014TMech}. It is also possible to directly reject disturbances in the open loop model \cite{HopkinsEA2015IROS,LuHaningerChenTomizuka2015AIM}, which has shown to be an equivalent approach \cite{RoozingWalzahnCaldwellTsagarakis2016IROS}. These force disturbance observers improve impedance rendering accuracy when used in a cascaded impedance controller \cite{MehlingHolleyOMalley2015IROS}. 

As they relate to understanding the limits of SEA performance, DOBs have two distinct effects. When a DOB cancels transmission friction, it works against the largest source of nonlinearity in the system and therefore makes linear stability analysis more accurate. On the other hand, when the DOB attempts to cancel an important aspect of the system behavior it can make the system more difficult to stabilize. For instance, canceling the relationship between output acceleration and spring force is an implicit acceleration feedback, which can jeopardize the passivity of the output (and require a minimum output inertia for stability). We therefore propose a transmission DOB which only removes errors in the autonomous motor model. This should make our system behave more linearly, while leaving any drastic changes to the system behavior to the linear feedback controller designed via compliance shaping.




\begin{figure}
	\centering\resizebox{\columnwidth}{!}{{\footnotesize%
		\input{BlockDiagramControl.pdf_tex}}}
	\caption{Block Diagram showing a) the cuff torque compensator, b) the SEA compliance shaping controller, c) the disturbance observer (along with the system it acts on), and d) our model of the physical system. .}\label{fig:BlockDiagram}
\end{figure}
The block diagram in Fig.~\ref{fig:BlockDiagram} shows our whole controller and the physical system (d). At the top level, (a) shows the force feedback control for the virtual SEA, with the filter $G_v(s)$ that compensates for the difference between motor torques and forces on the exoskeleton. A SEA compliance shaping controller (b) uses motor and spring torque feedback to make $C_5(s)$ resemble $\hat C_5(s)$. The disturbance observer (DOB) is the lowest level of control and only seeks to make the linear assumptions more accurate by rejecting the transmission force disturbance $\delta_f$. 


\begin{figure}
	\centering{\footnotesize\def\svgwidth{3in}
	\input{DOB_Tuning.pdf_tex}}
	\caption{Tuning the disturbance observer's Q filter to avoid destabilization by time delay.}\label{fig:DOBtune}
\end{figure}
The Bode plot in Fig.~\ref{fig:DOBtune} shows three transfer functions which explain the tuning limit of the DOB Q filter. First, looking closely at Fig.~\ref{fig:BlockDiagram}.c, we can see that the DOB has a net feedback gain of $Q(s)/(1-Q(s))C_1^{-1}(s)$ between output $\theta_m$ and input $\tau_m$. If we assume the model of the inverse plant is accurate, then this is essentially an open loop gain of $Q(s)/(1-Q(s))$ from $\tau_m$ back to $\tau_m$. The first line in Fig.~\ref{fig:DOBtune} is this open loop gain multiplied by the system's control time delay (essentially, an un-modeled aspect of the plant). When this open loop is closed, we can see how the time delay distorts the poles of the low pass filter Q---the second line in Fig.~\ref{fig:DOBtune}. The third line is the original Q filter, for comparison. When a Q filter is chosen with an excessive bandwidth or insufficient damping, this second line will reveal unstable pole behavior in the Bode plot.





\section{Double Compliance Shaping Control Design}\label{sec:theory}

\begin{figure*}
	\centering{{\footnotesize\def\svgwidth{6.0in}
		\input{PassivityTimeDelay.pdf_tex}}}\caption{For the simple system in (c), amplification control is shown for two different time delay values. The bandwidth limitation (which inevitably exists) is modeled as a second order low pass filter at 50 Hz. In (a) the behavior is minimum phase and stable. In (b), the larger time delay  leads to a non-minimum phase zero and an unstable impedance. The fundamental difficulty of amplification is that even if instability is avoided, there is no way to avoid a phase which drops below the passive phase region.}\label{fig:amplification}
\end{figure*}

Fig.~\ref{fig:amplification} demonstrates a fundamental stability issue of strength amplification exoskeletons: in order to increase compliance, a sort of ``phase-debt'' must be paid eventually. Unlike the introduction of non-minimum phase zeros, this problem cannot be avoided. When a human operator acts as a spring of sufficient stiffness, this non-passive phase can lead to instability. For this reason, we've added a mechanical spring in series with the cuff. While this will not resolve the issue of passivity, it helps ensure coupled stability by switching the problem of destabilization by stiff springs to the problem of destabilization by light inertias.

\begin{figure}
	\centering{\footnotesize\resizebox{\columnwidth}{!}{%
	\input{DesignFigure.pdf_tex}}}
	\caption{Bode plot of the amplification controller design.}\label{fig:design}
\end{figure}

The process we use to design the controller is visualized in Fig.~\ref{fig:design} and Fig.~\ref{fig:flowchart}. Our approach first designs a nominal compliance for $C_7$ (the compliance of the exoskeleton's cuff angle with respect to human torques) and then for $C_5$ (the compliance of the exoskeleton angle with respect to external torques from the environment). These nominal compliances are only approximations. In the case of $C_7$ this is due to the many low pass filters which must be added to the cuff torque signal in order to actually implement the compensator $G_v$ (which is not causal), as well as the time delay. For $C_5$, it is because the nominal motor model is only second order, while the actual system is fourth order and does not have any ability to alter its high-frequency inertia behavior. However, these nominal transfer functions are still very useful since they allow us to parameterize the controller in terms of nominal poles and zeros, asymptotic behaviors, or damping ratios for pole and zero pairs.

\begin{figure}
	\centering{\footnotesize\resizebox{\columnwidth}{!}{%
		\input{DesignFlowchart.pdf_tex}}}
	\caption{Flow chart of the controller design process.}\label{fig:flowchart}
\end{figure}

The first part of our design process (the top of Fig.~\ref{fig:flowchart}) is to set the filter frequencies and damping ratios for the low pass filters required for implementing the cuff torque compensation filter $G_v$. This is a straightforward trade-off between noise amplification and phase lag. The filters will be used to sanity-check the shape of $C_7$, as shown in Fig.~\ref{fig:flowchart}. However, the discrete-time implementation of these filters may slightly differ from the behavior of the continuous time filters, and this will require iterative design. Next, the behavior of the nominal cuff compliance $C_7$ is specified using the attenuated virtual motor inertia $\hat J_m/\alpha$, damping $\hat B_m/\alpha$, and the damping ratio $\hat \zeta$ of the nominal second order zero occurring at the intersection with the cuff spring compliance line. These parameters are chosen before the amplification rate and only describe the nominal $C_7$ behavior. 

At this point, we should be able to test the expected $C_7$ (including the low pass filters). Unfortunately, our implementation of the $G_v(s)$ only filters the part of the expression which needs a second order derivative, and this results in an actual filtering behavior which is slightly different (with a slightly better phase) than our prediction step, which assumes that the whole cuff feedback signal is filtered by these low-pass filters.

After choosing the amplification ratio $\alpha$, we can describe the nominal $C_7(s)$,
\begin{equation}
C_7(s)=\theta_h/\tau_c \approxeq \frac{s^2 + 2 \hat \zeta \sqrt {\alpha K_c/\hat J_m}+\alpha K_c/\hat J_m }{K_c (s^2 + \hat B_m/\hat J_m s)},\label{eq:nominal_C7}
\end{equation}
which entails gains 
\begin{gather}
\hat K_2 = \alpha-1,\quad \hat B_2 = (2 \hat \zeta \sqrt{K_c \hat J_m \alpha } - \hat B_m)/K_c,
\end{gather}
following the same logic as Sec.~\ref{sec:compliance_shaping}. At this step, we have designed the controller for the meta-SEA based on its compliance shape \eqref{eq:nominal_C7}. The above gains represent the spring force feedback controller for the meta-SEA. All that remains is to design the SEA compliance $C_4(s)$ such that the joint compliance $C_5(s)$ resembles the desired virtual motor compliance $\hat C_5(s)$ at low frequencies.

There is only one free parameter left---the damping ratio of the second order zero pair $\zeta$. The $C_4(s)$ which yields the appropriate low frequency behavior is
\begin{equation}
C_4(s) = \theta_j/\tau_s = \frac{s^2+2\zeta \sqrt{K_s/\hat J_m}s + K_s/\hat J_m}{K_s(s^2 + \hat B_m/\hat J_m s)}.
\end{equation}
And again, compliance shaping converts this desired compliance into actionable gains
\begin{gather}
K_1 = 0, \quad B_1 = \hat B_m J_m/\hat J_m - B_m,\\
K_2 =  J_m/\hat J_m - 1,\\ B_2 = (2\zeta\sqrt{K_s/\hat J_m} -\hat B_m /\hat J_m)J_m/K_s.
\end{gather}
From here, the filter $G_v$ can be calculated, and there are no more free parameters anywhere in the control.

What this control law is doing, in essence, is first setting the $C_7(s)$ compliance as high as possible without introducing a non-minimum phase zero. Then, in order to accomplish the desired amplification $\alpha$ it reduces the compliance of $C_5(s)$ by using negative spring torque feedback to amplify inertia of the motor, which dominates $C_5(s)$ at low frequencies. This has the unintended side effect of amplifying transmission friction in the output. However, this effect is mitigated by the transmission disturbance observer.


%
%
%This strategy of tuning SEA behavior to achieve a higher inertia by using negative spring feedback to amplify the reflected inertia of the motor is one of the contributions of this paper. It works much better if the spring stiffness is higher.

As the joint angle changes, so does the transmission Jacobian, and with it the reflected motor inertia, motor damping, and spring stiffness. Fortunately, our algebraic method is extremely simple computationally, and we can re-generate the appropriate gains according to the new values of these parameters at every time step. This simple gain scheduling approach comes with no guarantee of stability, but, in practice, is effective. Even as the Jacobian changes, the compliance $C_7(s)$ that the human feels should stay the same. However, the compliance that the environment feels, $C_5(s)$, will change noticeably. When the reflected spring compliance, $1/K_s$, increases, the frequency at which $C_5(s)$ deviates from its nominal model drops. When this happens, the bandwidth of the amplification ratio also decreases.

\subsection{Passivity of the Operator Compliance}
While the nominal operator compliance can be made passive, the phase lag in the cuff feedback signal make it impractical to actually achieve this. As pointed out in \cite{HeHuangThomasSentis2019ArXiv}, the human provides a very convenient hysteretic damping, and therefore using the passivity criteria to judge coupled stability with a human operator is very conservative. We instead claim that the addition of a spring, along with the SEA compliance shaping technique which ensures that the zeros introduced by this spring are sufficiently damped, serves to avoid oscillations due to high-stiffness operators interacting with high-frequency amplification. The strategy simply substitutes a susceptibility to spring-like humans for one to inertia-like humans.
















