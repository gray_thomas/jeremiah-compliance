%% Creator: Inkscape inkscape 0.91, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'BodeIllustration.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{252bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,1.14285714)%
    \put(0,0){\includegraphics[width=\unitlength,page=1]{BodeIllustration.pdf}}%
    \put(0.12972396,1.04055893){\color[rgb]{0,0,0}\makebox(0,0)[rb]{\smash{$10^{-2}$}}}%
    \put(0.12972396,0.99192311){\color[rgb]{0,0,0}\makebox(0,0)[rb]{\smash{$10^{-4}$}}}%
    \put(0.12972396,0.94328724){\color[rgb]{0,0,0}\makebox(0,0)[rb]{\smash{$10^{-6}$}}}%
    \put(0.12841304,0.87331528){\color[rgb]{0,0,0}\makebox(0,0)[rb]{\smash{$0^\circ$}}}%
    \put(0.12841304,0.78831971){\color[rgb]{0,0,0}\makebox(0,0)[rb]{\smash{$-90^\circ$}}}%
    \put(0.12841304,0.70332409){\color[rgb]{0,0,0}\makebox(0,0)[rb]{\smash{$-180^\circ$}}}%
    \put(0.05241083,1.10916631){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{a)}}}%
    \put(0.56054116,0.67282722){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{Frequency}}}%
    \put(0.17812553,0.66330973){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$10^0$}}}%
    \put(0.43502226,0.66330973){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$10^1$}}}%
    \put(0.69191896,0.66330973){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$10^2$}}}%
    \put(0.9488159,0.66330973){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$10^3$}}}%
    \put(0.02677486,1.01116408){\color[rgb]{0,0,0}\rotatebox{90}{\makebox(0,0)[b]{\smash{Magnitude}}}}%
    \put(0.02588985,0.78752371){\color[rgb]{0,0,0}\rotatebox{90}{\makebox(0,0)[b]{\smash{Phase}}}}%
    \put(0.17558458,0.98893857){\color[rgb]{0.83921569,0.15294118,0.15686275}\makebox(0,0)[lb]{\smash{Spring line}}}%
    \put(0.51842388,0.97044539){\color[rgb]{0.12156863,0.46666667,0.70588235}\rotatebox{-10.87127866}{\makebox(0,0)[lb]{\smash{Joint Inertia Line}}}}%
    \put(0.74015309,0.97799604){\color[rgb]{0.17254902,0.62745098,0.17254902}\rotatebox{-11.0556591}{\makebox(0,0)[lb]{\smash{Motor Inertia}}}}%
    \put(0.45087332,1.06724376){\color[rgb]{0.17254902,0.62745098,0.17254902}\rotatebox{-5.77042214}{\makebox(0,0)[lb]{\smash{Motor Damping}}}}%
    \put(0.85485411,1.02951847){\color[rgb]{1,0.50588235,0.06666667}\makebox(0,0)[b]{\smash{Actuator}}}%
    \put(0.33634317,0.84180824){\color[rgb]{0.2,0.24705882,0.28235294}\makebox(0,0)[b]{\smash{Passive Phase Region}}}%
    \put(0.5176756,0.26475382){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{e)}}}%
    \put(0.596716,0.62741991){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{closed loop motor compliance}}}%
    \put(0.08136423,0.62890994){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{closed loop actuator compliance}}}%
    \put(0.11092102,0.33833751){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$K_1$}}}%
    \put(0.81510242,0.46936754){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$B_1$}}}%
    \put(0.76587673,0.40291279){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$B_1$}}}%
    \put(0.0094812,0.57380241){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{b)}}}%
    \put(0.51187423,0.57264213){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{c)}}}%
    \put(0.09529457,0.57083051){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$K_1$}}}%
    \put(0.20168295,0.15936909){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$K_2$}}}%
    \put(0.31736348,0.08471008){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$K_2$}}}%
    \put(0.88439233,0.1495498){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$B_2$}}}%
    \put(0.93458224,0.08953601){\color[rgb]{0,0,0}\makebox(0,0)[b]{\smash{$B_2$}}}%
    \put(0.00484023,0.25895245){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{d)}}}%
    \put(0.39809805,0.03452077){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{amplified motor compliance}}}%
  \end{picture}%
\endgroup%
