%% Non-writing:
%	Remaking figures in software
%	Looking up references
%	Formatting
%	Drawing new figures in Inkscape
%% Feels like writing?
%	Looking up, reading the references, and taking notes
%% Definitely writing:
%	Writing new paragraphs
%% Probably writing:
%	Editing writing
%	Writing rambling comments which eventually become section outlines
%	Anything which requires assuming the author persona
%	Deciding what to call things

\section{Introduction} \label{sec:intro}
%% Introductions should be about remembering literature facts, not BSing.


%%% Exoskeltons -> Payload, Strength, Endurance -> Strength Amplification -> Similarity to force feedback human interaction -> This paper

%% Exoskeletons
%\IEEEPARstart{E}{xoskeletons}
Exoskeletons are a broad category of wearable collaborative robots that have been successful in a wide range of applications. Some aim to recover locomotion capability lost to disease \cite{KwaNoordenMisselCraigPrattNeuhaus2009ICRA,HaribHereidAgrawalGurrietFinetBoerisDuburocqMungaiMasselinAmesSreenathGrizzle2018CSM} or offload the strenuous work of rehabilitation therapy from therapists \cite{SugarHeEA2007TNSRE,KimDeshpande2017IJRR}. Others aim to minimally detract from the performance of healthy operators while augmenting their motion with simple pre-programmed boosts to improve efficiency in walking or other predictable tasks \cite{LeeKimBakerLongKaravasMenardGalianaWalshJNR2018,ZhangFiersWitteJacksonPoggenseeAtkesonCollins2017Science}. However, different control techniques are necessary to allow exoskeletons to increase the payload, strength, and endurance of operators (healthy or otherwise) as they attempt non-repetitive, unpredictable tasks. We call such systems ``amplification exoskeletons'', since their goal is the amplification of their human operators.

% Cite binghan's IROS paper
%% Payload, Strength, Endurance
We can subdivide amplification into three types: payload amplification seeks to reduce the burden of a modeled payload on the operator; strength amplification seeks to amplify the human with respect to unmodeled loads; and endurance amplification seeks to help the operator move their own body. These three areas of amplification are all independently valuable, and most exoskeleton systems in the literature have focused on one to the exclusion of others. The BLEEX exoskeleton focuses on payload amplification, using gravity compensation and positive acceleration feedback to help the operator move a carefully modeled load \cite{KazerooniRacineHuangSteger2005ICRA,Kazerooni2005IROS}. Endurance amplification has been addressed by extending inertia reduction \cite{KongTomizuka2009TMech} and gravity compensation \cite{KongMoonJeonTomizuka2010TMech} to (reduce) the inertia and gravity of operators themselves. This paper, however, focuses on the control difficulties of strength amplification (with regard to unknown loads), in which the environmental forces can still be perceived by the operator.

%% Test Exoskeleton
\begin{figure}%
	\centering%
	%	\includegraphics[width=1in,clip]{figs/LoadedTestbench.jpg}
\resizebox{\columnwidth}{!}{%
	\input{Testbench.pdf_tex}}
	\caption{The experimental compliant cuff exoskeleton, assembled at the Human Centered Robotics Lab in the University of Texas at Austin, comprising: (a) a motor suspension spring and (b) a belt-drive to ball screw transmission driven by an electric motor (these two comprising an Appronik Systems Orion Series Elastic Actuator (SEA)); (c) a mount for adding weights to the exoskeleton structure, (d) a flexure spring made of two aluminum plates, (e) a six axis ATI Mini40 force/torque sensor, (f) a cuff (padding not shown) to be attached to the human forearm, and (g) an inclined arm rest to position the elbow near the kinematic rotation axis of the exoskeleton (padding typically added below the elbow for comfort).} \label{fig:hardware}
\end{figure}

% Strength Amplification
To increase the strength of an operator means to augment the interaction forces they exert on the unknown world. Amplification exoskeletons do this by being physically sandwiched between the user and the environment, measuring the forces the user applies to the exoskeleton, and applying additional forces to make the force between the exoskeleton and the environment much larger than the force between the human and the exoskeleton. As shown in the HARDIMAN I project, this is a challenging control problem \cite{MakinsonBodineFitck1969Techreport}. The system's stability depends on the human and environmental impedance \cite{Kazerooni1990TSMC}. While at first it may seem that strength and payload amplification serve the same purpose (indeed strength augmentation helps carry large, unmodeled payloads) strength amplification is a relatively impractical way to handle known payloads and is therefore a relatively unusual approach.

% Admittance control
A strategy known as admittance control, or ``getting out of the way'' \cite{LecoursStongeGosselin2012ICRA, YuRosen2013TCyb, FontanaVertechyMarcheschiSalsedoBergamasco2014RAM, JacobsenOlivier2014Patent}, uses position control on the exoskeleton and shifts a position set-point based on the human forces. This strategy is more focused on payload than strength amplification, since it requires modification and additional sensors in order to allow the operator to control the interaction force when interacting with unmoving environments \cite{KazerooniGuo1993JDSMC}. 

%While \cite{Kazerooni1990TSMC} has two sets of force sensors on the cuffs and end-effector, our approach uses the built-in force sensors of series elastic actuators and our sensors are therefore at the cuffs and actuators. This configuration (when scaled up to a whole robot) should allow whole body amplification even without knowledge of the contact location between the world and the exoskeleton.

% Similarity to force feedback human interaction
Haptic impedance controllers  \cite{ColgateHogan1988IJC,Hogan1989ICRA,ColgateBrown1994ICRA,AdamsHannaford1999TRA} share many of the control difficulties associated with strength amplification. This community has recognized the importance of the human impedance---which can change due to antagonistic muscle co-contraction \cite{Hogan1984TAC}. While many interaction controllers are designed to be energetically passive---requiring net energy from the human if it is forced to follow any cyclic trajectory---this is a conservative requirement. More accurate bounds on the human's capabilities lead to higher performance controllers which are still stable when interfacing with humans \cite{BuergerHogan2007TRO}. Bounded uncertain human impedance models have also been applied to design the closed loop interface dynamics of an amplification exoskeleton \cite{HeThomasPaineSentis2018ArXiv}.
% However this work explores the compliance shapes which can be achieved, and is not concerned with modeling any human. has so far not explored any mechanical optimizations of the cuff sensing system nor control optimization including the low level SEA controller . % add new paper

% This paper
This paper introduces a mechanical spring between the human cuff and the exoskeleton structure in order to improve the passivity properties of the system. We do not seek complementary stability with any particular bounded uncertain human model, but rather a qualitative change in the types of passive human behaviors that can destabilize the system. While the exoskeleton is fundamentally non-passive in that it amplifies the human (and adds energy in doing so), its impedance (and dynamic compliance\footnote{Dynamic compliance is a position per force transfer function, and the integral of admittance.}) at the human interface can be shaped to a considerable degree. We apply the recently developed theory of compliance shaping \cite{ThomasMehlingHolleySentisXXXX} for series elastic actuators twice to design the feedback gains for the exoskeleton. We call this controller design approach ``double compliance shaping''. We treat the exoskeleton with the elastic cuff as a series elastic actuator whose motor subsystem is itself a series elastic actuator. The contributions of the paper are in 1) the theory of double compliance shaping for exoskeleton control, 2) a transmission disturbance observer which is designed to remove nonlinear transmission effects, and 3) empirical validations of the first two contributions. Overall, this study aims to offer a framework for devising controllers and compliant cuffs for well behaved strength amplifying exoskeletons. 